import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { AdminDetailsForm } from '../../components/AdminDetailsForm/AdminDetailsForm';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { AdminFactory } from '../../factories/AdminFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { AdminService } from '../../services/AdminService';

class BaseAdminView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      adminFirstName: '',
      adminLastName: '',
      adminEmail: '',
      adminRole: '',
      disableButton: false,
      isMakingNetworkCall: true,
      adminSelected: undefined
    };

    this.onAdminEntered = this.onAdminEntered.bind(this);
    this.updateAdmin = this.updateAdmin.bind(this);
    this.deleteAdmin = this.deleteAdmin.bind(this);
  }

  componentDidMount() {
    this.setState({
      isMakingNetworkCall: true,
    }, async () => {
      const response = await AdminService.getAdmin(this.props.match.params.adminId);
      const adminSelected = AdminFactory.createFromJson(response.data);

      this.setState({
        adminSelected,
        isMakingNetworkCall: false
      });
    });
  }

  updateAdmin() {
    this.setState({
      disableButton: true
    }, async () => {

      const requestBody = {
        first_name: this.state.adminFirstName,
        last_name: this.state.adminLastName,
        // email: this.state.adminEmail,
        // adminRole: this.state.adminRole,
      };

      const response = await AdminService.updateAdmin(this.state.adminSelected.id, requestBody);
      if (response.status === 200) this.props.history.push('/admins');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteAdmin() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await AdminService.deleteAdmin(this.state.adminSelected.id);
      if (response.status === 204) this.props.history.push('/admins');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  onAdminEntered(adminFirstName, adminLastName, adminEmail, adminRole) {
    this.setState({
      adminFirstName,
      adminLastName,
      adminEmail,
      adminRole,
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader />;
    if (this.state.adminSelected === undefined) return null;

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px',
      }}>
        <PositionedBreadcrumbs
          breadcrumbs={[
            new Breadcrumb('OVERVIEW', '/overview'),
            new Breadcrumb('ADMINS', '/admins'),
            new Breadcrumb(this.state.adminSelected.firstName.toUpperCase(), '/brands/' + this.state.adminSelected.id)
          ]}
        />

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'Edit Admin Information'} />

          <AdminDetailsForm
            adminSelected={this.state.adminSelected}
            onAdminEntered={this.onAdminEntered}
          />

          <div style={{
            display: 'flex',
            flexDirection: 'row'
          }}>
            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Update Admin'}
                onClick={this.updateAdmin}
                disabled={this.state.disableButton}
                colour={ColorConstants.teal} />
            </div>

            <div>
              <StylisedButton
                title={'Delete Admin'}
                onClick={this.deleteAdmin}
                disabled={this.state.disableButton}
                colour={ColorConstants.red} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BaseAdminView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const AdminView = withRouter(BaseAdminView);