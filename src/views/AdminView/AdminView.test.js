import { shallow } from 'enzyme';
import React from 'react';
import { AdminView } from './AdminView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<AdminView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});