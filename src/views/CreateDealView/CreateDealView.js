import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { DealDetails } from '../../components/DealDetails/DealDetails';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { SelectMerchantBrandOutletForm } from '../../components/SelectMerchantBrandOutletForm/SelectMerchantBrandOutletForm';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CreateDealView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      outletSelected: undefined,
      isDealDetailFormHidden: true
    };

    this.onOutletSelected = this.onOutletSelected.bind(this);
  }

  onOutletSelected(outletSelected) {
    this.setState({
      outletSelected,
      isDealDetailFormHidden: false
    });
  }

  render() {
    return (
      <div
        style={{
          backgroundColor: 'white',
          margin: '8px',
          borderRadius: '4px'
        }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('DEALS', '/deals'),
          new Breadcrumb('CREATE DEAL', '/deals/create')]} />

        <div
          style={{
            padding: '8px',
          }}
        >
          <PanelTitle title={'New Deal Details'} />

          <SelectMerchantBrandOutletForm onOutletSelected={this.onOutletSelected} />
        </div>
        <div
          style={{
            padding: '8px',
          }}
          hidden={this.state.isDealDetailFormHidden}
        >
          <DealDetails outletSelected={this.state.outletSelected} />
        </div>
      </div>
    );
  }
}