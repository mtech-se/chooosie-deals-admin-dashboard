import { shallow } from 'enzyme';
import React from 'react';
import { CreateDealView } from './CreateDealView';

describe('CreateDealView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<CreateDealView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});