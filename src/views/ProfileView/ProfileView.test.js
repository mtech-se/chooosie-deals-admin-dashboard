import { shallow } from 'enzyme';
import React from 'react';
import { MyAccountView } from './MyAccountView';

describe('LoginView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<MyAccountView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});