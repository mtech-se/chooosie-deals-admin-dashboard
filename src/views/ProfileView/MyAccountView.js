import React from 'react';
import { BasicInformationForm } from '../../components/BasicInformationForm/BasicInformationForm';
import { ChangePasswordForm } from '../../components/ChangePasswordForm/ChangePasswordForm';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProfilePhotoForm } from '../../components/ProfilePhotoForm/ProfilePhotoForm';

export const MyAccountView = () => {
  return (
    <div style={{
      backgroundColor: 'white',
      marginLeft: '114px',
      marginRight: '114px'
    }}>

      <div style={{
        margin: '16px'
      }}>
        <PanelTitle title={'Profile Photo'} />
        <ProfilePhotoForm />
      </div>

      <div style={{
        display: 'flex'
      }}>
        <div style={{
          flexGrow: 1,
          margin: '16px'
        }}>
          <PanelTitle title={'Basic Information'} />
          <BasicInformationForm />
        </div>
        <div style={{
          flexGrow: 1,
          margin: '16px'
        }}>
          <PanelTitle title={'Change Password'} />
          <ChangePasswordForm />
        </div>
      </div>
    </div>
  );
};