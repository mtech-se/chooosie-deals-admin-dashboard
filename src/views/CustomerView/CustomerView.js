import Tab from '@material-ui/core/Tab/Tab';
import Tabs from '@material-ui/core/Tabs/Tabs';
import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CustomerDetailsForm } from '../../components/CustomerDetailsForm/CustomerDetailsForm';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { TabContainer } from '../../components/TabContainer/TabContainer';
import { ColorConstants } from '../../constants/ColorConstants';
import { CustomerFactory } from '../../factories/CustomerFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { CustomerService } from '../../services/CustomerService';

class BaseCustomerView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      customerId: '',
      customerName: '',
      customerFirstName: '',
      customerLastName: '',
      customerEmail: '',
      customerGender: '',
      customerBillingFirstName: '',
      customerBillingLastName: '',
      customerBillingCellphone: '',
      customerBillingTelephone: '',
      customerBillingAddressLineOne: '',
      customerBillingAddressLineTwo: '',
      customerBillingAddressPostalCode: '',
      disableButton: false,
      isMakingNetworkCall: false,
      customerSelected: undefined,
      value: 0,
    };

    this.onCustomerEntered = this.onCustomerEntered.bind(this);
    this.updateCustomer = this.updateCustomer.bind(this);
    this.deleteCustomer = this.deleteCustomer.bind(this);
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  componentDidMount() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await CustomerService.getCustomer(this.props.match.params.customerId);
      const customerSelected = CustomerFactory.createFromJson(response.data);

      this.setState({
        customerSelected,
        isMakingNetworkCall: false
      });
    });
  }

  updateCustomer() {
    console.log(this.state.customerEmail);

    this.setState({
      disableButton: true
    }, async () => {

      const requestBody = {
        first_name: this.state.customerFirstName,
        last_name: this.state.customerLastName,
        email: this.state.customerEmail,
        billing_first_name: this.state.customerBillingFirstName,
        billing_last_name: this.state.customerBillingLastName,
        billing_cellphone: this.state.customerBillingCellphone,
        billing_telephone: this.state.customerBillingTelephone,
        billing_add_line_one: this.state.customerBillingAddressLineOne,
        billing_add_line_two: this.state.customerBillingAddressLineTwo,
        billing_add_postal_code: this.state.customerBillingAddressPostalCode
      };

      const response = await CustomerService.updateCustomer(this.state.customerSelected.id, requestBody);
      if (response.status === 200) this.props.history.push('/customers');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteCustomer() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await CustomerService.deleteCustomer(this.state.customerSelected.id);
      if (response.status === 204) this.props.history.push('/customers');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  onCustomerEntered(customerFirstName,
                    customerLastName,
                    customerEmail,
                    customerBillingFirstName,
                    customerBillingLastName,
                    customerBillingCellphone,
                    customerBillingTelephone,
                    customerBillingAddressLineOne,
                    customerBillingAddressLineTwo,
                    customerBillingAddressPostalCode) {
    this.setState({
      customerFirstName,
      customerLastName,
      customerEmail,
      customerBillingFirstName,
      customerBillingLastName,
      customerBillingCellphone,
      customerBillingTelephone,
      customerBillingAddressLineOne,
      customerBillingAddressLineTwo,
      customerBillingAddressPostalCode
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader />;

    if (this.state.customerSelected === undefined) return null;

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px',
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CUSTOMERS', '/customers'),
          new Breadcrumb(this.state.customerSelected.name.toUpperCase(), '/customers/' + this.state.customerSelected.id)]} />

        <div style={{
          padding: '8px',
        }}>
          <div>
            <div>
              <Tabs
                style={{
                  color: '#1e9385',
                  marginBottom: '8px'
                }}
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor='primary'
                variant="fullWidth"
              >
                <Tab label="Profile" />
                <Tab label="Purchase History" />
              </Tabs>
            </div>

            {this.state.value === 0 &&
            <TabContainer>
              <CustomerDetailsForm customerSelected={this.state.customerSelected}
                                   onCustomerEntered={this.onCustomerEntered} />
              <div style={{
                display: 'flex',
                flexDirection: 'row',
              }}>
                <div style={{
                  marginRight: '8px',
                }}>
                  <StylisedButton
                    title={'Update Customer'}
                    onClick={this.updateCustomer}
                    disabled={this.state.disableButton}
                    colour={ColorConstants.teal}
                  />
                </div>

                <div>
                  <StylisedButton
                    title={'Delete Customer'}
                    onClick={this.deleteCustomer}
                    disabled={this.state.disableButton}
                    colour={ColorConstants.red}
                  />
                </div>
              </div>
            </TabContainer>}

            {this.state.value === 1 &&
            <TabContainer>
              Purchase History
            </TabContainer>}
          </div>
        </div>
      </div>
    );
  }
}

BaseCustomerView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const CustomerView = withRouter(BaseCustomerView);