import { shallow } from 'enzyme';
import React from 'react';
import { CustomerView } from './CustomerView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<CustomerView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});