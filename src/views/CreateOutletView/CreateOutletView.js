import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { OutletDetailsForm } from '../../components/OutletDetailsForm/OutletDetailsForm';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { SelectMerchantBrandForm } from '../../components/SelectMerchantBrandForm/SelectMerchantBrandForm';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CreateOutletView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      brandSelected: undefined,
      isOutletDetailFormHidden: true
    };
    this.onBrandSelected = this.onBrandSelected.bind(this);
  }

  onBrandSelected(brandSelected) {
    this.setState({
      brandSelected: brandSelected,
      isOutletDetailFormHidden: false
    });
  }

  render() {
    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px'
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('OUTLETS', '/outlet'),
          new Breadcrumb('CREATE OUTLET', '/outlets/create')]} />

        <div style={{
          padding: '8px',
        }}>

          <PanelTitle title={'New Outlet Details'} />

          <SelectMerchantBrandForm onBrandSelected={this.onBrandSelected} />
        </div>
        <div style={{
          padding: '8px',
        }}
             hidden={this.state.isOutletDetailFormHidden}>
          <OutletDetailsForm brandSelected={this.state.brandSelected} />
        </div>
      </div>
    );
  }
}