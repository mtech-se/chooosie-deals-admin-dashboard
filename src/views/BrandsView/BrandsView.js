import React from 'react';
import { Link } from 'react-router-dom';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { BrandsTable } from '../../components/DataTable/BrandsTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class BrandsView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      disableButton: true,
      brandsSelected: undefined
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onBrandsSelected = this.onBrandsSelected.bind(this);
    this.deleteBrand = this.deleteBrand.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  onBrandsSelected(brandsSelected) {
    this.setState({
        disableButton: false,
        brandsSelected
      }, () => {
        console.log(this.state.brandsSelected);
      }
    );
  }

  deleteBrand() {
    this.setState({
        disableButton: true
      }, () => {
        console.log('Delete these brands');
      }
    );
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('BRANDS', '/brands')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            display: 'flex',
            alignItems: 'baseline',
          }}>

            <StylisedButton
              title={'Create Brand'}
              colour={ColorConstants.primaryPurple}
              variant={'contained'}
              component={Link}
              to={'brands/create/'}
            />

            <div style={{
              flexGrow: '3',
              display: 'flex',
              alignItems: 'baseline',
              justifyContent: 'flex-end',
            }}>
              <SearchField
                style={{ margin: '8px 8px' }}
                onChange={this.onSearchTermChange}
              />

              {/*<StylisedButton*/}
              {/*title={'Delete'}*/}
              {/*onClick={this.deleteBrand}*/}
              {/*disabled={this.state.disableButton}*/}
              {/*colour={ColorConstants.red}*/}
              {/*/>*/}
            </div>
          </div>

          <BrandsTable
            searchTerm={this.state.searchTerm}
            onBrandsSelected={this.onBrandsSelected}
          />
        </div>
      </div>
    );
  }
}
