import React from 'react';
import { Link } from 'react-router-dom';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CuisinesTable } from '../../components/DataTable/CuisinesTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CuisinesView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      disableButton: true,
      cuisinesSelected: undefined
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onCuisinesSelected = this.onCuisinesSelected.bind(this);
    this.deleteCuisine = this.deleteCuisine.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  onCuisinesSelected(cuisinesSelected) {
    this.setState({
        disableButton: false,
        cuisinesSelected
      }, () => {
        console.log(this.state.cuisinesSelected);
      }
    );
  }

  deleteCuisine() {
    this.setState({
        disableButton: true
      }, () => {
        console.log('Delete these cuisines');
      }
    );
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CUISINES', '/cuisines')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            height: '60px',
          }}>
            <div style={{
              display: 'flex',
              alignItems: 'baseline',
            }}>

              <StylisedButton
                title={'Create Cuisines'}
                colour={ColorConstants.primaryPurple}
                variant={'contained'}
                component={Link}
                to={'cuisines/create/'}
              />

              <div style={{
                flexGrow: '3',
                display: 'flex',
                alignItems: 'baseline',
                justifyContent: 'flex-end',
              }}>
                <SearchField
                  style={{ margin: '8px 8px' }}
                  onChange={this.onSearchTermChange}
                />

                {/*<StylisedButton*/}
                {/*title={'Delete'}*/}
                {/*onClick={this.deleteCuisine}*/}
                {/*disabled={this.state.disableButton}*/}
                {/*colour={ColorConstants.red}*/}
                {/*/>*/}
              </div>
            </div>
          </div>

          <CuisinesTable
            searchTerm={this.state.searchTerm}
            onCuisinesSelected={this.onCuisinesSelected}
          />
        </div>
      </div>
    );
  }
}