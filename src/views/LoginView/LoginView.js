import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { AppBrand } from '../../components/AppBrand/AppBrand';
import { LabelField } from '../../components/LabelField/LabelField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { logInAdmin } from '../../store/actions/action-creators';

class BaseLoginView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: 'root@halalfoodhunt.com',
      password: 'secret',
      isSignInButtonDisabled: false,
    };

    this.onSignInButtonClick = this.onSignInButtonClick.bind(this);
  }

  async onSignInButtonClick() {
    this.setState({
      isSignInButtonDisabled: true,
    });

    await this.props.logInAdmin(this.state.email, this.state.password);

    this.setState({
      isSignInButtonDisabled: false,
    }, () => this.props.history.push('/'));
  }

  render() {
    return (
      <div style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <div style={{
          width: '400px',
        }}>
          <AppBrand style={{
            borderRadius: '8px 8px 0 0',
          }} />

          <div style={{
            padding: '16px',
            borderRadius: '0 0 8px 8px',
            backgroundColor: ColorConstants.faintGray,
          }}>

            <LabelField
              title={'Email'}
              type={'text'}
              value={this.state.email}
              onChangeHandler={event => this.setState({ email: event.target.value })}
            />

            <LabelField
              title={'Password'}
              type={'password'}
              value={this.state.password}
              onChangeHandler={event => this.setState({ password: event.target.value })}
            />

            <StylisedButton
              title={'Sign in'}
              onClick={this.onSignInButtonClick}
              disabled={this.state.isSignInButtonDisabled}
              colour={ColorConstants.teal}
              style={{
                marginTop: '16px',
              }}
            />

          </div>
        </div>
      </div>
    );
  }
}

BaseLoginView.propTypes = {
  history: PropTypes.object,
  logInAdmin: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => {
  return {
    logInAdmin: (email, password) => dispatch(logInAdmin(email, password))
  };
};

export const LoginView = connect(null, mapDispatchToProps)(withRouter(BaseLoginView));