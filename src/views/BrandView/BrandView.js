import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { BrandDetailsForm } from '../../components/BrandDetailsForm/BrandDetailsForm';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { LabelField } from '../../components/LabelField/LabelField';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { BrandFactory } from '../../factories/BrandFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { BrandService } from '../../services/BrandService';
import { PhotoService } from '../../services/PhotoService';

class BaseBrandView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      brandName: '',
      brandPhoto: undefined,
      disableButton: false,
      isMakingNetworkCall: false,
      brandSelected: undefined
    };

    this.onBrandEntered = this.onBrandEntered.bind(this);
    this.updateBrand = this.updateBrand.bind(this);
    this.deleteBrand = this.deleteBrand.bind(this);
    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await BrandService.getBrand(this.props.match.params.brandId);
      const brandSelected = BrandFactory.createFromJson(response.data);

      this.setState({
        brandSelected,
        isMakingNetworkCall: false
      });
    });
  }

  async updateBrandPhoto() {
    const formData = new FormData();
    formData.append('photos', this.state.brandPhoto);

    const updateUrl = '/brands/' + this.state.brandSelected.id;

    const responseUpdatePhoto = await PhotoService.updatePhoto(updateUrl, formData);
    console.log(responseUpdatePhoto);

    if (responseUpdatePhoto.status === 201) this.props.history.push('/brands');
    else throw new Error('Something went wrong - the response was something other than 200');
  }

  updateBrand() {
    this.setState({
      disableButton: true
    }, async () => {

      const requestBody = {
        name: this.state.brandName,
        id: this.state.brandSelected.id
      };

      const responseUpdateBrand = await BrandService.updateBrand(this.state.brandSelected.id, requestBody);

      if (responseUpdateBrand.status === 200) this.updateBrandPhoto();
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteBrand() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await BrandService.deleteBrand(this.state.brandSelected.id);
      if (response.status === 204) this.props.history.push('/brands');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  onBrandEntered(brandName, brandPhoto) {
    this.setState({
      brandName,
      brandPhoto
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader/>;

    if (this.state.brandSelected === undefined) return null;

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px',
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('BRANDS', '/brands'),
          new Breadcrumb(this.state.brandSelected.name.toUpperCase(), '/brands/' + this.state.brandSelected.id)]}/>

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'Edit Brand Details'}/>

          <div style={{
            width: '33.3%',
            marginBottom: '16px'
          }}>

            <PanelTitle
              title={'Merchant'}
              marginBottom={'0px'}
            />
            <LabelField
              title={'Name'}
              value={this.state.brandSelected.merchant.name}
              type={'text'}
              isDisabled={true}
            />
          </div>

          <BrandDetailsForm
            brandSelected={this.state.brandSelected}
            onBrandEntered={this.onBrandEntered}
            onPhotoDeleted={this.fetchData}
          />

          <div style={{
            display: 'flex',
            flexDirection: 'row',
            marginTop: '16px',
          }}>
            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Update Brand'}
                onClick={this.updateBrand}
                disabled={this.state.disableButton}
                colour={ColorConstants.teal}
              />
            </div>

            <div>
              <StylisedButton
                title={'Delete Brand'}
                onClick={this.deleteBrand}
                disabled={this.state.disableButton}
                colour={ColorConstants.red}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BaseBrandView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const BrandView = withRouter(BaseBrandView);