import { shallow } from 'enzyme';
import React from 'react';
import { BrandView } from './BrandView';

describe('BrandView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<BrandView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});