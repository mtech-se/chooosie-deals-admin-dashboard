import { shallow } from 'enzyme';
import React from 'react';
import { PurchasesView } from './PurchasesView';

describe('SalesSummaryView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<PurchasesView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});