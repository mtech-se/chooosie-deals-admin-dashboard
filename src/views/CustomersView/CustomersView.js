import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CustomersTable } from '../../components/DataTable/CustomersTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CustomersView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: undefined,
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CUSTOMERS', '/customers')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            display: 'flex',
            justifyContent: 'flex-end',
          }}>
            <SearchField
              style={{ margin: '8px 8px' }}
              onChange={this.onSearchTermChange}
            />
          </div>

          <CustomersTable searchTerm={this.state.searchTerm} />
        </div>
      </div>
    );
  }
}