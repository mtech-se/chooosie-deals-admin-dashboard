import { shallow } from 'enzyme';
import React from 'react';
import { CustomersView } from './CustomersView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<CustomersView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});