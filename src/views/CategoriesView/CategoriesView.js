import React from 'react';
import { Link } from 'react-router-dom';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CategoriesTable } from '../../components/DataTable/CategoriesTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CategoriesView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      disableButton: true,
      categoriesSelected: undefined
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onCategoriesSelected = this.onCategoriesSelected.bind(this);
    this.deleteCategory = this.deleteCategory.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  onCategoriesSelected(categoriesSelected) {
    this.setState({
        disableButton: false,
        categoriesSelected
      }, () => {
        console.log(this.state.categoriesSelected);
      }
    );
  }

  deleteCategory() {
    this.setState({
        disableButton: true
      }, () => {
        console.log('Delete these categories');
      }
    );
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CATEGORIES', '/categories')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            height: '60px',
          }}>
            <div style={{
              display: 'flex',
              alignItems: 'baseline',
            }}>

              <StylisedButton
                title={'Create Category'}
                colour={ColorConstants.primaryPurple}
                variant={'contained'}
                component={Link}
                to={'categories/create/'}
              />

              <div style={{
                flexGrow: '3',
                display: 'flex',
                alignItems: 'baseline',
                justifyContent: 'flex-end',
              }}>
                <SearchField
                  style={{ margin: '8px 8px' }}
                  onChange={this.onSearchTermChange}
                />

                {/*<StylisedButton*/}
                {/*title={'Delete'}*/}
                {/*onClick={this.deleteCategory}*/}
                {/*disabled={this.state.disableButton}*/}
                {/*colour={ColorConstants.red}*/}
                {/*/>*/}
              </div>
            </div>
          </div>

          <CategoriesTable
            searchTerm={this.state.searchTerm}
            onCategoriesSelected={this.onCategoriesSelected}
          />
        </div>
      </div>
    );
  }
}