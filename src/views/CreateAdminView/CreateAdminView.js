import React from 'react';
import { AdminDetails } from '../../components/AdminDetails/AdminDetails';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CreateAdminView extends React.Component {
  render() {
    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px'
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('ADMINS', '/admins'),
          new Breadcrumb('CREATE ADMIN', '/admins/create')]} />

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'New Admin Details'} />

          <AdminDetails />
        </div>
      </div>
    );
  }
}