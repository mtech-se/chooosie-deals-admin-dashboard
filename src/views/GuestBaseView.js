import * as PropTypes from 'prop-types';
import React from 'react';

export class GuestBaseView extends React.Component {
  render() {
    return (
      <div style={{
        margin: '0 auto',
        maxWidth: '1366px',
        minHeight: '768px',
        // height: '100vh',
        display: 'flex',
        // justifyContent: 'center',
        // alignItems: 'center',
        flexDirection: 'row',
      }}>
        {this.props.component}
      </div>
    );
  }
}

GuestBaseView.propTypes = {
  component: PropTypes.element.isRequired,
};