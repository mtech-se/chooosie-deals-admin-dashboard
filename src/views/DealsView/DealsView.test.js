import { shallow } from 'enzyme';
import React from 'react';
import { DealsView } from './DealsView';

describe('DealsView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<DealsView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});