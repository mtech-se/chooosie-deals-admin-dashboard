import React from 'react';
import { Link } from 'react-router-dom';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { DealsTable } from '../../components/DataTable/DealsTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class DealsView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      disableButton: true,
      outletsSelected: undefined
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onDealsSelected = this.onDealsSelected.bind(this);
    this.deleteDeal = this.deleteDeal.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  onDealsSelected(dealsSelected) {
    this.setState({
        disableButton: false,
        dealsSelected
      }, () => {
        console.log(this.state.dealsSelected);
      }
    );
  }

  deleteDeal() {
    this.setState({
        disableButton: true
      }, () => {
        console.log('Delete these deals');
      }
    );
  }

  render() {
    return (
      <div style={{ padding: '8px', }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('DEALS', '/deals')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            display: 'flex',
            alignItems: 'baseline',
          }}>

            <StylisedButton
              title={'Create Deal'}
              colour={ColorConstants.primaryPurple}
              variant={'contained'}
              component={Link}
              to={'deals/create/'}
            />

            <div style={{
              flexGrow: '3',
              display: 'flex',
              alignItems: 'baseline',
              justifyContent: 'flex-end',
            }}>
              <SearchField
                style={{ margin: '8px 8px' }}
                onChange={this.onSearchTermChange}
              />

              {/*<StylisedButton*/}
              {/*title={'Delete'}*/}
              {/*onClick={this.deleteDeal}*/}
              {/*disabled={this.state.disableButton}*/}
              {/*colour={ColorConstants.red}*/}
              {/*/>*/}
            </div>
          </div>

          <DealsTable
            searchTerm={this.state.searchTerm}
            onDealsSelected={this.onDealsSelected}
          />
        </div>
      </div>
    );
  }
}