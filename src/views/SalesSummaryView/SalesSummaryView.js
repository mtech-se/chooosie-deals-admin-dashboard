import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { Breadcrumb } from '../../models/Breadcrumb';

export const SalesSummaryView = () => {
  const breadcrumbs = [
    new Breadcrumb('SALES SUMMARY', '/sales-summary'),
  ];

  return (
    <div style={{
      padding: '8px',
    }}>
      <PositionedBreadcrumbs breadcrumbs={breadcrumbs} />

    </div>
  );
};