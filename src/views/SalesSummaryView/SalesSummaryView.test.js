import { shallow } from 'enzyme';
import React from 'react';
import { SalesSummaryView } from './SalesSummaryView';

describe('SalesSummaryView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<SalesSummaryView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});