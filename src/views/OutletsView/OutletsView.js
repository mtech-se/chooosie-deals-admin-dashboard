import React from 'react';
import { Link } from 'react-router-dom';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { OutletsTable } from '../../components/DataTable/OutletsTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class OutletsView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      disableButton: true,
      outletsSelected: undefined
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onOutletsSelected = this.onOutletsSelected.bind(this);
    this.deleteOutlet = this.deleteOutlet.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  onOutletsSelected(outletsSelected) {
    this.setState({
        disableButton: false,
        outletsSelected
      }, () => {
        console.log(this.state.outletsSelected);
      }
    );
  }

  deleteOutlet() {
    this.setState({
        disableButton: true
      }, () => {
        console.log('Delete these outlets');
      }
    );
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('OUTLETS', '/outlets')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            display: 'flex',
            alignItems: 'baseline',
          }}>

            <StylisedButton
              title={'Create Outlet'}
              colour={ColorConstants.primaryPurple}
              variant={'contained'}
              component={Link}
              to={'outlets/create/'}
            />

            <div style={{
              flexGrow: '3',
              display: 'flex',
              alignItems: 'baseline',
              justifyContent: 'flex-end',
            }}>
              <SearchField
                style={{ margin: '8px 8px' }}
                onChange={this.onSearchTermChange}
              />

              {/*<StylisedButton*/}
              {/*title={'Delete'}*/}
              {/*onClick={this.deleteOutlet}*/}
              {/*disabled={this.state.disableButton}*/}
              {/*colour={ColorConstants.red}*/}
              {/*/>*/}
            </div>
          </div>

          <OutletsTable
            searchTerm={this.state.searchTerm}
            onOutletsSelected={this.onOutletsSelected}
          />
        </div>
      </div>
    );
  }
}