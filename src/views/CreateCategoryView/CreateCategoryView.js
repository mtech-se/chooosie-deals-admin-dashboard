import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CategoryDetails } from '../../components/CategoryDetails/CategoryDetails';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CreateCategoryView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px'
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CATEGORIES', '/categories'),
          new Breadcrumb('CREATE CATEGORY', '/categories/create')]} />

        <div style={{
          padding: '8px',
        }}>

          <PanelTitle title={'New Category Details'} />

          <CategoryDetails />
        </div>
      </div>
    );
  }
}