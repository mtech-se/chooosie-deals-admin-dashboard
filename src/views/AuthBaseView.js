import * as PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { LeftNavBar } from '../components/LeftNavBar/LeftNavBar';
import { RightPanel } from '../components/RightPanel/RightPanel';
import { initStoreWithAccessTokenAndUpdateAdminDetails } from '../store/actions/action-creators';

class BaseAuthBaseView extends React.Component {
  componentDidMount() {
    this.props.initStore();
  }

  render() {
    if (this.props.accessToken === undefined) return <div>Waiting for Redux to hydrate...</div>;

    return (
      <div style={{
        margin: '0 auto',
        maxWidth: '1366px',
        minHeight: '768px',
        // height: '100vh',
        display: 'flex',
        flexDirection: 'row',
      }}>
        <LeftNavBar />
        <RightPanel component={this.props.component} />
      </div>
    );
  }
}

BaseAuthBaseView.propTypes = {
  component: PropTypes.element.isRequired,
  accessToken: PropTypes.string,
  initStore: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  accessToken: state.accessToken,
});

const mapDispatchToProps = dispatch => {
  return {
    initStore: () => dispatch(initStoreWithAccessTokenAndUpdateAdminDetails()),
  };
};

export const AuthBaseView = connect(mapStateToProps, mapDispatchToProps)(BaseAuthBaseView);