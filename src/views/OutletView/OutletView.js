import React from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { LabelField } from '../../components/LabelField/LabelField';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { OutletFactory } from '../../factories/OutletFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { OutletService } from '../../services/OutletService';

class BaseOutletView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locationName: '',
      locationAddress: '',
      disableButton: false,
      isMakingNetworkCall: false,
      outletSelected: undefined
    };

    this.onOutletEntered = this.onOutletEntered.bind(this);
    this.updateOutlet = this.updateOutlet.bind(this);
    this.deleteOutlet = this.deleteOutlet.bind(this);
  }

  componentDidMount() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await OutletService.getOutlet(this.props.match.params.outletId);
      const outletSelected = OutletFactory.createFromJson(response.data);

      this.setState({
        outletSelected,
        locationName: outletSelected.name,
        locationAddress: outletSelected.address,
        isMakingNetworkCall: false
      });
    });
  }

  updateOutlet() {
    this.setState({
      disableButton: true
    }, async () => {

      const requestBody = {
        name: this.state.locationName,
        address: this.state.locationAddress,
        id: this.state.outletSelected.id
      };

      const response = await OutletService.updateOutlet(this.state.outletSelected.id, requestBody);
      if (response.status === 200) this.props.history.push('/outlets');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteOutlet() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await OutletService.deleteOutlet(this.state.outletSelected.id);
      if (response.status === 204) this.props.history.push('/outlets');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  onOutletEntered(outletName) {
    this.setState({
      outletName
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader />;

    if (this.state.outletSelected === undefined) return null;

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px',
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('OUTLETS', '/outlets'),
          new Breadcrumb(this.state.outletSelected.name.toUpperCase(), '/outlets/' + this.state.outletSelected.id)]} />

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'Edit Outlet Details'} />

          <div style={{
            display: 'flex',
            flexDirection: 'row',
            marginBottom: '16px'
          }}>
            <div style={{
              width: '33.3%',
              marginRight: '8px'
            }}>
              <PanelTitle
                title={'Merchant'}
                marginBottom={'0px'}
              />
              <LabelField
                title={''}
                value={this.state.outletSelected.brand.merchant.name}
                type={'text'}
                isDisabled={true}
              />
            </div>

            <div style={{
              width: '33.3%'
            }}>
              <PanelTitle
                title={'Brand'}
                marginBottom={'0px'}
              />
              <LabelField
                title={''}
                value={this.state.outletSelected.brand.name}
                type={'text'}
                isDisabled={true}
              />
            </div>
          </div>

          <div style={{
            maxWidth: '33.3%',
            marginBottom: '16px'
          }}>
            <PanelTitle
              title={'Outlet'}
              marginBottom={'0px'}
            />
            <LabelField
              marginBottom={'8px'}
              title={'Location Name'}
              type={'text'}
              value={this.state.locationName}
              onChangeHandler={event => {
                this.setState({
                  locationName: event.target.value
                });
              }}
            />

            <LabelField
              marginBottom={'16px'}
              title={'Location Address'}
              type={'text'}
              value={this.state.locationAddress}
              onChangeHandler={event => {
                this.setState({
                  locationAddress: event.target.value
                });
              }}
            />
          </div>

          <div style={{
            display: 'flex',
            flexDirection: 'row'
          }}>
            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Update Outlet'}
                onClick={this.updateOutlet}
                disabled={this.state.disableButton}
                colour={ColorConstants.teal}
              />
            </div>

            <div>
              <StylisedButton
                title={'Delete Outlet'}
                onClick={this.deleteOutlet}
                disabled={this.state.disableButton}
                colour={ColorConstants.red}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BaseOutletView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const OutletView = withRouter(BaseOutletView);