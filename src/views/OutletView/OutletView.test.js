import { shallow } from 'enzyme';
import React from 'react';
import { OutletView } from './OutletView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<OutletView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});