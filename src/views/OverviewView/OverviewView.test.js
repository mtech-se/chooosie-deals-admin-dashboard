import { shallow } from 'enzyme';
import React from 'react';
import { OverviewView } from './OverviewView';

describe('OverviewView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<OverviewView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});