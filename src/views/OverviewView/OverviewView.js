import moment from 'moment/moment';
import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CardTitle } from '../../components/CardTitle/CardTitle';
import { CategoryList } from '../../components/CategoriesList/CategoryList';
import { DashboardCard } from '../../components/DashboardCard/DashboardCard';
import { DateRange } from '../../components/DateRange/DateRange';
import { DealList } from '../../components/DealsList/DealList';
import { RedemptionList } from '../../components/RedemptionList/RedemptionList';
import { CategoryFactory } from '../../factories/CategoryFactory';
import { DealFactory } from '../../factories/DealFactory';
import { MerchantFactory } from '../../factories/MerchantFactory';
import { OverviewStatsFactory } from '../../factories/OverviewStatsFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { CategoryWithPublishedDealCount } from '../../models/CategoryWithPublishedDealCount';
import { MerchantWithRedemptionCount } from '../../models/MerchantWithRedemptionCount';
import { OverviewStatsService } from '../../services/OverviewStatsService';

const calculatePreviousDateRange = (startDate, endDate) => {
  const period = moment(endDate).diff(moment(startDate), 'days') + 1;
  const previousStartDate = moment(startDate).subtract(period, 'days');
  const previousEndDate = moment(endDate).subtract(period, 'days');

  return { previousStartDate, previousEndDate };
};

export class OverviewView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      totalPublishedDeals: undefined,
      totalPurchasedDeals: undefined,
      totalNewCustomers: undefined,
      totalSales: undefined,
      totalPayouts: undefined,
      totalCommissions: undefined,

      previousTotalPublishedDeals: undefined,
      previousTotalPurchasedDeals: undefined,
      previousTotalNewCustomers: undefined,
      previousTotalSales: undefined,
      previousTotalPayouts: undefined,
      previousTotalCommissions: undefined,

      topDealsByPurchaseCount: undefined,
      topCategoriesByPublishedDealCount: undefined,
      topMerchantsByPurchaseRedemptionCount: undefined,

      startDate: moment('2018-01-01'),
      endDate: moment('2018-12-31'),
      isLoading: true,
    };

    this.handleDateUpdate = this.handleDateUpdate.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    const previousPeriodDateRange = calculatePreviousDateRange(this.state.startDate, this.state.endDate);

    Promise
      .all([
        this.fetchOverviewStats(this.state.startDate, this.state.endDate),
        this.fetchOverviewStats(previousPeriodDateRange.previousStartDate, previousPeriodDateRange.previousEndDate),
        this.fetchTopDealsByPurchaseCount(this.state.startDate, this.state.endDate),
        this.fetchTopCategoriesByPublishedDealCount(this.state.startDate, this.state.endDate),
        this.fetchTopMerchantsByPurchaseRedemptionCount(this.state.startDate, this.state.endDate)
      ])
      .then(resolvedValues => {
        /** @type {OverviewStats} */
        const overviewStats = resolvedValues[0];
        const previousOverviewStats = resolvedValues[1];

        this.setState({
          totalPublishedDeals: overviewStats.totalPublishedDeals,
          totalPurchasedDeals: overviewStats.totalPurchasedDeals,
          totalNewCustomers: overviewStats.totalNewCustomers,
          totalSales: overviewStats.totalSales,
          totalPayouts: overviewStats.totalPayouts,
          totalCommissions: overviewStats.totalCommissions,

          previousTotalPublishedDeals: previousOverviewStats.totalPublishedDeals,
          previousTotalPurchasedDeals: previousOverviewStats.totalPurchasedDeals,
          previousTotalNewCustomers: previousOverviewStats.totalNewCustomers,
          previousTotalSales: previousOverviewStats.totalSales,
          previousTotalPayouts: previousOverviewStats.totalPayouts,
          previousTotalCommissions: previousOverviewStats.totalCommissions,

          topDealsByPurchaseCount: resolvedValues[2],
          topCategoriesByPublishedDealCount: resolvedValues[3],
          topMerchantsByPurchaseRedemptionCount: resolvedValues[4],
          isLoading: false,
        });
      });
  }

  fetchOverviewStats(startDate, endDate) {
    return new Promise(resolve => {
      OverviewStatsService
        .getOverviewStats(startDate.toISOString(), endDate.toISOString())
        .then(response => resolve(OverviewStatsFactory.createFromJson(response.data.content)));
    });
  }

  fetchTopDealsByPurchaseCount(startDate, endDate) {
    return new Promise(resolve => {
      OverviewStatsService
        .getTopDealsByPurchaseCount(startDate.toISOString(), endDate.toISOString())
        .then(response => resolve(DealFactory.createFromJsonArray(response.data.content)));
    });
  }

  fetchTopCategoriesByPublishedDealCount(startDate, endDate) {
    return new Promise(resolve => {
      OverviewStatsService
        .getTopCategoriesByPublishedDealCount(startDate.toISOString(), endDate.toISOString())
        .then(response => {
          const results = response.data.map(item => {
            const category = CategoryFactory.createFromJson(item.category);
            const numPublishedDeals = item.num_published_deals;

            return new CategoryWithPublishedDealCount(category, numPublishedDeals);
          });

          resolve(results);
        });
    });
  }

  fetchTopMerchantsByPurchaseRedemptionCount(startDate, endDate) {
    return new Promise(resolve => {
      OverviewStatsService
        .getTopMerchantsByPurchaseRedemptionCount(startDate.toISOString(), endDate.toISOString())
        .then(response => {
          const results = response.data.map(item => {
            const merchant = MerchantFactory.createFromJson(item.merchant);
            const numDealsRedeemed = item.num_deals_redeemed;

            return new MerchantWithRedemptionCount(merchant, numDealsRedeemed);
          });

          resolve(results);
        });
    });
  }

  handleDateUpdate(startDate, endDate) {
    this.setState({
      startDate: startDate,
      endDate: endDate,
    }, () => {
      this.fetchData();
    });
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>
        <PositionedBreadcrumbs breadcrumbs={[new Breadcrumb('OVERVIEW', '/overview')]} />

        <div style={{
          padding: 8,
        }}>
          <DateRange onDateUpdate={this.handleDateUpdate} />
        </div>

        <div style={{
          display: 'flex',
          marginBottom: '8px',
        }}>
          <div style={{
            width: '33%',
            marginRight: '8px',
          }}>
            <DashboardCard
              title={'Total Published Deals'}
              currentFigure={this.state.totalPublishedDeals}
              previousFigure={this.state.previousTotalPublishedDeals}
              type={'Non-currency'}
              isLoading={this.state.isLoading}
            />
          </div>

          <div style={{
            width: '33%',
            marginRight: '8px',
          }}>
            <DashboardCard
              title={'Total Purchased Deals'}
              currentFigure={this.state.totalPurchasedDeals}
              previousFigure={this.state.previousTotalPurchasedDeals}
              type={'Non-currency'}
              isLoading={this.state.isLoading}
            />
          </div>

          <div style={{
            width: '33%',
          }}>
            <DashboardCard
              title={'Total New Customers'}
              currentFigure={this.state.totalNewCustomers}
              previousFigure={this.state.previousTotalNewCustomers}
              type={'Non-currency'}
              isLoading={this.state.isLoading}
            />
          </div>
        </div>

        <div style={{
          display: 'flex',
          marginBottom: '8px',
        }}>
          <div style={{
            width: '33%',
            marginRight: '8px',
          }}>
            <DashboardCard
              title={'Total Sales'}
              currentFigure={this.state.totalSales}
              previousFigure={this.state.previousTotalSales}
              type={'Currency'}
            />
          </div>

          <div style={{
            width: '33%',
            marginRight: '8px',
          }}>
            <DashboardCard
              title={'Total Payouts'}
              currentFigure={this.state.totalPayouts}
              previousFigure={this.state.previousTotalPayouts}
              type={'Currency'}
            />
          </div>

          <div style={{
            width: '33%',
          }}>
            <DashboardCard
              title={'Total Commission'}
              currentFigure={this.state.totalCommissions}
              previousFigure={this.state.previousTotalCommissions}
              type={'Currency'}
            />
          </div>
        </div>

        <div style={{
          display: 'flex',
        }}>
          <div style={{
            width: '33%',
            marginRight: '8px',
          }}>
            <CardTitle
              title={'Top 5 Deals by Purchases'}
            />
            <DealList
              deals={this.state.topDealsByPurchaseCount}
              isLoading={this.state.isLoading}
            />
          </div>

          <div style={{
            width: '33%',
            marginRight: '8px',
          }}>
            <CardTitle
              title={'Top 5 Categories by Published Deals'}
            />
            <CategoryList
              categoriesWithPublishedDealCount={this.state.topCategoriesByPublishedDealCount}
              isLoading={this.state.isLoading}
            />
          </div>

          <div style={{
            width: '33%',
          }}>
            <CardTitle
              title={'Top 5 Merchants by Redemption'}
            />
            <RedemptionList
              merchantsWithRedemptionCount={this.state.topMerchantsByPurchaseRedemptionCount}
              isLoading={this.state.isLoading}
            />
          </div>
        </div>
      </div>
    );
  }
}