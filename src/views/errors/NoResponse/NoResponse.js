import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';

const BaseNoResponse = props => {
  let params = new URLSearchParams(props.location.search);

  return <div>{params.get('message')}</div>;
};

BaseNoResponse.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
};

export const NoResponse = withRouter(BaseNoResponse);