import { shallow } from 'enzyme';
import React from 'react';
import { NoResponse } from './NoResponse';

describe('NoResponse', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<NoResponse />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});