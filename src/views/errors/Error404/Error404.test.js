import { shallow } from 'enzyme';
import React from 'react';
import { Error404 } from './Error404';

describe('Error404', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<Error404 />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});