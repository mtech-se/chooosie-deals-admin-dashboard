import React from 'react';
import logo from '../../../assets/images/error-404-star-wars.gif';

export const Error404 = () => (
  <div className="Error404">
    <img src={logo}
         alt="404 - Page Not Found" />
  </div>
);