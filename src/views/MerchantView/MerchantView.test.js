import { shallow } from 'enzyme';
import React from 'react';
import { MerchantView } from './MerchantView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<MerchantView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});