import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { MerchantDetailsForm } from '../../components/MerchantDetailsForm/MerchantDetailsForm';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { MerchantFactory } from '../../factories/MerchantFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { ContactPerson } from '../../models/ContactPerson';
import { Merchant } from '../../models/Merchant';
import { MerchantService } from '../../services/MerchantService';

class BaseMerchantView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      merchantId: '',
      merchantName: '',
      merchantRegistrationNumber: '',
      merchantMailingAddress: '',
      contactPersonName: '',
      contactPersonPosition: '',
      contactPersonMobileNumber: '',
      contactPersonEmailAddress: '',
      disableButton: false,
      isMakingNetworkCall: false,
    };

    this.onMerchantEntered = this.onMerchantEntered.bind(this);
    this.updateMerchant = this.updateMerchant.bind(this);
    this.deleteMerchant = this.deleteMerchant.bind(this);
  }

  componentDidMount() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await MerchantService.getMerchant(this.props.match.params.merchantId);
      const merchant = MerchantFactory.createFromJson(response.data);

      this.setState({
        merchantId: merchant.id,
        merchantName: merchant.name,
        merchantRegistrationNumber: merchant.businessRegistrationNumber,
        merchantMailingAddress: merchant.mailingAddress,
        contactPersonName: merchant.contactPersons[0].name,
        contactPersonPosition: merchant.contactPersons[0].position,
        contactPersonMobileNumber: merchant.contactPersons[0].mobileNumber,
        contactPersonEmailAddress: merchant.contactPersons[0].email,
        isMakingNetworkCall: false
      });
    });
  }

  updateMerchant() {
    this.setState({
      disableButton: true
    }, async () => {

      const requestBody = {
        name: this.state.merchantName,
        business_registration_number: this.state.merchantRegistrationNumber,
        mailing_address: this.state.merchantMailingAddress,
        contact_persons: [
          {
            name: this.state.contactPersonName,
            position: this.state.contactPersonPosition,
            mobile_number: this.state.contactPersonMobileNumber,
            email: this.state.contactPersonEmailAddress
          }
        ]
      };

      const response = await MerchantService.updateMerchant(this.state.merchantId, requestBody);
      if (response.status === 200) this.props.history.push('/merchants');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteMerchant() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await MerchantService.deleteMerchant(this.state.merchantId);
      if (response.status === 204) this.props.history.push('/merchants');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  onMerchantEntered(merchantName,
                    merchantRegistrationNumber,
                    merchantMailingAddress,
                    contactPersonName,
                    contactPersonPosition,
                    contactPersonMobileNumber,
                    contactPersonEmailAddress) {
    this.setState({
      merchantName,
      merchantRegistrationNumber,
      merchantMailingAddress,
      contactPersonName,
      contactPersonPosition,
      contactPersonMobileNumber,
      contactPersonEmailAddress
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader />;

    const merchantSelected = new Merchant(
      this.state.merchantId,
      this.state.merchantName,
      this.state.merchantRegistrationNumber,
      this.state.merchantMailingAddress,
      undefined,
      undefined,
      undefined,
      [
        new ContactPerson(
          undefined,
          this.state.contactPersonName,
          this.state.contactPersonPosition,
          this.state.contactPersonMobileNumber,
          this.state.contactPersonEmailAddress,
          undefined,
          undefined,
          undefined,
        )
      ],
    );

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px',
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('MERCHANTS', '/merchants'),
          new Breadcrumb(this.state.merchantName.toUpperCase(), '/merchants/' + this.state.merchantId)]} />

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'Edit Merchant Details'} />

          <MerchantDetailsForm merchantSelected={merchantSelected}
                               onMerchantEntered={this.onMerchantEntered} />

          <div style={{
            display: 'flex',
            flexDirection: 'row'
          }}>
            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Update Merchant'}
                onClick={this.updateMerchant}
                disabled={this.state.disableButton}
                colour={ColorConstants.teal} />
            </div>

            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Delete Merchant'}
                onClick={this.deleteMerchant}
                disabled={this.state.disableButton}
                colour={ColorConstants.red} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BaseMerchantView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const MerchantView = withRouter(BaseMerchantView);