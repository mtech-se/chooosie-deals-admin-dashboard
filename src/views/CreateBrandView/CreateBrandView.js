import React from 'react';
import { BrandDetails } from '../../components/BrandDetails/BrandDetails';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { SelectMerchantForm } from '../../components/SelectMerchantForm/SelectMerchantForm';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CreateBrandView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      merchantSelected: undefined,
      isBrandDetailFormHidden: true
    };
    this.onMerchantSelected = this.onMerchantSelected.bind(this);
  }

  onMerchantSelected(merchantSelected) {
    this.setState({
      merchantSelected: merchantSelected,
      isBrandDetailFormHidden: false
    });
  }

  render() {
    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px'
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('BRANDS', '/brands'),
          new Breadcrumb('CREATE BRAND', '/brands/create')]} />

        <div style={{
          padding: '8px',
        }}>

          <PanelTitle title={'New Brand Details'} />

          <div style={{
            maxWidth: '33%',
            marginBottom: '16px'
          }}>
            <SelectMerchantForm onMerchantSelected={this.onMerchantSelected} />

          </div>
          <div
            hidden={this.state.isBrandDetailFormHidden}>
            <BrandDetails merchantSelected={this.state.merchantSelected} />
          </div>
        </div>
      </div>
    );
  }
}