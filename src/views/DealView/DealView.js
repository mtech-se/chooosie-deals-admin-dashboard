import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { DealDetailsForm } from '../../components/DealDetailsForm/DealDetailsForm';
import { DisplayMerchantBrandOutlet } from '../../components/DisplayMerchantBrandOutlet/DisplayMerchantBrandOutlet';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { DealFactory } from '../../factories/DealFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { Deal } from '../../models/Deal';
import { DealService } from '../../services/DealService';

class BaseDealView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dealId: undefined,
      dealTitle: undefined,
      dealCategory: undefined,
      dealQuantity: undefined,
      dealFinePrint: undefined,
      dealDescription: undefined,
      dealOriginalPrice: undefined,
      dealDiscountedPrice: undefined,
      dealAutoPublishDate: undefined,
      dealOfTheMonthFlag: undefined,
      dealRedemptionExpiryDate: undefined,
      dealRedemptionInformation: undefined,
      isMakingNetworkCall: false,
      dealSelected: undefined,
    };

    this.onDealEntered = this.onDealEntered.bind(this);
    this.updateDeal = this.updateDeal.bind(this);
    this.deleteDeal = this.deleteDeal.bind(this);
  }

  componentDidMount() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await DealService.getDeal(this.props.match.params.dealId);
      const deal = DealFactory.createFromJson(response.data);

      this.setState({
        dealId: deal.id,
        dealTitle: deal.title,
        dealDescription: deal.description,
        dealOriginalPrice: deal.originalPrice,
        dealDiscountedPrice: deal.discountedPrice,
        dealQuantity: deal.maxQuantity,
        dealRedemptionInformation: deal.redemptionInstructions,
        dealFinePrint: deal.termsOfUse,
        dealCategory: deal.categoryDeals,
        dealCuisineDeals: deal.cuisineDeals,
        dealAutoPublishDate: deal.autoPublishDate,
        dealRedemptionExpiryDate: deal.redemptionExpiryDate,
        dealOfTheMonthFlag: deal.dealOfTheMonth,
        isMakingNetworkCall: false,
        dealSelected: deal,
      });
    });
  }

  /** @param {Deal} deal */
  onDealEntered(deal) {
    this.setState({
      dealTitle: deal.title,
      dealDescription: deal.description,
      dealOriginalPrice: deal.originalPrice,
      dealDiscountedPrice: deal.discountedPrice,
      dealQuantity: deal.maxQuantity,
      dealRedemptionInformation: deal.redemptionInstructions,
      dealFinePrint: deal.termsOfUse,
      dealCategory: deal.categoryDeals,
      dealCuisineDeals: deal.cuisineDeals,
      dealAutoPublishDate: deal.autoPublishDate,
      dealRedemptionExpiryDate: deal.redemptionExpiryDate,
      dealOfTheMonthFlag: deal.dealOfTheMonth,
    });
  }

  updateDeal() {
    this.setState({
      disableButton: true
    }, async () => {

      const deal = new Deal(
        undefined,
        this.state.dealTitle,
        this.state.dealDescription,
        this.state.dealOriginalPrice,
        this.state.dealDiscountedPrice,
        undefined,
        this.state.dealQuantity,
        undefined,
        this.state.dealRedemptionInformation,
        this.state.dealFinePrint,
        undefined,
        this.state.dealOfTheMonthFlag,
        undefined,
        this.state.dealCategory,
        this.state.dealCuisineDeals,
        this.state.dealAutoPublishDate,
        this.state.dealRedemptionExpiryDate,
        undefined,
        undefined,
        undefined,
        undefined,
      );

      const requestBody = deal.toJson();

      const response = await DealService.updateDeal(this.state.dealId, requestBody);
      if (response.status === 200) this.props.history.push('/deals');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteDeal() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await DealService.deleteDeal(this.state.dealSelected.id);
      if (response.status === 204) this.props.history.push('/deals');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader />;

    if (this.state.dealId === undefined) return null;

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px'
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('DEALS', '/deals'),
          new Breadcrumb(this.state.dealTitle.toUpperCase(), '/deals/' + this.state.dealSelected.id)]} />

        <div style={{
          padding: '8px',
        }}>

          <PanelTitle title={'Edit Deal Details - #' + this.state.dealId} />

          <DisplayMerchantBrandOutlet dealSelected={this.state.dealSelected} />

          <DealDetailsForm
            deal={this.state.dealSelected}
            onDealEntered={this.onDealEntered}
          />

          <div style={{
            display: 'flex',
            flexDirection: 'row'
          }}>
            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Update Deal'}
                onClick={this.updateDeal}
                disabled={this.state.disableButton}
                colour={ColorConstants.teal}
              />
            </div>

            <div>
              <StylisedButton
                title={'Delete Deal'}
                onClick={this.deleteDeal}
                disabled={this.state.disableButton}
                colour={ColorConstants.red}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BaseDealView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const DealView = withRouter(BaseDealView);