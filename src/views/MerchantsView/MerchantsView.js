import React from 'react';
import { Link } from 'react-router-dom';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { MerchantsTable } from '../../components/DataTable/MerchantsTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class MerchantsView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      disableButton: true,
      merchantsSelected: undefined
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onMerchantsSelected = this.onMerchantsSelected.bind(this);
    this.deleteMerchant = this.deleteMerchant.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  onMerchantsSelected(merchantsSelected) {
    this.setState({
        disableButton: false,
      merchantsSelected
      }, () => {
        console.log(this.state.merchantsSelected);
      }
    );
  }

  deleteMerchant() {
    this.setState({
        disableButton: true
      }, () => {
        console.log('Delete these merchants');
      }
    );
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('MERCHANTS', '/merchants')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            display: 'flex',
            alignItems: 'baseline',
          }}>

            <StylisedButton
              title={'Create Merchant'}
              colour={ColorConstants.primaryPurple}
              variant={'contained'}
              component={Link}
              to={'merchants/create/'}
            />

            <div style={{
              flexGrow: '3',
              display: 'flex',
              alignItems: 'baseline',
              justifyContent: 'flex-end',
            }}>
              <SearchField
                style={{ margin: '8px 8px' }}
                onChange={this.onSearchTermChange}
              />

              {/*<StylisedButton*/}
              {/*title={'Delete'}*/}
              {/*onClick={this.deleteMerchant}*/}
              {/*disabled={this.state.disableButton}*/}
              {/*colour={ColorConstants.red}*/}
              {/*/>*/}
            </div>
          </div>

          <MerchantsTable
            searchTerm={this.state.searchTerm}
            onMerchantsSelected={this.onMerchantsSelected}
          />
        </div>
      </div>
    );
  }
}