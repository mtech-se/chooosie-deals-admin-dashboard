import { shallow } from 'enzyme';
import React from 'react';
import { MerchantsView } from './MerchantsView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<MerchantsView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});