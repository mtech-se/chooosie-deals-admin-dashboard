import { shallow } from 'enzyme';
import React from 'react';
import { CategoryView } from './CategoryView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<CategoryView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});