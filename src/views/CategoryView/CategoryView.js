import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CategoryDetailsForm } from '../../components/CategoryDetailsForm/CategoryDetailsForm';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { CategoryFactory } from '../../factories/CategoryFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { CategoryService } from '../../services/CategoryService';

class BaseCategoryView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categoryName: '',
      disableButton: false,
      isMakingNetworkCall: false,
      categorySelected: undefined
    };

    this.onCategoryEntered = this.onCategoryEntered.bind(this);
    this.updateCategory = this.updateCategory.bind(this);
    this.deleteCategory = this.deleteCategory.bind(this);
  }

  componentDidMount() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await CategoryService.getCategory(this.props.match.params.categoryId);
      const categorySelected = CategoryFactory.createFromJson(response.data);

      this.setState({
        categorySelected,
        isMakingNetworkCall: false
      });
    });
  }

  updateCategory() {
    this.setState({
      disableButton: true
    }, async () => {

      const requestBody = {
        name: this.state.categoryName,
        id: this.state.categorySelected.id
      };

      const response = await CategoryService.updateCategory(this.state.categorySelected.id, requestBody);
      if (response.status === 200) this.props.history.push('/categories');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteCategory() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await CategoryService.deleteCategory(this.state.categorySelected.id);
      if (response.status === 204) this.props.history.push('/categories');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  onCategoryEntered(categoryName) {
    this.setState({
      categoryName
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader />;

    if (this.state.categorySelected === undefined) return null;

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px',
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CATEGORIES', '/categories'),
          new Breadcrumb(this.state.categorySelected.name.toUpperCase(), '/brands/' + this.state.categorySelected.id)]} />

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'Edit Category Details'} />

          <CategoryDetailsForm categorySelected={this.state.categorySelected}
                               onCategoryEntered={this.onCategoryEntered} />

          <div style={{
            display: 'flex',
            flexDirection: 'row'
          }}>
            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Update Category'}
                onClick={this.updateCategory}
                disabled={this.state.disableButton}
                colour={ColorConstants.teal}
              />
            </div>

            <div>
              <StylisedButton
                title={'Delete Category'}
                onClick={this.deleteCategory}
                disabled={this.state.disableButton}
                colour={ColorConstants.red}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BaseCategoryView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const CategoryView = withRouter(BaseCategoryView);