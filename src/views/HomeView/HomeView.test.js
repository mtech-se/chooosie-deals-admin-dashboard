import { shallow } from 'enzyme';
import React from 'react';
import { HomeView } from './HomeView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<HomeView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});