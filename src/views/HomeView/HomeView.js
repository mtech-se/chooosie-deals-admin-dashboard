import React from 'react';
import { Redirect } from 'react-router';

export const HomeView = () => {
  return <Redirect to={'/overview'} />;
};