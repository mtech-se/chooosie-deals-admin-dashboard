import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CuisineDetails } from '../../components/CuisineDetails/CuisineDetails';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CreateCuisineView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px'
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CUISINES', '/cuisines'),
          new Breadcrumb('CREATE CUISINE', '/cuisines/create')]} />

        <div style={{
          padding: '8px',
        }}>

          <PanelTitle title={'New Cuisine Details'} />

          <CuisineDetails />
        </div>
      </div>
    );
  }
}