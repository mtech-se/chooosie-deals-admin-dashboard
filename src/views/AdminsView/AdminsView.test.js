import { shallow } from 'enzyme';
import React from 'react';
import { AdminsView } from './AdminsView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<AdminsView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});