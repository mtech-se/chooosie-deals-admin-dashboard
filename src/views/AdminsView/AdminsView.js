import React from 'react';
import { Link } from 'react-router-dom';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { AdminsTable } from '../../components/DataTable/AdminsTable';
import { SearchField } from '../../components/DataTable/SearchField/SearchField';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { Breadcrumb } from '../../models/Breadcrumb';

export class AdminsView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: '',
      disableButton: true,
      adminsSelected: undefined
    };

    this.onSearchTermChange = this.onSearchTermChange.bind(this);
    this.onAdminsSelected = this.onAdminsSelected.bind(this);
    this.deleteAdmin = this.deleteAdmin.bind(this);
  }

  onSearchTermChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  onAdminsSelected(adminsSelected) {
    this.setState({
        disableButton: false,
        adminsSelected
      }, () => {
        console.log(this.state.adminsSelected);
      }
    );
  }

  deleteAdmin() {
    this.setState({
        disableButton: true
      }, () => {
        console.log('Delete these admins');
      }
    );
  }

  render() {
    return (
      <div style={{
        padding: '8px',
      }}>

        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('ADMINS', '/admins')]}
        />

        <div style={{
          padding: '8px',
          borderRadius: '4px',
          backgroundColor: ColorConstants.white
        }}>

          <div style={{
            display: 'flex',
            alignItems: 'baseline',
          }}>

            <StylisedButton
              title={'Create Admin'}
              colour={ColorConstants.primaryPurple}
              variant={'contained'}
              component={Link}
              to={'admins/create/'}
            />

            <div style={{
              flexGrow: '3',
              display: 'flex',
              alignItems: 'baseline',
              justifyContent: 'flex-end',
            }}>
              <SearchField
                style={{ margin: '8px 8px' }}
                onChange={this.onSearchTermChange}
              />

              <StylisedButton
                title={'Delete'}
                onClick={this.deleteAdmin}
                disabled={this.state.disableButton}
                colour={ColorConstants.red}
              />
            </div>
          </div>

          <AdminsTable
            searchTerm={this.state.searchTerm}
            onAdminsSelected={this.onAdminsSelected}
          />
        </div>
      </div>
    );
  }
}