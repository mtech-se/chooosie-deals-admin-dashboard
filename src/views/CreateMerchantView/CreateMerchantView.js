import React from 'react';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { MerchantDetails } from '../../components/MerchantDetails/MerchantDetails';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { Breadcrumb } from '../../models/Breadcrumb';

export class CreateMerchantView extends React.Component {
  render() {
    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px'
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('MERCHANTS', '/merchants'),
          new Breadcrumb('CREATE MERCHANT', '/merchants/create')]} />

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'New Merchant Details'} />

          <MerchantDetails />
        </div>
      </div>
    );
  }
}