import { shallow } from 'enzyme';
import React from 'react';
import { CuisineView } from './CuisineView';

describe('CustomersView', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<CuisineView />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});