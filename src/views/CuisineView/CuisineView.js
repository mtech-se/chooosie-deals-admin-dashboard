import React from 'react';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import { PositionedBreadcrumbs } from '../../components/Breadcrumbs/PositionedBreadcrumbs';
import { CuisineDetailsForm } from '../../components/CuisineDetailsForm/CuisineDetailsForm';
import { PanelTitle } from '../../components/PanelTitle/PanelTitle';
import { ProgressLoader } from '../../components/ProgressLoader/ProgressLoader';
import { StylisedButton } from '../../components/StylisedButton/StylisedButton';
import { ColorConstants } from '../../constants/ColorConstants';
import { CuisineFactory } from '../../factories/CuisineFactory';
import { Breadcrumb } from '../../models/Breadcrumb';
import { CuisineService } from '../../services/CuisineService';

class BaseCuisineView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cuisineName: '',
      cuisineDescription: '',
      disableButton: false,
      isMakingNetworkCall: false,
      cuisineSelected: undefined
    };

    this.onCuisineEntered = this.onCuisineEntered.bind(this);
    this.updateCuisine = this.updateCuisine.bind(this);
    this.deleteCuisine = this.deleteCuisine.bind(this);
  }

  componentDidMount() {
    this.setState({ isMakingNetworkCall: true }, async () => {
      const response = await CuisineService.getCuisine(this.props.match.params.cuisineId);
      const cuisineSelected = CuisineFactory.createFromJson(response.data);

      this.setState({
        cuisineSelected,
        isMakingNetworkCall: false
      });
    });
  }

  updateCuisine() {
    this.setState({
      disableButton: true
    }, async () => {

      const requestBody = {
        name: this.state.cuisineName,
        description: this.state.cuisineDescription,
        id: this.state.cuisineSelected.id
      };

      const response = await CuisineService.updateCuisine(this.state.cuisineSelected.id, requestBody);
      if (response.status === 200) this.props.history.push('/cuisines');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  deleteCuisine() {
    this.setState({
      disableButton: true
    }, async () => {

      const response = await CuisineService.deleteCuisine(this.state.cuisineSelected.id);
      if (response.status === 204) this.props.history.push('/cuisines');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  onCuisineEntered(cuisineName, cuisineDescription) {
    this.setState({
      cuisineName,
      cuisineDescription
    });
  }

  render() {
    if (this.state.isMakingNetworkCall) return <ProgressLoader />;

    if (this.state.cuisineSelected === undefined) return null;

    return (
      <div style={{
        backgroundColor: 'white',
        margin: '8px',
        borderRadius: '4px',
      }}>
        <PositionedBreadcrumbs breadcrumbs={[
          new Breadcrumb('OVERVIEW', '/overview'),
          new Breadcrumb('CUISINES', '/cuisines'),
          new Breadcrumb(this.state.cuisineSelected.name.toUpperCase(), '/brands/' + this.state.cuisineSelected.id)]} />

        <div style={{
          padding: '8px',
        }}>
          <PanelTitle title={'Edit Cuisine Details'} />

          <CuisineDetailsForm cuisineSelected={this.state.cuisineSelected}
                              onCuisineEntered={this.onCuisineEntered} />

          <div style={{
            display: 'flex',
            flexDirection: 'row'
          }}>
            <div style={{
              marginRight: '8px',
            }}>
              <StylisedButton
                title={'Update Cuisine'}
                onClick={this.updateCuisine}
                disabled={this.state.disableButton}
                colour={ColorConstants.teal}
              />
            </div>

            <div>
              <StylisedButton
                title={'Delete Cuisine'}
                onClick={this.deleteCuisine}
                disabled={this.state.disableButton}
                colour={ColorConstants.red}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BaseCuisineView.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const CuisineView = withRouter(BaseCuisineView);