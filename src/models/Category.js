export class Category {
  /**
   *
   * @param {number} id
   * @param {string} name
   * @param {string} description
   * @param {EntityPhoto[]} entityPhotos
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    name,
    description,
    entityPhotos,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.entityPhotos = entityPhotos;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }

  /** @returns {string} */
  getMainPhotoUrl() {
    if (this.entityPhotos === undefined) return undefined;
    if (this.entityPhotos.length === 0) return undefined;

    return this.entityPhotos[0].getPhotoUrl();
  }
}