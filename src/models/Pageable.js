export class Pageable {
  /**
   *
   * @param {Sort} sort
   * @param {number} offset
   * @param {number} pageNumber
   * @param {number} pageSize
   * @param {boolean} unpaged
   * @param {boolean} paged
   */
  constructor(
    sort,
    offset,
    pageNumber,
    pageSize,
    unpaged,
    paged
  ) {
    this.sort = sort;
    this.offset = offset;
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    this.unpaged = unpaged;
    this.paged = paged;
  }
}