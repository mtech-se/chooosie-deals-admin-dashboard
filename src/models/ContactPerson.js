export class ContactPerson {
  /**
   *
   * @param {number} id
   * @param {string} name
   * @param {string} position
   * @param {string} mobileNumber
   * @param {string} email
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   * @param {moment} createdAt
   */
  constructor(
    id,
    name,
    position,
    mobileNumber,
    email,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.name = name;
    this.position = position;
    this.mobileNumber = mobileNumber;
    this.email = email;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}