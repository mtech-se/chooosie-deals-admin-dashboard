export class OrderItem {
  /**
   *
   * @param {number} id
   * @param {Deal} deal
   * @param {number} spotPrice
   * @param {moment} redeemedAt
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    deal,
    spotPrice,
    redeemedAt,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.deal = deal;
    this.spotPrice = spotPrice;
    this.redeemedAt = redeemedAt;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}