export class Breadcrumb {
  /**
   *
   * @param {string} label
   * @param {string} url
   */
  constructor(label, url) {
    this.label = label;
    this.url = url;
  }
}