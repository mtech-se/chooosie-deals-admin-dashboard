export class Country {
  constructor(json) {
    this.id = json.id;
    this.name = json.name;
  }
}