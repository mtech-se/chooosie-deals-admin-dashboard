export class CuisineDeal {
  /**
   *
   * @param {number} id
   * @param {Cuisine} cuisine
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    cuisine,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.cuisine = cuisine;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}