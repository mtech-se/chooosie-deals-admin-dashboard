export class MerchantWithRedemptionCount {
  /**
   *
   * @param {Merchant} merchant
   * @param {number} numPurchasesRedeemed
   */
  constructor(
    merchant,
    numPurchasesRedeemed
  ) {
    this.merchant = merchant;
    this.numPurchasesRedeemed = numPurchasesRedeemed;
  }
}