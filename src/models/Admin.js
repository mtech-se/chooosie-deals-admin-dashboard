export class Admin {
  /**
   *
   * @param {number} id
   * @param {string} firstName
   * @param {string} lastName
   * @param {string} gender
   * @param {string} email
   * @param {AdminRole[]} adminRoles
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    firstName,
    lastName,
    gender,
    email,
    adminRoles,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.email = email;
    this.adminRoles = adminRoles;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}