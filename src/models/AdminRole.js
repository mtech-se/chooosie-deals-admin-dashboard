export class AdminRole {
  /**
   *
   * @param {number} id
   * @param {Role} role
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    role,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.role = role;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}