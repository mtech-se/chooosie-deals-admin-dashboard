export class Merchant {
  /**
   *
   * @param id
   * @param name
   * @param businessRegistrationNumber
   * @param mailingAddress
   * @param createdAt
   * @param updatedAt
   * @param deletedAt
   * @param contactPersons
   */
  constructor(
    id,
    name,
    businessRegistrationNumber,
    mailingAddress,
    createdAt,
    updatedAt,
    deletedAt,
    contactPersons
  ) {
    this.id = id;
    this.name = name;
    this.businessRegistrationNumber = businessRegistrationNumber;
    this.mailingAddress = mailingAddress;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
    this.contactPersons = contactPersons;
  }
}