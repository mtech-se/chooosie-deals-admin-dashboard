export class OverviewStats {
  /**
   *
   * @param {number} totalPublishedDeals
   * @param {number} totalNewCustomers
   * @param {number} totalPurchasedDeals
   * @param {number} totalSales
   * @param {number} totalPayouts
   * @param {number} totalCommissions
   */
  constructor(totalPublishedDeals, totalNewCustomers, totalPurchasedDeals, totalSales, totalPayouts, totalCommissions) {
    this.totalPublishedDeals = totalPublishedDeals;
    this.totalNewCustomers = totalNewCustomers;
    this.totalPurchasedDeals = totalPurchasedDeals;
    this.totalSales = totalSales;
    this.totalPayouts = totalPayouts;
    this.totalCommissions = totalCommissions;
  }
}