export class Photo {
  /**
   *
   * @param {number} id
   * @param {string} path
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    path,
    createdAt,
    updatedAt,
    deletedAt
  ) {
    this.id = id;
    this.path = path;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}