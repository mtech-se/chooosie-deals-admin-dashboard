export class Customer {
  /**
   *
   * @param id
   * @param firstName
   * @param lastName
   * @param email
   * @param phoneNumber
   * @param gender
   * @param billingFirstName
   * @param billingLastName
   * @param billingCellphone
   * @param billingTelephone
   * @param billingAddressLineOne
   * @param billingAddressLineTwo
   * @param billingAddressPostalCode
   * @param createdAt
   * @param deletedAt
   */
  constructor(
    id,
    firstName,
    lastName,
    email,
    gender,
    billingFirstName,
    billingLastName,
    billingCellphone,
    billingTelephone,
    billingAddressLineOne,
    billingAddressLineTwo,
    billingAddressPostalCode,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.name = firstName + ' ' + lastName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.gender = gender;
    this.billingFirstName = billingFirstName;
    this.billingLastName = billingLastName;
    this.billingCellphone = billingCellphone;
    this.billingTelephone = billingTelephone;
    this.billingAddressLineOne = billingAddressLineOne;
    this.billingAddressLineTwo = billingAddressLineTwo;
    this.billingAddressPostalCode = billingAddressPostalCode;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}