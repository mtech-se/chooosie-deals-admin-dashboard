export class PaginationRequestParams {
  /**
   *
   * @param {number} size
   * @param {number} page
   */
  constructor(size, page) {
    // this.size = size === undefined ? undefined : Number.parseInt(size, 10);
    // this.page = page === undefined ? undefined : Number.parseInt(page, 10);
    this.size = size === undefined ? undefined : size;
    this.page = page === undefined ? undefined : page;
  }
}