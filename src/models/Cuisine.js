export class Cuisine {
  /**
   *
   * @param id
   * @param name
   * @param description
   * @param createdAt
   * @param updatedAt
   * @param deletedAt
   */
  constructor(
    id,
    name,
    description,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}