export class CategoryWithPublishedDealCount {
  /**
   *
   * @param {Category} category
   * @param {number} numPublishedDeals
   */
  constructor(category, numPublishedDeals) {
    this.category = category;
    this.numPublishedDeals = numPublishedDeals;
  }
}