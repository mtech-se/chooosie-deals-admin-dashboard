export class CategoryDeal {
  /**
   *
   * @param {number} id
   * @param {Category} category
   * @param {moment} createdAt
   * @param {moment} updatedAt
   * @param {moment} deletedAt
   */
  constructor(
    id,
    category,
    createdAt,
    updatedAt,
    deletedAt,
  ) {
    this.id = id;
    this.category = category;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}