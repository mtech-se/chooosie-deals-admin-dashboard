import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class PhotoService {
  /**
   *
   * @param {number} photoId
   * @returns {AxiosPromise}
   */
  static deletePhoto(photoId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(ApiConstants.BASE_URL + '/photos/' + photoId);
  }

  /**
   *
   * @param {number} photoId
   * @returns {AxiosPromise}
   */
  static updatePhoto(updateUrl, formData) {
    const config = {
      headers: {
        'Content-type': 'multipart/form-data'
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + updateUrl + '/photos', formData, config);

  }
}
