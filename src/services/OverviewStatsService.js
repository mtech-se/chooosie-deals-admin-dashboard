import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class OverviewStatsService {
  /**
   *
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Promise<*>}
   */
  static getOverviewStats(startDate, endDate) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + `/reports/overview-stats?startDate=${startDate}&endDate=${endDate}`);
  }

  /**
   *
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Promise<*>}
   */
  static getTopDealsByPurchaseCount(startDate, endDate) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + `/reports/top-deals-by-purchase-count?startDate=${startDate}&endDate=${endDate}`);
  }

  /**
   *
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Promise<*>}
   */
  static getTopCategoriesByPublishedDealCount(startDate, endDate) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + `/reports/top-categories-by-published-deal-count?startDate=${startDate}&endDate=${endDate}`);
  }

  /**
   *
   * @param {string} startDate
   * @param {string} endDate
   * @returns {Promise<*>}
   */
  static getTopMerchantsByPurchaseRedemptionCount(startDate, endDate) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + `/reports/top-merchants-by-purchase-redemption-count?startDate=${startDate}&endDate=${endDate}`);
  }
}