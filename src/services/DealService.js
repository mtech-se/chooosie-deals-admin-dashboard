import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class DealService {
  /**
   *
   * @param {number} size
   * @param {number} page
   * @param {string} title
   * @returns {Promise<*>}
   */
  static getDeals(size = undefined, page = undefined, title = undefined) {
    let url = `${ApiConstants.BASE_URL}/deals`;

    const axiosOptions = {
      params: {
        size,
        page,
        title,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param {number} dealId
   * @returns {AxiosPromise<any>}
   */
  static getDeal(dealId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/deals/' + dealId);
  }

  static createDeal(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/deals', data);
  }

  /**
   *
   * @param dealId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateDeal(dealId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/deals/' + dealId, data);
  }

  /**
   *
   * @param dealId
   * @returns {AxiosPromise}
   */
  static deleteDeal(dealId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(ApiConstants.BASE_URL + '/deals/' + dealId);
  }
}