import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class AdminService {
  /**
   *
   * @param {number} size
   * @param {number} page
   * @param {string} name
   * @returns {AxiosPromise<any>}
   */
  static getAdmins(size = undefined, page = undefined, name = undefined) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/admins');
  }

  /**
   *
   * @param {number} id
   * @returns {AxiosPromise<any>}
   */
  static getAdmin(id) {
    return AxiosSingleton.getAuthAxiosInstance().get(`${ApiConstants.BASE_URL}/admins/${id}`);
  }

  /**
   *
   * @param {object} data
   * @returns {AxiosPromise<any>}
   */
  static createAdmin(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(`${ApiConstants.BASE_URL}/admins`, data);
  }

  /**
   *
   * @param {number} id
   * @param {object} data
   * @returns {AxiosPromise<any>}
   */
  static updateAdmin(id, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(`${ApiConstants.BASE_URL}/admins/${id}`, data);
  }

  /**
   *
   * @param {number} id
   * @returns {AxiosPromise<any>}
   */
  static deleteAdmin(id) {
    return AxiosSingleton.getAuthAxiosInstance().delete(`${ApiConstants.BASE_URL}/admins/${id}`);
  }
}