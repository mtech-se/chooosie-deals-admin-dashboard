import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class OutletService {
  /**
   *
   * @returns {Promise<*>}
   */
  static getAllOutlets() {
    return this.getOutlets(undefined, undefined, undefined);
  }

  /**
   *
   * @param size
   * @param page
   * @param name
   * @returns {AxiosPromise<any>}
   */
  static getOutlets(size = undefined, page = undefined, name = undefined) {
    let url = `${ApiConstants.BASE_URL}/outlets`;

    const axiosOptions = {
      params: {
        size,
        page,
        name,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param {number} brandId
   * @returns {AxiosPromise<any>}
   */
  static getOutletsForThisBrand(brandId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/outlets?brand_id=' + brandId);
  }

  /**
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static createOutlet(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/outlets', data);
  }

  /**
   *
   * @param outletId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateOutlet(outletId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/outlets/' + outletId, data);
  }

  /**
   *
   * @param outletId
   * @returns {AxiosPromise}
   */
  static deleteOutlet(outletId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(ApiConstants.BASE_URL + '/outlets/' + outletId);
  }

  /**
   *
   * @param {number} outletId
   * @returns {AxiosPromise<any>}
   */
  static getOutlet(outletId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/outlets/' + outletId);
  }
}