import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class BrandService {
  /**
   *
   * @returns {Promise<*>}
   */
  static getAllBrands() {
    return this.getBrands(undefined, undefined, undefined);
  }

  /**
   *
   * @param size
   * @param page
   * @param name
   * @returns {AxiosPromise<any>}
   */
  static getBrands(size = undefined, page = undefined, name = undefined) {
    let url = `${ApiConstants.BASE_URL}/brands`;

    const axiosOptions = {
      params: {
        size,
        page,
        name,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param {number} merchantId
   * @returns {AxiosPromise<any>}
   */
  static getBrandsForThisMerchant(merchantId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/brands?merchant_id=' + merchantId);
  }

  /**
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static createBrand(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/brands', data);
  }

  /**
   *
   * @param brandId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateBrand(brandId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/brands/' + brandId, data);
  }

  /**
   *
   * @param brandId
   * @returns {AxiosPromise}
   */
  static deleteBrand(brandId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(ApiConstants.BASE_URL + '/brands/' + brandId);
  }

  /**
   *
   * @param {number} brandId
   * @returns {AxiosPromise<any>}
   */
  static getBrand(brandId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/brands/' + brandId);
  }
}
