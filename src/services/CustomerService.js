import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class CustomerService {
  /**
   *
   * @param size
   * @param page
   * @param name
   * @returns {AxiosPromise<any>}
   */
  static getCustomers(size = undefined, page = undefined, name = undefined) {
    let url = `${ApiConstants.BASE_URL}/customers`;

    const axiosOptions = {
      params: {
        size,
        page,
        name,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param {number} customerId
   * @returns {AxiosPromise<any>}
   */
  static getCustomer(customerId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/customers/' + customerId);
  }

  /**
   *
   * @param customerId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateCustomer(customerId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/customers/' + customerId, data);
  }

  /**
   *
   * @param customerId
   * @returns {AxiosPromise}
   */
  static deleteCustomer(customerId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(ApiConstants.BASE_URL + '/customers/' + customerId);
  }

}