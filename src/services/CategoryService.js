import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class CategoryService {
  /**
   *
   * @returns {Promise<*>}
   */
  static getAllCategories() {
    return this.getCategories(undefined, undefined, undefined);
  }

  /**
   *
   * @param size
   * @param page
   * @param name
   * @returns {AxiosPromise<any>}
   */
  static getCategories(size = undefined, page = undefined, name = undefined) {
    let url = `${ApiConstants.BASE_URL}/categories`;

    const axiosOptions = {
      params: {
        size,
        page,
        name,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static createCategory(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/categories', data);
  }

  /**
   *
   * @param categoryId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateCategory(categoryId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/categories/' + categoryId, data);
  }

  /**
   *
   * @param categoryId
   * @returns {AxiosPromise}
   */
  static deleteCategory(categoryId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(ApiConstants.BASE_URL + '/categories/' + categoryId);
  }

  /**
   *
   * @param {number} categoryId
   * @returns {AxiosPromise<any>}
   */
  static getCategory(categoryId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/categories/' + categoryId);
  }
}