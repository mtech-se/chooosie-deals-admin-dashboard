import AxiosSingleton from '../AxiosSingleton';

import { ApiConstants } from '../constants/ApiConstants';

export class MerchantService {
  /**
   *
   * @param {number} size
   * @param {number} page
   * @param {string} name
   * @returns {Promise<AxiosPromise<any>>}
   */
  static getMerchants(size = undefined, page = undefined, name = undefined) {
    let url = `${ApiConstants.BASE_URL}/merchants`;

    const axiosOptions = {
      params: {
        size,
        page,
        name,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static createMerchant(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(
      ApiConstants.BASE_URL + '/merchants',
      data
    );
  }

  /**
   *
   * @param merchantId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateMerchant(merchantId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(
      ApiConstants.BASE_URL + '/merchants/' + merchantId,
      data,
    );
  }

  /**
   *
   * @param merchantId
   * @returns {AxiosPromise}
   */
  static deleteMerchant(merchantId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(
      ApiConstants.BASE_URL + '/merchants/' + merchantId,
    );
  }

  /**
   *
   * @param {number} merchantId
   * @returns {AxiosPromise<any>}
   */
  static getMerchant(merchantId) {
    return AxiosSingleton.getAuthAxiosInstance().get(
      ApiConstants.BASE_URL + '/merchants/' + merchantId,
    );
  }
}