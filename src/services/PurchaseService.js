import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class PurchaseService {
  /**
   *
   * @param {number} size
   * @param {number} page
   * @param {string} name
   * @returns {Promise<AxiosPromise<any>>}
   */
  static getPurchases(size = undefined, page = undefined, name = undefined) {
    let url = `${ApiConstants.BASE_URL}/purchases`;

    const axiosOptions = {
      params: {
        size,
        page,
        name,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param {number} purchaseId
   * @returns {AxiosPromise<any>}
   */
  static getPurchase(purchaseId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/purchases/' + purchaseId);
  }

}