import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class UserService {
  /**
   *
   * @returns {AxiosPromise<any>}
   */
  static getMyDetails() {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/users');
  }

  static createUser(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/user/signup', data);
  }
}