import AxiosSingleton from '../AxiosSingleton';
import { ApiConstants } from '../constants/ApiConstants';

export class CuisineService {
  /**
   *
   * @returns {Promise<*>}
   */
  static getAllCuisines() {
    return this.getCuisines(undefined, undefined, undefined);
  }

  /**
   *
   * @param size
   * @param page
   * @param name
   * @returns {AxiosPromise<any>}
   */
  static getCuisines(size = undefined, page = undefined, name = undefined) {
    let url = `${ApiConstants.BASE_URL}/cuisines`;

    const axiosOptions = {
      params: {
        size,
        page,
        name,
      }
    };

    return AxiosSingleton.getAuthAxiosInstance().get(url, axiosOptions);
  }

  /**
   *
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static createCuisine(data) {
    return AxiosSingleton.getAuthAxiosInstance().post(ApiConstants.BASE_URL + '/cuisines', data);
  }

  /**
   *
   * @param cuisineId
   * @param data
   * @returns {AxiosPromise<any>}
   */
  static updateCuisine(cuisineId, data) {
    return AxiosSingleton.getAuthAxiosInstance().put(ApiConstants.BASE_URL + '/cuisines/' + cuisineId, data);
  }

  /**
   *
   * @param cuisineId
   * @returns {AxiosPromise}
   */
  static deleteCuisine(cuisineId) {
    return AxiosSingleton.getAuthAxiosInstance().delete(ApiConstants.BASE_URL + '/cuisines/' + cuisineId);
  }

  /**
   *
   * @param cuisineId
   * @returns {AxiosPromise<any>}
   */
  static getCuisine(cuisineId) {
    return AxiosSingleton.getAuthAxiosInstance().get(ApiConstants.BASE_URL + '/cuisines/' + cuisineId);
  }
}