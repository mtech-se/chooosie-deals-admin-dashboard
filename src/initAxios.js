import axios from 'axios';
import { LocalStorageConstants } from './constants/LocalStorageConstants';
import { NoAccessTokenFoundError } from './errors/NoAccessTokenFoundError';
import { history } from './history';

export const addRequestInterceptor = () => {
  const accessTokenPrefix = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN_PREFIX);
  const accessToken = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN);

  if (accessTokenPrefix === undefined || accessToken === undefined) throw new NoAccessTokenFoundError('No access token found in local storage');

  axios.interceptors.request.use(config => {
    config.headers = {
      'Authorization': accessTokenPrefix + accessToken
    };

    return config;
  }, error => Promise.reject(error));
};

export const addResponseInterceptor = () => {
  axios.interceptors.response.use(
    response => response,
    error => {
      if (error.response !== undefined) {
        const statusCode = error.response.status;
        const errors = error.response.data.errors;

        if (statusCode === 403) {
          history.push('/login');
          return Promise.reject('Request access token is invalid');
        }

        if (statusCode === 404) {
          // history.push('/error404');
          return Promise.reject(errors);
        }

        if (statusCode === 422) {
          return Promise.reject(errors);
        }

        if (statusCode === 500) {
          return Promise.reject(errors);
        }
      } else if (error.request !== undefined) {
        const message = 'No response from backend server - is the server down?';
        // history.push(`/errors/no-response?message=${message}`);
        return Promise.reject(message);
      } else {
        return Promise.reject('No response from backend server - is the server down?');
      }

      throw new Error('Unknown issue why Axios failed');
    }
  );
};