import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import { history } from './history';
import { AdminsView } from './views/AdminsView/AdminsView';
import { AdminView } from './views/AdminView/AdminView';
import { AuthBaseView } from './views/AuthBaseView';
import { BrandsView } from './views/BrandsView/BrandsView';
import { BrandView } from './views/BrandView/BrandView';
import { CategoriesView } from './views/CategoriesView/CategoriesView';
import { CategoryView } from './views/CategoryView/CategoryView';
import { CreateAdminView } from './views/CreateAdminView/CreateAdminView';
import { CreateBrandView } from './views/CreateBrandView/CreateBrandView';
import { CreateCategoryView } from './views/CreateCategoryView/CreateCategoryView';
import { CreateCuisineView } from './views/CreateCuisineView/CreateCuisineView';
import { CreateDealView } from './views/CreateDealView/CreateDealView';
import { CreateMerchantView } from './views/CreateMerchantView/CreateMerchantView';
import { CreateOutletView } from './views/CreateOutletView/CreateOutletView';
import { CuisinesView } from './views/CuisinesView/CuisinesView';
import { CuisineView } from './views/CuisineView/CuisineView';
import { CustomersView } from './views/CustomersView/CustomersView';
import { CustomerView } from './views/CustomerView/CustomerView';
import { DealsView } from './views/DealsView/DealsView';
import { DealView } from './views/DealView/DealView';
import { Error404 } from './views/errors/Error404/Error404';
import { NoResponse } from './views/errors/NoResponse/NoResponse';
import { GuestBaseView } from './views/GuestBaseView';
import { HomeView } from './views/HomeView/HomeView';
import { LoginView } from './views/LoginView/LoginView';
import { LogoutView } from './views/LogoutView/LogoutView';
import { MerchantsView } from './views/MerchantsView/MerchantsView';
import { MerchantView } from './views/MerchantView/MerchantView';
import { OutletsView } from './views/OutletsView/OutletsView';
import { OutletView } from './views/OutletView/OutletView';
import { OverviewView } from './views/OverviewView/OverviewView';
import { MyAccountView } from './views/ProfileView/MyAccountView';
import { PurchasesView } from './views/PurchasesView/PurchasesView';
import { SalesSummaryView } from './views/SalesSummaryView/SalesSummaryView';

class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route exact
                 path="/login"
                 render={() => <GuestBaseView component={<LoginView />} />} />

          <Route exact
                 path="/"
                 render={() => <AuthBaseView component={<HomeView />} />} />
          <Route exact
                 path="/overview"
                 render={() => <AuthBaseView component={<OverviewView />} />} />
          <Route exact
                 path="/sales-summary"
                 render={() => <AuthBaseView component={<SalesSummaryView />} />} />

          <Route exact
                 path="/deals"
                 render={() => <AuthBaseView component={<DealsView />} />} />
          <Route exact
                 path="/deals/create"
                 render={() => <AuthBaseView component={<CreateDealView />} />} />
          <Route exact
                 path="/deals/:dealId"
                 render={() => <AuthBaseView component={<DealView />} />} />

          <Route exact
                 path="/purchases"
                 render={() => <AuthBaseView component={<PurchasesView />} />} />
          {/*<Route exact*/}
          {/*path="/purchases/:purchaseId"*/}
          {/*render={() => <AuthBaseView component={<PurchaseView />} />} />*/}

          <Route exact
                 path="/merchants"
                 render={() => <AuthBaseView component={<MerchantsView />} />} />
          <Route exact
                 path="/merchants/create"
                 render={() => <AuthBaseView component={<CreateMerchantView />} />} />
          <Route exact
                 path="/merchants/:merchantId"
                 render={() => <AuthBaseView component={<MerchantView />} />} />

          <Route exact
                 path="/brands"
                 render={() => <AuthBaseView component={<BrandsView />} />} />
          <Route exact
                 path="/brands/create"
                 render={() => <AuthBaseView component={<CreateBrandView />} />} />
          <Route exact
                 path="/brands/:brandId"
                 render={() => <AuthBaseView component={<BrandView />} />} />

          <Route exact
                 path="/outlets"
                 render={() => <AuthBaseView component={<OutletsView />} />} />
          <Route exact
                 path="/outlets/create"
                 render={() => <AuthBaseView component={<CreateOutletView />} />} />
          <Route exact
                 path="/outlets/:outletId"
                 render={() => <AuthBaseView component={<OutletView />} />} />

          <Route exact
                 path="/customers"
                 render={() => <AuthBaseView component={<CustomersView />} />} />
          <Route exact
                 path="/customers/:customerId"
                 render={() => <AuthBaseView component={<CustomerView />} />} />

          <Route exact
                 path="/categories"
                 render={() => <AuthBaseView component={<CategoriesView />} />} />
          <Route exact
                 path="/categories/create"
                 render={() => <AuthBaseView component={<CreateCategoryView />} />} />
          <Route exact
                 path="/categories/:categoryId"
                 render={() => <AuthBaseView component={<CategoryView />} />} />

          <Route exact
                 path="/cuisines"
                 render={() => <AuthBaseView component={<CuisinesView />} />} />
          <Route exact
                 path="/cuisines/create"
                 render={() => <AuthBaseView component={<CreateCuisineView />} />} />
          <Route exact
                 path="/cuisines/:cuisineId"
                 render={() => <AuthBaseView component={<CuisineView />} />} />

          <Route exact
                 path="/admins"
                 render={() => <AuthBaseView component={<AdminsView />} />} />
          <Route exact
                 path="/admins/create"
                 render={() => <AuthBaseView component={<CreateAdminView />} />} />
          <Route exact
                 path="/admins/:adminId"
                 render={() => <AuthBaseView component={<AdminView />} />} />

          <Route exact
                 path="/my-account"
                 render={() => <AuthBaseView component={<MyAccountView />} />} />

          <Route exact
                 path="/errors/no-response"
                 render={() => <GuestBaseView component={<NoResponse />} />} />

          <Route exact
                 path="/logout"
                 render={() => <LogoutView />} />

          <Route component={Error404} />
        </Switch>
      </Router>
    );
  }
}

export default App;
