export class NoAccessTokenFoundError extends Error {
  constructor(message) {
    super();

    this.message = message;
  }
}