import { LocalStorageConstants } from '../../constants/LocalStorageConstants';
import { AdminFactory } from '../../factories/AdminFactory';
import { LoginService } from '../../services/LoginService';
import { UserService } from '../../services/UserService';
import { CLEAR_APP_DATA, INIT_STORE, SET_ADMIN_AND_ACCESS_TOKEN, UPDATE_SEARCH_TERM } from './action-types';

export const logInAdmin = (email, password) => function (dispatch) {
  return LoginService.login(email, password)
    .then(response => {
      const accessToken = response.data.access_token;
      const accessTokenPrefix = response.data.access_token_prefix;
      const admin = AdminFactory.createFromJson(response.data.user);

      return {
        accessToken,
        accessTokenPrefix,
        admin,
      };
    })
    .then(payload => dispatch({
      type: SET_ADMIN_AND_ACCESS_TOKEN,
      payload
    }));
};

export const initStoreWithAccessTokenAndUpdateAdminDetails = () => async function (dispatch) {
  const accessToken = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN);
  const accessTokenPrefix = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN_PREFIX);

  // addRequestInterceptorToAxios(accessToken, accessTokenPrefix);
  // addResponseInterceptorToAxios();

  const response = await UserService.getMyDetails();
  const admin = AdminFactory.createFromJson(response.data);

  dispatch({
    type: INIT_STORE,
    payload: {
      accessToken,
      accessTokenPrefix,
      admin,
    },
  });
};

export const clearAppData = () => ({
  type: CLEAR_APP_DATA,
});

export const updateSearchTerm = payload => ({
  type: UPDATE_SEARCH_TERM,
  payload
});