import { LocalStorageConstants } from '../../constants/LocalStorageConstants';
import { CLEAR_APP_DATA, INIT_STORE, SET_ADMIN_AND_ACCESS_TOKEN, UPDATE_SEARCH_TERM } from '../actions/action-types';
import { initialState } from '../store';

const rootReducer = (state = initialState, action) => {

  if (action.type === SET_ADMIN_AND_ACCESS_TOKEN) {
    const accessToken = action.payload.accessToken;
    const accessTokenPrefix = action.payload.accessTokenPrefix;

    localStorage.setItem(LocalStorageConstants.ACCESS_TOKEN, accessToken);
    localStorage.setItem(LocalStorageConstants.ACCESS_TOKEN_PREFIX, accessTokenPrefix);

    return Object.assign({}, state, {
      admin: action.payload.admin,
      accessToken,
      accessTokenPrefix,
    });
  }

  if (action.type === INIT_STORE) {
    return Object.assign({}, state, {
      accessToken: action.payload.accessToken,
      accessTokenPrefix: action.payload.accessTokenPrefix,
      admin: action.payload.admin,
    });
  }

  if (action.type === CLEAR_APP_DATA) {
    localStorage.clear();

    return state;
  }

  if (action.type === UPDATE_SEARCH_TERM) {
    return Object.assign({}, state, {
      searchTerm: action.payload,
    });
  }

  return state;
};

export default rootReducer;