export class DateUtils {
  static toFriendlyDateString(momentDate) {
    if (momentDate === undefined) return undefined;
    return momentDate.format('DD MMM YYYY');
  }
}