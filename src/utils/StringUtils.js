// const capitalizeFirstLetter = (string) => {
//   return string.charAt(0).toUpperCase() + string.slice(1);
// };

const toTitleCase = string => {
  return string.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

const slugify = string => {
  return string.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w-]+/g, '')       // Remove all non-word chars
    .replace(/--+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
};

/**
 *
 * @param {string} string
 * @param {number} numCharacters
 * @returns {string}
 */
const ellipsify = (string, numCharacters = 10) => {
  if (string === undefined) return undefined;

  return (string.length > numCharacters) ? string.substr(0, numCharacters - 1) + '...' : string;
};

export const StringUtils = {
  // capitalizeFirstLetter,
  toTitleCase,
  slugify,
  ellipsify,
};