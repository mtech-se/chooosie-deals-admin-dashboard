import { shallow } from 'enzyme';

/**
 *
 * @param {function} functionReturningComponent
 */
export const smokeTest = functionReturningComponent => {
  global.console.error = jest.fn();
  const shallowWrapper = shallow(functionReturningComponent());
  expect(shallowWrapper.exists()).toBeTruthy();

  try {
    expect(global.console.error).toHaveBeenCalledTimes(0);
  } catch (e) {
    console.log(global.console.error.mock.calls[0][0]);
    throw new Error(e.message);
  }
};

// export const smokeTestToThrow = (component, error) => {
//   global.console.error = jest.fn();
//
//   expect(() => {
//     shallow(component);
//   }).toThrow(error);
// };