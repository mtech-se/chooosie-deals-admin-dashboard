import axios from 'axios';
import { LocalStorageConstants } from './constants/LocalStorageConstants';
import { history } from './history';

/** @returns {AxiosInstance} */
const getGuestAxiosInstance = () => {
  const axiosInstance = axios.create();
  addResponseInterceptor(axiosInstance);
  return axiosInstance;
};

/** @returns {AxiosInstance} */
const getAuthAxiosInstance = () => {
  const accessToken = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN);
  const accessTokenPrefix = localStorage.getItem(LocalStorageConstants.ACCESS_TOKEN_PREFIX);

  const axiosInstance = axios.create();
  addResponseInterceptor(axiosInstance);

  axiosInstance.interceptors.request.use(config => {
    config.headers = {
      'Authorization': accessTokenPrefix + accessToken
    };

    return config;
  }, error => Promise.reject(error));

  return axiosInstance;
};

const addResponseInterceptor = axiosInstance => {
  axiosInstance.interceptors.response.use(
    response => response,
    error => {
      if (error.response !== undefined) {
        const statusCode = error.response.status;
        const errors = error.response.data.errors;

        if (statusCode === 403) {
          history.push('/login');
          return Promise.reject('Request access token is invalid');
        }

        if (statusCode === 404) {
          // history.push('/error404');
          return Promise.reject(errors);
        }

        if (statusCode === 422) {
          return Promise.reject(errors);
        }

        if (statusCode === 500) {
          return Promise.reject(errors);
        }
      } else if (error.request !== undefined) {
        const message = 'No response from backend server - is the server down?';
        // history.push(`/errors/no-response?message=${message}`);
        return Promise.reject(message);
      } else {
        return Promise.reject('No response from backend server - is the server down?');
      }

      throw new Error('Unknown issue why Axios failed');
    }
  );
};

export default {
  getGuestAxiosInstance,
  getAuthAxiosInstance
};