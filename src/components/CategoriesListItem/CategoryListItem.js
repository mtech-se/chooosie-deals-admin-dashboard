import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import { CategoryWithPublishedDealCount } from '../../models/CategoryWithPublishedDealCount';
import { StringUtils } from '../../utils/StringUtils';

export const CategoryListItem = props => {
  const categoryPhotoUrl = props.categoryWithPublishedDealCount.category.getMainPhotoUrl();

  return (
    <div style={{
      height: '95px',
      padding: '0px 15px',
      border: '1px solid #dddddd',
      backgroundColor: '#ffffff',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    }}>

      <div style={{
        flex: '0 0 60px',
        borderRadius: '8%',
        overflow: 'hidden',
      }}>
        <img
          style={{
            width: '60px',
            height: '60px',
            display: 'block',
          }}
          src={categoryPhotoUrl}
          alt="Logo"
        />
      </div>

      <div style={{
        fontFamily: 'Roboto',
        fontSize: '14px',
        flex: '0 0 150px',
      }}>
        <Link to={'/categories/' + props.categoryWithPublishedDealCount.category.id}>
          {StringUtils.ellipsify(props.categoryWithPublishedDealCount.category.name, 40)}
        </Link>
      </div>

      <div style={{
        fontFamily: 'Roboto',
        fontSize: '14px',
        flex: '0 0 48px',
        textAlign: 'center',
      }}>
        <div style={{
          fontSize: '1.5em',
          fontWeight: '700',
        }}>
          {props.categoryWithPublishedDealCount.numPublishedDeals}
        </div>
        deals
      </div>

    </div>
  );
};

CategoryListItem.propTypes = {
  categoryWithPublishedDealCount: PropTypes.instanceOf(CategoryWithPublishedDealCount).isRequired,
};