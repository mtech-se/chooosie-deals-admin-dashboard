import { shallow } from 'enzyme';
import React from 'react';
import { Category } from '../../models/Category';
import { CategoryWithPublishedDealCount } from '../../models/CategoryWithPublishedDealCount';
import { CategoryListItem } from './CategoryListItem';

describe('', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();

    const categoryWithPublishedDealCount = new CategoryWithPublishedDealCount(
      new Category(
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
      ),
      0
    );

    shallow(
      <CategoryListItem
        categoryWithPublishedDealCount={categoryWithPublishedDealCount}
      />
    );
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});