import { shallow } from 'enzyme';
import React from 'react';
import { LeftNavBarLink } from './LeftNavBarLink';

describe('LeftNavBarLink', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<LeftNavBarLink label={'Test'}
                            url={'test-url'} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });

  // it('it should change color from initial to something else on mouseover', async () => {
  //   // given a shallow rendering of the element
  //   const wrapper = shallow(<LeftNavBarLink label={'Test'} url={'test-url'}/>);
  //   const originalColor = wrapper.find('div').prop('style').backgroundColor;
  //
  //   // when mouseover event happens
  //   wrapper.simulate('mouseover');
  //   await wrapper.update();
  //
  //   // then we expect the color to change
  //   const newColor = wrapper.find('div').prop('style').backgroundColor;
  //   expect(newColor).not.toMatch(originalColor);
  // });
  //
  // it('it should change color to initial on mouseout', async () => {
  //   // given a shallow rendering of the element which has a mouseover
  //   const wrapper = shallow(<LeftNavBarLink label={'Test'} url={'test-url'}/>);
  //   wrapper.simulate('mouseover');
  //   await wrapper.update();
  //   const originalColor = wrapper.find('div').prop('style').backgroundColor;
  //
  //   // when mouseover event happens
  //   wrapper.simulate('mouseout');
  //   await wrapper.update();
  //
  //   // then we expect the color to change
  //   const newColor = wrapper.find('div').prop('style').backgroundColor;
  //   expect(newColor).not.toMatch(originalColor);
  //   expect(newColor).toMatch('initial');
  // });
});