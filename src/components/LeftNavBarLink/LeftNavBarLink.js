import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import { ColorConstants } from '../../constants/ColorConstants';
import './LeftNavBarLink.css';

export class LeftNavBarLink extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isMouseOver: false,
    };
  }

  render() {
    return (
      <Link
        to={this.props.url}
        style={{
          textDecoration: 'none',
          color: ColorConstants.softBlack,
          fontSize: '16px',
          fontWeight: '500',
        }}
        onMouseOver={() => this.setState({ isMouseOver: true })}
        onMouseOut={() => this.setState({ isMouseOver: false })}
      >
        <div className={'LeftNavBarLink'}
             style={{
               height: '47px',
               fontFamily: 'Roboto, Times New Roman, sans-serif',
               display: 'flex',
               alignItems: 'center',
               paddingLeft: '44px',
               textDecoration: 'none',
               background: this.props.isActive ? `linear-gradient(to right, ${ColorConstants.primaryPurple} 2%, ${ColorConstants.translucentPrimaryPurple} 2%)` : 'none',
               // backgroundColor: this.state.isMouseOver ? ColorConstants.translucentPrimaryPurple : 'initial',
             }}>
        <span style={{
          // backgroundColor: ColorConstants.red
        }}>{this.props.label}</span>
        </div>
      </Link>
    );
  }
}

LeftNavBarLink.propTypes = {
  label: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
};

LeftNavBarLink.defaultProps = {
  isActive: false,
};