import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { CategoryService } from '../../services/CategoryService';
import { CategoryDetailsForm } from '../CategoryDetailsForm/CategoryDetailsForm';
import { StylisedButton } from '../StylisedButton/StylisedButton';

class BaseCategoryDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categoryName: '',
      disableButton: false
    };

    this.onCategoryEntered = this.onCategoryEntered.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onCategoryEntered(categoryName) {
    this.setState({
      categoryName
    });
  }

  onFormSubmit() {
    this.setState({
      disableButton: true,
    }, async () => {

      const requestBody = {

        name: this.state.categoryName,
      };

      const response = await CategoryService.createCategory(requestBody);
      if (response.status === 201) this.props.history.push('/categories');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  render() {
    return (
      <div>
        <div>
          <CategoryDetailsForm onCategoryEntered={this.onCategoryEntered} />

          <StylisedButton
            title={'Create Category'}
            onClick={this.onFormSubmit}
            disabled={this.state.disableButton}
            colour={ColorConstants.teal}
          />
        </div>
      </div>
    );
  }
}

BaseCategoryDetails.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
};

export const CategoryDetails = withRouter(BaseCategoryDetails);