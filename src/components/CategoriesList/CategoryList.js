import * as PropTypes from 'prop-types';
import React from 'react';
import { CategoryWithPublishedDealCount } from '../../models/CategoryWithPublishedDealCount';
import { CategoryListItem } from '../CategoriesListItem/CategoryListItem';
import { ProgressLoader } from '../ProgressLoader/ProgressLoader';

export const CategoryList = props => {
  const loadingIndicator = (
    <div style={{
      marginTop: '16px',
      display: 'inline-block',
      marginLeft: '16px',
    }}>
      <ProgressLoader />
    </div>
  );

  let categoriesWithCounts = props.categoriesWithPublishedDealCount;

  const categoryListItems = categoriesWithCounts === undefined ? undefined : categoriesWithCounts.map(categoryWithCount => {
    return (
      <CategoryListItem
        key={categoryWithCount.category.id}
        categoryWithPublishedDealCount={categoryWithCount}
      />
    );
  });

  return (
    <div>
      {props.isLoading ? loadingIndicator : categoryListItems}
    </div>
  );
};

CategoryList.propTypes = {
  categoriesWithPublishedDealCount: PropTypes.arrayOf(PropTypes.instanceOf(CategoryWithPublishedDealCount)),
  isLoading: PropTypes.bool.isRequired,
};
