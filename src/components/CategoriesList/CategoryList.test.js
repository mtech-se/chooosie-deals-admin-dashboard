import { shallow } from 'enzyme';
import React from 'react';
import { CategoryList } from './CategoryList';

describe('CategoryList', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(
      <CategoryList
        categoriesWithPublishedDealCount={[]}
        isLoading={false}
      />
    );
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});