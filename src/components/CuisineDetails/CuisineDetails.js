import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { CuisineService } from '../../services/CuisineService';
import { CuisineDetailsForm } from '../CuisineDetailsForm/CuisineDetailsForm';
import { StylisedButton } from '../StylisedButton/StylisedButton';

class BaseCuisineDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cuisineName: '',
      cuisineDescription: '',
      disableButton: false
    };

    this.onCuisineEntered = this.onCuisineEntered.bind(this);
    this.createCuisine = this.createCuisine.bind(this);
  }

  onCuisineEntered(cuisineName, cuisineDescription) {
    this.setState({
      cuisineName,
      cuisineDescription
    });
  }

  createCuisine() {
    this.setState({
      disableButton: true,
    }, async () => {

      const requestBody = {

        name: this.state.cuisineName,
        description: this.state.cuisineDescription
      };

      const response = await CuisineService.createCuisine(requestBody);
      if (response.status === 201) this.props.history.push('/cuisines');
      else throw new Error('Something went wrong - the response was something other than 201');
    });
  }

  render() {
    return (
      <div>
        <div>
          <CuisineDetailsForm onCuisineEntered={this.onCuisineEntered} />

          <StylisedButton
            title={'Create Cuisine'}
            onClick={this.createCuisine}
            disabled={this.state.disableButton}
            colour={ColorConstants.teal}
          />
        </div>
      </div>
    );
  }
}

BaseCuisineDetails.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
};

export const CuisineDetails = withRouter(BaseCuisineDetails);