import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { Cuisine } from '../../models/Cuisine';
import { LabelField } from '../LabelField/LabelField';

export class CuisineDetailsForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cuisineName: '',
      cuisineDescription: ''
    };

    this.updateParentProps = this.updateParentProps.bind(this);
  }

  updateParentProps() {
    this.props.onCuisineEntered(
      this.state.cuisineName,
      this.state.cuisineDescription,
    );
  }

  componentDidMount() {
    if (this.props.cuisineSelected === undefined || this.props.cuisineSelected === null) return;

    this.setState({
      cuisineName: this.props.cuisineSelected.name,
      cuisineDescription: this.props.cuisineSelected.description
    });
  }

  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
      }}>
        <div style={{
          maxWidth: '33.3%',
        }}>
          <LabelField
            title={'Name'}
            type={'text'}
            value={this.state.cuisineName}
            onChangeHandler={event => {
              this.setState({
                cuisineName: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />
        </div>

        <LabelField
          marginBottom={'16px'}
          title={'Description'}
          type={'text'}
          value={this.state.cuisineDescription}
          onChangeHandler={event => {
            this.setState({
              cuisineDescription: event.target.value
            }, () => {
              this.updateParentProps();
            });
          }}
          required={true}
          multiline={true}
        />
      </div>
    );
  }
}

CuisineDetailsForm.propTypes = {
  cuisineSelected: PropTypes.instanceOf(Cuisine),
  onCuisineEntered: PropTypes.func.isRequired,
};