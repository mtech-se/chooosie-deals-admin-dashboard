import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { OutletService } from '../../services/OutletService';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';
import { StylisedButton } from '../StylisedButton/StylisedButton';

export class BaseOutletDetailsForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      locationName: '',
      locationAddress: '',
    };

    this.createOutlet = this.createOutlet.bind(this);
  }

  createOutlet() {
    this.setState({
      disableButton: true,
    }, async () => {
      const brandId = this.props.brandSelected.value;

      const requestBody = {

        name: this.state.locationName,
        brand: {
          id: brandId
        },
        address: this.state.locationAddress
      };

      const response = await OutletService.createOutlet(requestBody);
      if (response.status === 201) this.props.history.push('/outlets');
      else throw new Error('Something went wrong - the response was something other than 201');
    });
  }

  render() {
    return (
      <div>
        <PanelTitle
          title={'Outlet'}
        />

        <div style={{
          display: 'flex',
          flexDirection: 'column',
        }}>
          <div style={{
            maxWidth: '33.3%',
          }}>
            <LabelField
              title={'Location Name'}
              type={'text'}
              value={this.state.locationName}
              onChangeHandler={event => {
                this.setState({
                  locationName: event.target.value
                });
              }}
            />
          </div>

          <div style={{
            maxWidth: '33.3%',
          }}>
            <LabelField
              marginBottom={'16px'}
              title={'Location Address'}
              type={'text'}
              value={this.state.locationAddress}
              onChangeHandler={event => {
                this.setState({
                  locationAddress: event.target.value
                });
              }}
            />
          </div>
        </div>

        <StylisedButton
          title={'Create Outlet'}
          onClick={this.createOutlet}
          disabled={this.state.disableButton}
          colour={ColorConstants.teal}
        />
      </div>
    );
  }
}

BaseOutletDetailsForm.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
  brandSelected: PropTypes.object,
};

export const OutletDetailsForm = withRouter(BaseOutletDetailsForm);