import * as PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select';
import { MerchantFactory } from '../../factories/MerchantFactory';
import { MerchantService } from '../../services/MerchantService';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class SelectMerchantForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listOfAllMerchants: [],
      merchantSelected: '',
    };

    this.handleMerchantSelected = this.handleMerchantSelected.bind(this);
  }

  async componentDidMount() {
    const response = await MerchantService.getMerchants();
    const listOfAllMerchants = MerchantFactory.createFromJsonArray(response.data.content);

    this.setState({
      listOfAllMerchants,
    });
  }

  handleMerchantSelected(merchantSelected) {
    this.setState({
      merchantSelected: merchantSelected
    }, () => {
      this.props.onMerchantSelected(this.state.merchantSelected);
    });
  }

  render() {
    const merchantOptions = this.state.listOfAllMerchants.map(merchantOption => {
      return {
        value: merchantOption.id,
        label: merchantOption.name,
      };
    });

    return (
      <div>
        <PanelTitle
          title={'Merchant'}
          marginBottom={'0px'}
        />
        <div style={{
          display: 'flex',
          flexDirection: 'row',
        }}>
          <div style={{
            flexGrow: '1',
            paddingRight: '8px',
            flexBasis: '33.3%'
          }}>
            <Select
              placeholder={'Select or enter name'}
              options={merchantOptions}
              onChange={this.handleMerchantSelected}
            />
          </div>
        </div>
      </div>
    );
  }
}

SelectMerchantForm.propTypes = {
  onMerchantSelected: PropTypes.func.isRequired
};