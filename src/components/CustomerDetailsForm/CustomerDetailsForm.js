import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { Customer } from '../../models/Customer';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class CustomerDetailsForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      customerId: '',
      customerName: '',
      customerFirstName: '',
      customerLastName: '',
      customerEmail: '',
      customerGender: '',
      customerBillingFirstName: '',
      customerBillingLastName: '',
      customerBillingCellphone: '',
      customerBillingTelephone: '',
      customerBillingAddressLineOne: '',
      customerBillingAddressLineTwo: '',
      customerBillingAddressPostalCode: '',
    };

    this.updateParentProps = this.updateParentProps.bind(this);
  }

  updateParentProps() {
    this.props.onCustomerEntered(
      this.state.customerFirstName,
      this.state.customerLastName,
      this.state.customerEmail,
      this.state.customerBillingFirstName,
      this.state.customerBillingLastName,
      this.state.customerBillingCellphone,
      this.state.customerBillingTelephone,
      this.state.customerBillingAddressLineOne,
      this.state.customerBillingAddressLineTwo,
      this.state.customerBillingAddressPostalCode);
  }

  componentDidMount() {
    if (this.props.customerSelected === undefined || this.props.customerSelected === null) return;

    this.setState({
      customerFirstName: this.props.customerSelected.firstName,
      customerLastName: this.props.customerSelected.lastName,
      customerEmail: this.props.customerSelected.email,
      customerBillingFirstName: this.props.customerSelected.billingFirstName,
      customerBillingLastName: this.props.customerSelected.billingLastName,
      customerBillingCellphone: this.props.customerSelected.billingCellphone,
      customerBillingTelephone: this.props.customerSelected.billingTelephone,
      customerBillingAddressLineOne: this.props.customerSelected.billingAddressLineOne,
      customerBillingAddressLineTwo: this.props.customerSelected.billingAddressLineTwo,
      customerBillingAddressPostalCode: this.props.customerSelected.billingAddressPostalCode
    });
  }

  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '16px'
      }}>
        <div style={{
          width: '50%',
          marginRight: '8px',
        }}>
          <PanelTitle title={'Account Information'} />

          <div style={{
            width: '80%'
          }}>

            <LabelField
              title={'First Name'}
              type={'text'}
              value={this.state.customerFirstName}
              onChangeHandler={event => {
                this.setState({
                  customerFirstName: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Last Name'}
              type={'text'}
              value={this.state.customerLastName}
              onChangeHandler={event => {
                this.setState({
                  customerLastName: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Email'}
              type={'email'}
              value={this.state.customerEmail}
              onChangeHandler={event => {
                this.setState({
                  customerEmail: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />
          </div>
        </div>

        <div style={{
          width: '50%',
        }}>

          <PanelTitle title={'Billing Information'} />

          <div style={{
            width: '80%'
          }}>

            <LabelField
              title={'First Name'}
              type={'text'}
              value={this.state.customerBillingFirstName}
              onChangeHandler={event => {
                this.setState({
                  customerBillingFirstName: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Last Name'}
              type={'text'}
              value={this.state.customerBillingLastName}
              onChangeHandler={event => {
                this.setState({
                  customerBillingLastName: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Cellphone Number'}
              type={'text'}
              value={this.state.customerBillingCellphone}
              onChangeHandler={event => {
                this.setState({
                  customerBillingCellphone: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Telephone Number'}
              type={'text'}
              value={this.state.customerBillingTelephone}
              onChangeHandler={event => {
                this.setState({
                  customerBillingTelephone: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Address (Line 1)'}
              type={'text'}
              value={this.state.customerBillingAddressLineOne}
              onChangeHandler={event => {
                this.setState({
                  customerBillingAddressLineOne: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Address (Line 2)'}
              type={'text'}
              value={this.state.customerBillingAddressLineTwo}
              onChangeHandler={event => {
                this.setState({
                  customerBillingAddressLineTwo: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />

            <LabelField
              title={'Postal Code'}
              type={'text'}
              value={this.state.customerBillingAddressPostalCode}
              onChangeHandler={event => {
                this.setState({
                  customerBillingAddressPostalCode: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

CustomerDetailsForm.propTypes = {
  customerSelected: PropTypes.instanceOf(Customer),
  onCustomerEntered: PropTypes.func.isRequired,
};