import React from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { UserService } from '../../services/UserService';
import { LabelField } from '../LabelField/LabelField';
import { StylisedButton } from '../StylisedButton/StylisedButton';

class BaseBasicInformationForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      businessName: '',
      businessNumber: '',
      businessAddress: '',
      contactPerson: '',
      contactPersonPosition: '',
      contactPersonMobileNumber: '',
      disableButton: false
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  // componentDidMount() {
  //   axios.get('http://localhost:8080/user/1')
  //     .then(response => {
  //       const user = UserFactory.createFromJson(response.data.data);
  //       this.setState({
  //           email: user.email,
  //           username: user.firstName + ' ' + user.lastName,
  //           businessName: user.firstName + ' ' + user.lastName,
  //           businessNumber: user.businessNumber,
  //           contactPerson: user.lastName,
  //           businessAddress: user.businessMailingAddress + ', ' + user.businessPostCode,
  //           contactPersonPosition: user.contactPosition,
  //           contactPersonMobileNumber: user.contactNumber,
  //       });
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }

  onFormSubmit() {
    console.log(this.state);

    this.setState({ disableButton: true }, async () => {

      const requestBody = {
        email: this.state.email,
        username: this.state.firstName + ' ' + this.state.lastName,
        businessName: this.state.firstName + ' ' + this.state.lastName,
        businessNumber: this.state.businessNumber,
        contactPerson: this.state.lastName,
        businessAddress: this.state.businessMailingAddress + ', ' + this.state.businessPostCode,
        contactPersonPosition: this.state.contactPosition,
        contactPersonMobileNumber: this.state.contactNumber
      };

      const response = await UserService.createUser(requestBody);
      if (response.status === 200) this.props.history.push('/merchants');
      else throw new Error('Something went wrong - the response was something other than 200');
    });

  }

  render() {
    return (
      <div>

        <LabelField
          marginBottom={'8px'}
          title={'Username'}
          type={'text'}
          value={this.state.username}
          onChangeHandler={event => {
            this.setState({
              username: event.target.value
            });
          }}
        />

        <LabelField
          marginBottom={'8px'}
          title={'Email'}
          type={'text'}
          value={this.state.email}
          onChangeHandler={event => {
            this.setState({
              email: event.target.value
            });
          }}
        />

        <LabelField
          marginBottom={'8px'}
          title={'Registered Business Name'}
          type={'text'}
          value={this.state.businessName}
          onChangeHandler={event => {
            this.setState({
              businessName: event.target.value
            });
          }}
        />

        <LabelField
          marginBottom={'8px'}
          title={'Business Registration Number'}
          type={'text'}
          value={this.state.businessNumber}
          onChangeHandler={event => {
            this.setState({
              businessNumber: event.target.value
            });
          }}
        />

        <LabelField
          marginBottom={'8px'}
          title={'Business Mailing Address'}
          type={'text'}
          value={this.state.businessAddress}
          onChangeHandler={event => {
            this.setState({
              businessAddress: event.target.value
            });
          }}
        />

        <LabelField
          marginBottom={'8px'}
          title={'Contact Person'}
          type={'text'}
          value={this.state.contactPerson}
          onChangeHandler={event => {
            this.setState({
              contactPerson: event.target.value
            });
          }}
        />

        <LabelField
          marginBottom={'16px'}
          title={'Contact Person Mobile Number'}
          type={'text'}
          value={this.state.contactPersonMobileNumber}
          onChangeHandler={event => {
            this.setState({
              contactPersonMobileNumber: event.target.value
            });
          }}
        />

        <StylisedButton
          title={'Update'}
          onClick={this.onFormSubmit}
          disabled={this.state.disableButton}
          colour={ColorConstants.teal}
        />
      </div>
    );
  }
}

BaseBasicInformationForm.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};

export const BasicInformationForm = withRouter(BaseBasicInformationForm);