import { shallow } from 'enzyme';
import React from 'react';
import { BasicInformationForm } from './BasicInformationForm';

describe('BasicInformationForm', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<BasicInformationForm component={<div>hello</div>} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});