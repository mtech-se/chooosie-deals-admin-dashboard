import * as PropTypes from 'prop-types';
import React from 'react';
import { CurrencyUtils } from '../../utils/CurrencyUtils';
import { CompareArrow } from '../CompareArrow/CompareArrow';
import { ProgressLoader } from '../ProgressLoader/ProgressLoader';

export const CardDetail = props => {
  const loadingIndicator = <ProgressLoader />;

  const finalContent = props.type === 'Currency' ? CurrencyUtils.toCurrency(props.currentFigure) : props.currentFigure;

  return (
    <div>
      <div style={{
        height: '113px',
        padding: '10px 16px',
        border: '1px solid #dddddd',
        backgroundColor: '#ffffff',
        position: 'relative',
      }}>
        <div style={{
          fontSize: '54px',
          fontFamily: 'Roboto',
          height: '80px',
          display: 'flex',
          alignItems: 'center',
        }}>
          {props.isLoading ? loadingIndicator : finalContent}
        </div>
        <div style={{
          marginTop: '16px',
          position: 'absolute',
          bottom: '16px',
        }}>
          <CompareArrow
            trend={props.trending}
            percent={props.percentage}
            previousFigure={props.previousFigure}
          />
        </div>
      </div>
    </div>

  );
};

CardDetail.propTypes = {
  currentFigure: PropTypes.number,
  previousFigure: PropTypes.number,
  trending: PropTypes.bool.isRequired,
  percentage: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['Currency', 'Non-currency']).isRequired,
  isLoading: PropTypes.bool,
};

CardDetail.defaultProps = {
  isLoading: false,
};