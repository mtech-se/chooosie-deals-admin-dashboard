import React from 'react';
import { smokeTest } from '../../utils/TestUtils';
import { CardDetail } from './CardDetail';

describe('CardDetail', () => {
  it('it renders without crashing', async () => {
    smokeTest(() => (
      <CardDetail
        currentFigure={10}
        trending={true}
        percentage={'10'}
        compareText={'test'}
        type={'Currency'}
      />
    ));
  });
});