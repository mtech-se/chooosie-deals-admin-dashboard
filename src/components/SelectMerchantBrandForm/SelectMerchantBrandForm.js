import * as PropTypes from 'prop-types';
import React from 'react';
import Select from 'react-select';
import { BrandFactory } from '../../factories/BrandFactory';
import { MerchantFactory } from '../../factories/MerchantFactory';
import { BrandService } from '../../services/BrandService';
import { MerchantService } from '../../services/MerchantService';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class SelectMerchantBrandForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listOfAllMerchants: [],
      listOfAllBrands: [],
      merchantSelected: '',
      brandSelected: '',
      brandOptions: undefined
    };

    this.handleMerchantSelected = this.handleMerchantSelected.bind(this);
    this.handleBrandSelected = this.handleBrandSelected.bind(this);
  }

  async componentDidMount() {
    const response = await MerchantService.getMerchants();
    const listOfAllMerchants = MerchantFactory.createFromJsonArray(response.data.content);

    this.setState({
      listOfAllMerchants,
    });
  }

  handleMerchantSelected(merchantSelected) {
    this.setState({
        merchantSelected: merchantSelected
      }, () => {
        this.fetchAllBrands();
      }
    );
  }

  async fetchAllBrands() {
    const response = await BrandService.getBrandsForThisMerchant(this.state.merchantSelected.value);
    const listOfAllBrands = BrandFactory.createFromJsonArray(response.data.content);

    this.setState({
      listOfAllBrands,
    });
  }

  handleBrandSelected(brandSelected) {
    this.setState({
      brandSelected: brandSelected
    }, () => {
      this.props.onBrandSelected(this.state.brandSelected);
    });
  }

  render() {
    const merchantOptions = this.state.listOfAllMerchants.map(merchantOption => {
      return {
        value: merchantOption.id,
        label: merchantOption.name,
      };
    });

    const brandOptions = this.state.listOfAllBrands.map(brandOptions => {
      return {
        value: brandOptions.id,
        label: brandOptions.name,
      };
    });

    return (
      <div>
        <div style={{
          display: 'flex',
          flexDirection: 'row',
        }}>
          <div style={{
            flexGrow: '1',
            paddingRight: '8px',
            maxWidth: '33.3%'
          }}>
            <PanelTitle
              title={'Merchant'}
              marginBottom={'0px'}
            />
            <Select
              placeholder={'Select or enter name'}
              options={merchantOptions}
              onChange={this.handleMerchantSelected}
            />
          </div>

          <div style={{
            flexGrow: '1',
            paddingRight: '8px',
            maxWidth: '33.3%'
          }}>
            <PanelTitle
              title={'Brand'}
              marginBottom={'0px'}
            />
            <Select
              placeholder={'Select or enter name'}
              options={brandOptions}
              onChange={this.handleBrandSelected}
            />
          </div>
        </div>
      </div>
    );
  }
}

SelectMerchantBrandForm.propTypes = {
  onBrandSelected: PropTypes.func.isRequired
};