import Button from '@material-ui/core/Button/Button';
import * as PropTypes from 'prop-types';
import React from 'react';

export const StylisedButton = props => (
  <Button
    onClick={props.onClick}
    disabled={props.disabled}
    variant={props.variant}
    component={props.component}
    to={props.to}
    style={{
      height: '32px',
      borderRadius: '3px',
      backgroundColor: props.disabled ? 'grey' : props.colour,
      fontFamily: 'Roboto, sans-serif',
      fontSize: '16px',
      color: '#ffffff',
      textAlign: 'center',
      ...props.style
    }}>
    {props.title}
  </Button>
);

StylisedButton.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  colour: PropTypes.string.isRequired,
  variant: PropTypes.string,
  component: PropTypes.func,
  to: PropTypes.string,
  style: PropTypes.object,
};