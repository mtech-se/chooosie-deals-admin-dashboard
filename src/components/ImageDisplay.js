import { IconButton } from '@material-ui/core';
import Button from '@material-ui/core/Button/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import * as PropTypes from 'prop-types';
import React from 'react';
import imagePlaceholder from '../assets/images/48x48.png';
import { ColorConstants } from '../constants/ColorConstants';

export const ImageDisplay = props => {
  let imageSource = props.url === undefined ? imagePlaceholder : props.url;

  const dimension = '150px';

  const contentForNoImage = (
    <div style={{
      width: dimension,
      height: dimension,
      backgroundColor: '#f1f1f1',
    }}>
      <input
        accept="image/*"
        style={{ display: 'none' }}
        id="upload-button"
        type="file"
        onChange={props.onUpdatePhoto}
      />
      <label htmlFor="upload-button">
        <Button
          component="span"
          style={{
            width: dimension,
            height: dimension,
            backgroundColor: '#f1f1f1',
          }}
        >
          Add a photo
        </Button>
      </label>
    </div>
  );

  const contentForGotImage = (
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative',
    }}>
      <img
        src={imageSource}
        style={{
          width: dimension,
          height: dimension,
          display: 'block',
        }}
        alt={'entity'}
      />

      <IconButton
        onClick={() => props.onDeletePhotoClicked(props.photoId)}
        style={{
          color: ColorConstants.neutralOffWhite,
          position: 'absolute',
          top: '2px',
          right: '2px',
        }}
      >
        <DeleteIcon/>
      </IconButton>
    </div>
  );

  return (
    <div style={{
      width: dimension,
      height: dimension,
      backgroundColor: 'green',
      ...props.style
    }}>
      {props.url === undefined ? contentForNoImage : contentForGotImage}
    </div>
  );
};

ImageDisplay.propTypes = {
  url: PropTypes.string,
  photoId: PropTypes.number,
  onDeletePhotoClicked: PropTypes.func.isRequired,
  style: PropTypes.object,
  onUpdatePhoto: PropTypes.func.isRequired,
};