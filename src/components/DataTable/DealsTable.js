import debounce from 'debounce';
import moment from 'moment';
import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { DealFactory } from '../../factories/DealFactory';
import { SpringPaginationFactory } from '../../factories/SpringPaginationFactory';
import { DealService } from '../../services/DealService';
import { DateUtils } from '../../utils/DateUtils';

export class DealsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      deals: undefined,
      pagination: undefined,
      loading: true,
      pageSize: 10,
      page: 0,
      selected: {},
      selectAll: 0
    };

    this.onFetchData = this.onFetchData.bind(this);
    this.fetchDeals = debounce(this.fetchDeals, 200);

    this.toggleRow = this.toggleRow.bind(this);
    this.toggleSelectAll = this.toggleSelectAll.bind(this);
  }

  onFetchData(state) {
    const { page, pageSize } = state;
    this.setState({ page, pageSize }, () => this.fetchDeals());
  }

  async fetchDeals() {
    const response = await DealService.getDeals(this.state.pageSize, this.state.page, this.props.searchTerm);
    const deals = DealFactory.createFromJsonArray(response.data.content);
    const pagination = SpringPaginationFactory.createFromJson(response.data);

    this.setState({
      deals,
      pagination,
      loading: false,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.searchTerm !== prevProps.searchTerm) this.fetchDeals();
  }

  toggleRow(id) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    this.setState({
      selected: newSelected,
      selectAll: 2
    }, () => {
      this.props.onDealsSelected(this.state.selected);
    });
  }

  toggleSelectAll() {
    let newSelected = {};

    if (this.state.selectAll === 0) {
      this.state.deals.forEach(row => {
        newSelected[row.id] = true;
      });
    }

    this.setState({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    }, () => {
      this.props.onDealsSelected(this.state.selected);
    });
  }

  render() {
    const columns = [
      // {
      //   id: 'checkbox',
      //   accessor: '',
      //   style: { textAlign: 'center' },
      //   Cell: ({ original }) => {
      //     return (
      //       <div
      //         style={{
      //           display: 'block'
      //         }}>
      //         <input
      //           type="checkbox"
      //           checked={this.state.selected[original.id] === true}
      //           onChange={() => this.toggleRow(original.id)}
      //         />
      //       </div>
      //     );
      //   },
      //   Header: () => {
      //     return (
      //       <input
      //         type="checkbox"
      //         checked={this.state.selectAll === 1}
      //         ref={input => {
      //           if (input) {
      //             input.indeterminate = this.state.selectAll === 2;
      //           }
      //         }}
      //         onChange={() => this.toggleSelectAll()}
      //       />
      //     );
      //   },
      //   width: 50,
      //   sortable: false,
      // },
      {
        Header: 'ID',
        accessor: 'id',
        width: 40,
        style: { textAlign: 'center' },
      },
      {
        Header: 'Deal Title',
        accessor: 'title',
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/deals/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Outlet',
        accessor: 'outlet.name',
        width: 250,
        style: { textAlign: 'center' },
      },
      {
        Header: 'Num Bought',
        accessor: 'numBought',
        width: 100,
        style: { textAlign: 'center' },
      },
      {
        id: 'createdAt',
        Header: 'Created At',
        width: 120,
        style: { textAlign: 'center' },
        accessor: deal => DateUtils.toFriendlyDateString(deal.createdAt),
      },
      {
        id: 'redemptionExpiryDate',
        Header: 'Expires At',
        width: 120,
        style: { textAlign: 'center' },
        accessor: deal => DateUtils.toFriendlyDateString(deal.redemptionExpiryDate),
        sortMethod: (a, b, desc) => {
          console.log(desc);
          const aAsMoment = moment(a, 'DD MMM YYYY');
          const bAsMoment = moment(b, 'DD MMM YYYY');

          const isSame = aAsMoment.isSame(bAsMoment);
          const aIsAfter = aAsMoment.isAfter(bAsMoment);
          const bIsAfter = bAsMoment.isAfter(aAsMoment);

          if (isSame) return 0;
          if (aIsAfter) return 1;
          if (bIsAfter) return -1;
          throw new Error('sort failed');
        }
      },
    ];

    const pages = this.state.pagination === undefined ? 1 : this.state.pagination.totalPages;

    return (
      <ReactTable
        data={this.state.deals}
        columns={columns}
        resizable={false}
        loading={this.state.loading}
        defaultPageSize={this.state.pageSize}
        pages={pages}
        manual
        onFetchData={this.onFetchData}
      />
    );
  }
}

DealsTable.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  onDealsSelected: PropTypes.func.isRequired,
};