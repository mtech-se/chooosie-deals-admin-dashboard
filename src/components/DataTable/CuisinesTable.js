import debounce from 'debounce';
import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { CuisineFactory } from '../../factories/CuisineFactory';
import { SpringPaginationFactory } from '../../factories/SpringPaginationFactory';
import { CuisineService } from '../../services/CuisineService';
import { DateUtils } from '../../utils/DateUtils';

export class CuisinesTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cuisines: undefined,
      pagination: undefined,
      loading: true,
      pageSize: 10,
      page: 0,
      selected: {},
      selectAll: 0
    };

    this.onFetchData = this.onFetchData.bind(this);
    this.fetchCuisines = debounce(this.fetchCuisines, 200);

    this.toggleRow = this.toggleRow.bind(this);
    this.toggleSelectAll = this.toggleSelectAll.bind(this);
  }

  onFetchData(state) {
    const { page, pageSize } = state;
    this.setState({ page, pageSize }, () => this.fetchCuisines());
  }

  async fetchCuisines() {
    const response = await CuisineService.getCuisines(this.state.pageSize, this.state.page, this.props.searchTerm);
    const cuisines = CuisineFactory.createFromJsonArray(response.data.content);
    const pagination = SpringPaginationFactory.createFromJson(response.data);

    this.setState({
      cuisines,
      pagination,
      loading: false,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.searchTerm !== prevProps.searchTerm) this.fetchCuisines();
  }

  toggleRow(id) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    this.setState({
      selected: newSelected,
      selectAll: 2
    }, () => {
      this.props.onCuisinesSelected(this.state.selected);
    });
  }

  toggleSelectAll() {
    let newSelected = {};

    if (this.state.selectAll === 0) {
      this.state.cuisines.forEach(row => {
        newSelected[row.id] = true;
      });
    }

    this.setState({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    }, () => {
      this.props.onCuisinesSelected(this.state.selected);
    });
  }

  render() {
    const columns = [
      // {
      //   id: 'checkbox',
      //   accessor: '',
      //   style: { textAlign: 'center' },
      //   Cell: ({ original }) => {
      //     return (
      //       <div
      //         style={{
      //           display: 'block'
      //         }}>
      //         <input
      //           type="checkbox"
      //           checked={this.state.selected[original.id] === true}
      //           onChange={() => this.toggleRow(original.id)}
      //         />
      //       </div>
      //     );
      //   },
      //   Header: () => {
      //     return (
      //       <input
      //         type="checkbox"
      //         checked={this.state.selectAll === 1}
      //         ref={input => {
      //           if (input) {
      //             input.indeterminate = this.state.selectAll === 2;
      //           }
      //         }}
      //         onChange={() => this.toggleSelectAll()}
      //       />
      //     );
      //   },
      //   width: 50,
      //   sortable: false,
      // },
      {
        Header: 'ID',
        accessor: 'id',
        width: 40,
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/cuisines/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Cuisine Name',
        accessor: 'name',
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/cuisines/${row.original.id}`}>
            {row.value}
          </Link>
        )
      },
      {
        Header: 'Description',
        accessor: 'description',
        style: { textAlign: 'center' },
      },
      {
        id: 'createdAt',
        Header: 'Created At',
        width: 120,
        style: { textAlign: 'center' },
        accessor: cuisine => DateUtils.toFriendlyDateString(cuisine.createdAt),
      },
      {
        id: 'updatedAt',
        Header: 'Updated On',
        width: 120,
        style: { textAlign: 'center' },
        accessor: cuisine => DateUtils.toFriendlyDateString(cuisine.updatedAt)
      },
    ];

    const pages = this.state.pagination === undefined ? 1 : this.state.pagination.totalPages;

    return (
      <ReactTable
        data={this.state.cuisines}
        columns={columns}
        resizable={false}
        loading={this.state.loading}
        defaultPageSize={this.state.pageSize}
        pages={pages}
        manual
        onFetchData={this.onFetchData}
      />
    );
  }
}

CuisinesTable.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  onCuisinesSelected: PropTypes.func.isRequired,
};