import debounce from 'debounce';
import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { AdminFactory } from '../../factories/AdminFactory';
import { SpringPaginationFactory } from '../../factories/SpringPaginationFactory';
import { AdminService } from '../../services/AdminService';

export class AdminsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      admins: undefined,
      pagination: undefined,
      loading: true,
      page: 0,
      pageSize: 10,
      selected: {},
      selectAll: 0
    };

    this.onFetchData = this.onFetchData.bind(this);
    this.fetchAdmins = debounce(this.fetchAdmins, 200);

    this.toggleRow = this.toggleRow.bind(this);
    this.toggleSelectAll = this.toggleSelectAll.bind(this);
  }

  componentDidMount() {
    this.fetchAdmins();
  }

  onFetchData(state) {
    const { page } = state;
    this.setState({ page }, () => this.fetchAdmins());
  }

  async fetchAdmins() {
    const response = await AdminService.getAdmins(
      this.state.pageSize,
      this.state.page,
      this.props.searchTerm
    );
    const admins = AdminFactory.createFromJsonArray(response.data.content);
    const pagination = SpringPaginationFactory.createFromJson(response.data);

    this.setState({
      admins,
      pagination,
      loading: false,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.searchTerm !== prevProps.searchTerm) this.fetchAdmins();
  }

  toggleRow(id) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    this.setState({
      selected: newSelected,
      selectAll: 2
    }, () => {
      this.props.onAdminsSelected(this.state.selected);
    });
  }

  toggleSelectAll() {
    let newSelected = {};

    if (this.state.selectAll === 0) {
      this.state.admins.forEach(row => {
        newSelected[row.id] = true;
      });
    }

    this.setState({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    }, () => {
      this.props.onAdminsSelected(this.state.selected);
    });
  }

  render() {
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
        width: 40,
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/admins/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'First Name',
        accessor: 'firstName',
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/admins/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Last Name',
        accessor: 'lastName',
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/admins/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Email',
        accessor: 'email',
        width: 200,
        style: { textAlign: 'center' },
      },
      {
        id: 'createdAt',
        Header: 'Created At',
        width: 120,
        style: { textAlign: 'center' },
        // accessor: admin => DateUtils.toFriendlyDateString(admin.createdAt),
      },
      {
        id: 'updatedOn',
        Header: 'Updated On',
        width: 120,
        style: { textAlign: 'center' },
        // accessor: admin => DateUtils.toFriendlyDateString(admin.updatedAt)
      },
    ];

    const pages = this.state.pagination === undefined ? 1 : this.state.pagination.totalPages;

    return (
      <ReactTable
        data={this.state.admins}
        columns={columns}
        resizable={false}
        loading={this.state.loading}
        defaultPageSize={this.state.pageSize}
        pages={pages}
        manual
        onFetchData={this.onFetchData}
      />
    );
  }
}

AdminsTable.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  onAdminsSelected: PropTypes.func.isRequired,
};