import * as PropTypes from 'prop-types';
import React from 'react';

export const SearchField = props => {
  return (
    <input
      type="text"
      style={{
        height: '32px',
        width: '200px',
        paddingLeft: '8px',
        borderRadius: '5px',
        fontFamily: 'Roboto',
        fontSize: '16px',
        ...props.style
      }}
      placeholder={'Search...'}
      onChange={props.onChange}
    />
  );
};

SearchField.propTypes = {
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object,
};