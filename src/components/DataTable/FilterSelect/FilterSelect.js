import * as PropTypes from 'prop-types';
import React from 'react';

export const FilterSelect = props => {
  return (
    <select style={{
      ...props.style
    }}>
      <option value="All">All</option>
      <option value="Published">Published</option>
      <option value="Unpublished">Unpublished</option>
      <option value="Archived">Archived</option>
    </select>
  );
};

FilterSelect.propTypes = {
  style: PropTypes.object,
};