// import debounce from 'debounce';
// import * as PropTypes from 'prop-types';
// import React from 'react';
// import { Link } from 'react-router-dom';
// import ReactTable from 'react-table';
// import 'react-table/react-table.css';
// import { PurchaseFactory } from '../../factories/PurchaseFactory';
// import { PurchaseService } from '../../services/PurchaseService';
//
// export class PurchasesTable extends React.Component {
//   constructor(props) {
//     super(props);
//
//     this.state = {
//       purchases: undefined,
//       pagination: undefined,
//       loading: true,
//       page: 0,
//       pageSize: 10,
//     };
//
//     this.onFetchData = this.onFetchData.bind(this);
//     this.fetchPurchases = debounce(this.fetchPurchases, 200);
//   }
//
//   onFetchData(state) {
//     const { page, pageSize } = state;
//     this.setState({ page, pageSize }, () => this.fetchPurchases());
//   }
//
//   async fetchPurchases() {
//     const response = await PurchaseService.getPurchases(this.state.pageSize, this.state.page, this.props.searchTerm);
//     const purchases = PurchaseFactory.createFromJsonArray(response.data.content);
//     const pagination = response.data.totalPages;
//     // const pagination = SpringPaginationFactory.createFromJson(response.data);
//
//     this.setState({
//       purchases,
//       pagination,
//       loading: false,
//     });
//   }
//
//   componentDidUpdate(prevProps, prevState, snapshot) {
//     if (this.props.searchTerm !== prevProps.searchTerm) this.fetchPurchases();
//   }
//
//   render() {
//     const columns = [
//       {
//         Header: 'ID',
//         accessor: 'id',
//         width: 40,
//         style: { textAlign: 'center' },
//         Cell: row => (
//           <Link to={`/purchases/${row.original.id}`}>
//             {row.value}
//           </Link>
//         ),
//       },
//       {
//         Header: 'Customer Name',
//         accessor: 'customer.name',
//         width: 300,
//         style: { textAlign: 'center' },
//         Cell: row => (
//           <Link to={`/purchases/${row.original.id}`}>
//             {row.value}
//           </Link>
//         ),
//       },
//       {
//         Header: 'Deal Title',
//         accessor: 'deal.title',
//         style: { textAlign: 'center' },
//       },
//       {
//         Header: 'Quantity',
//         accessor: 'quantity',
//         width: 100,
//         style: { textAlign: 'center' },
//
//       },
//       {
//         id: 'createdAt',
//         Header: 'Purchased On',
//         width: 120,
//         style: { textAlign: 'center' },
//         // accessor: purchase => DateUtils.toFriendlyDateString(purchase.createdAt),
//       }
//     ];
//
//     // const pages = this.state.pagination === undefined ? 1 : this.state.pagination.totalPages;
//
//     return (
//       <ReactTable
//         data={this.state.purchases}
//         columns={columns}
//         resizable={false}
//         loading={this.state.loading}
//         defaultPageSize={this.state.pageSize}
//         pages={this.state.pagination}
//         manual
//         onFetchData={this.onFetchData}
//       />
//     );
//   }
// }
//
// PurchasesTable.propTypes = {
//   searchTerm: PropTypes.string.isRequired,
// };