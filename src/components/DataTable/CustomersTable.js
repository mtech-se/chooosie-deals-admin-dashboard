import debounce from 'debounce';
import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { CustomerFactory } from '../../factories/CustomerFactory';
import { SpringPaginationFactory } from '../../factories/SpringPaginationFactory';
import { CustomerService } from '../../services/CustomerService';
import { DateUtils } from '../../utils/DateUtils';

export class CustomersTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      customers: undefined,
      pagination: undefined,
      loading: true,
      pageSize: 10,
      page: 0,
    };

    this.onFetchData = this.onFetchData.bind(this);
    this.fetchCustomers = debounce(this.fetchCustomers, 200);
  }

  onFetchData(state) {
    const { page, pageSize } = state;
    this.setState({ page, pageSize }, () => this.fetchCustomers());
  }

  async fetchCustomers() {
    const response = await CustomerService.getCustomers(this.state.pageSize, this.state.page, this.props.searchTerm);
    const customers = CustomerFactory.createFromJsonArray(response.data.content);
    const pagination = SpringPaginationFactory.createFromJson(response.data);

    this.setState({
      customers,
      pagination,
      loading: false,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.searchTerm !== prevProps.searchTerm) this.fetchCustomers();
  }

  render() {
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
        width: 40,
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/customers/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Customer Name',
        accessor: 'name',
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/customers/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Email',
        accessor: 'email',
        width: 250,
        style: { textAlign: 'center' },

      },
      {
        Header: 'Phone Num',
        accessor: 'billingCellphone',
        width: 200,
        style: { textAlign: 'center' },
      },
      {
        id: 'createdAt',
        Header: 'Created At',
        width: 120,
        style: { textAlign: 'center' },
        accessor: customer => DateUtils.toFriendlyDateString(customer.createdAt),
      },
      {
        id: 'updateAt',
        Header: 'Updated On',
        width: 120,
        style: { textAlign: 'center' },
        accessor: customer => DateUtils.toFriendlyDateString(customer.updatedAt)
      },
    ];

    const pages = this.state.pagination === undefined ? 1 : this.state.pagination.totalPages;

    return (
      <ReactTable
        data={this.state.customers}
        columns={columns}
        resizable={false}
        loading={this.state.loading}
        defaultPageSize={this.state.pageSize}
        pages={pages}
        manual
        onFetchData={this.onFetchData}
      />
    );
  }
}

CustomersTable.propTypes = {
  searchTerm: PropTypes.string,
};