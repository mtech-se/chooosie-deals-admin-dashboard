import debounce from 'debounce';
import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { MerchantFactory } from '../../factories/MerchantFactory';
import { SpringPaginationFactory } from '../../factories/SpringPaginationFactory';
import { MerchantService } from '../../services/MerchantService';
import { DateUtils } from '../../utils/DateUtils';

export class MerchantsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      merchants: undefined,
      pagination: undefined,
      loading: true,
      pageSize: 10,
      page: 0,
      selected: {},
      selectAll: 0
    };

    this.onFetchData = this.onFetchData.bind(this);
    this.fetchMerchants = debounce(this.fetchMerchants, 200);

    this.toggleRow = this.toggleRow.bind(this);
    this.toggleSelectAll = this.toggleSelectAll.bind(this);
  }

  onFetchData(state) {
    const { page, pageSize } = state;
    this.setState({ page, pageSize }, () => this.fetchMerchants());
  }

  async fetchMerchants() {
    const response = await MerchantService.getMerchants(this.state.pageSize, this.state.page, this.props.searchTerm);
    const merchants = MerchantFactory.createFromJsonArray(response.data.content);
    const pagination = SpringPaginationFactory.createFromJson(response.data);

    this.setState({
      merchants,
      pagination,
      loading: false,
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.searchTerm !== prevProps.searchTerm) this.fetchMerchants();
  }

  toggleRow(id) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    this.setState({
      selected: newSelected,
      selectAll: 2
    }, () => {
      this.props.onMerchantsSelected(this.state.selected);
    });
  }

  toggleSelectAll() {
    let newSelected = {};

    if (this.state.selectAll === 0) {
      this.state.merchants.forEach(row => {
        newSelected[row.id] = true;
      });
    }

    this.setState({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    }, () => {
      this.props.onMerchantsSelected(this.state.selected);
    });
  }

  render() {
    const columns = [
      // {
      //   id: 'checkbox',
      //   accessor: '',
      //   style: { textAlign: 'center' },
      //   Cell: ({ original }) => {
      //     return (
      //       <div
      //         style={{
      //           display: 'block'
      //         }}>
      //         <input
      //           type="checkbox"
      //           checked={this.state.selected[original.id] === true}
      //           onChange={() => this.toggleRow(original.id)}
      //         />
      //       </div>
      //     );
      //   },
      //   Header: () => {
      //     return (
      //       <input
      //         type="checkbox"
      //         checked={this.state.selectAll === 1}
      //         ref={input => {
      //           if (input) {
      //             input.indeterminate = this.state.selectAll === 2;
      //           }
      //         }}
      //         onChange={() => this.toggleSelectAll()}
      //       />
      //     );
      //   },
      //   width: 50,
      //   sortable: false,
      // },
      {
        Header: 'ID',
        accessor: 'id',
        style: { textAlign: 'center' },
        width: 40,
        Cell: row => (
          <Link to={`/merchants/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Name',
        accessor: 'name',
        style: { textAlign: 'center' },
        Cell: row => (
          <Link to={`/merchants/${row.original.id}`}>
            {row.value}
          </Link>
        ),
      },
      {
        Header: 'Contact Person',
        accessor: 'contactPersons[0].name',
        style: { textAlign: 'center' },
        width: 200,
      },
      {
        Header: 'Position',
        accessor: 'contactPersons[0].position',
        style: { textAlign: 'center' },
        width: 150,
      },
      {
        id: 'createdAt',
        Header: 'Created At',
        style: { textAlign: 'center' },
        width: 120,
        accessor: merchant => DateUtils.toFriendlyDateString(merchant.createdAt),
      },
      {
        id: 'updatedOn',
        Header: 'Updated On',
        style: { textAlign: 'center' },
        width: 120,
        accessor: merchant => DateUtils.toFriendlyDateString(merchant.updatedAt)
      },
    ];

    const pages = this.state.pagination === undefined ? 1 : this.state.pagination.totalPages;

    return (
      <ReactTable
        data={this.state.merchants}
        columns={columns}
        resizable={false}
        loading={this.state.loading}
        defaultPageSize={this.state.pageSize}
        pages={pages}
        manual
        onFetchData={this.onFetchData}
      />
    );
  }
}

MerchantsTable.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  onMerchantsSelected: PropTypes.func,
};