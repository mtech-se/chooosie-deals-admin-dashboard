import { shallow } from 'enzyme';
import React from 'react';
import { RedemptionList } from './RedemptionList';

describe('RedemptionList', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<RedemptionList component={<div>hello</div>} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});