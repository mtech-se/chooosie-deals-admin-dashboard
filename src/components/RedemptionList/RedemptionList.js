import * as PropTypes from 'prop-types';
import React from 'react';
import { MerchantWithRedemptionCount } from '../../models/MerchantWithRedemptionCount';
import { ProgressLoader } from '../ProgressLoader/ProgressLoader';
import { RedemptionListItem } from '../RedemptionListItem/RedemptionListItem';

export const RedemptionList = props => {
  const loadingIndicator = (
    <div style={{
      marginTop: '16px',
      display: 'inline-block',
      marginLeft: '16px',
    }}>
      <ProgressLoader />
    </div>
  );

  const finalContent = props.merchantsWithRedemptionCount === undefined ? undefined : props.merchantsWithRedemptionCount.map(merchantWithRedemptionCount => {
    return (
      <RedemptionListItem
        key={merchantWithRedemptionCount.merchant.id}
        merchantWithRedemptionCount={merchantWithRedemptionCount}
      />
    );
  });

  return (
    <div>
      {props.isLoading ? loadingIndicator : finalContent}
    </div>
  );
};

RedemptionList.propTypes = {
  merchantsWithRedemptionCount: PropTypes.arrayOf(PropTypes.instanceOf(MerchantWithRedemptionCount)),
  isLoading: PropTypes.bool,
};

RedemptionList.defaultProps = {
  isLoading: false,
};