import React from 'react';
import { smokeTest } from '../../utils/TestUtils';
import { AppBrand } from './AppBrand';

describe('AppBrand', () => {
  it('renders without errors', () => {
    smokeTest(() => <AppBrand />);
  });
});