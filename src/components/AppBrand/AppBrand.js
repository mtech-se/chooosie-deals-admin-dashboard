import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/deals-logo.png';
import { ColorConstants } from '../../constants/ColorConstants';
import { DimensionConstants } from '../../constants/DimensionConstants';

export const AppBrand = props => {
  return (
    <Link to="/"
          {...props}>
      <div style={{
        backgroundColor: ColorConstants.primaryPurple,
        textAlign: 'center',
        height: DimensionConstants.topNavBarHeight,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        ...props.style,
      }}>
        <img
          src={logo}
          alt="Deals Logo"
          style={{
            width: '125px',
            height: '56px',
          }}
        />
      </div>
    </Link>
  );
};

AppBrand.propTypes = {
  style: PropTypes.object
};