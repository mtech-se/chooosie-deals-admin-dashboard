import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { Category } from '../../models/Category';
import { LabelField } from '../LabelField/LabelField';
import { PhotosDropZoneForm } from '../PhotosDropZoneForm/PhotosDropZoneForm';

export class CategoryDetailsForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categoryName: ''
    };

    this.updateParentProps = this.updateParentProps.bind(this);
  }

  updateParentProps() {
    this.props.onCategoryEntered(this.state.categoryName);
  }

  componentDidMount() {
    if (this.props.categorySelected === undefined || this.props.categorySelected === null) return;

    this.setState({
      categoryName: this.props.categorySelected.name
    });
  }

  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
      }}>
        <div style={{
          maxWidth: '33.3%',
          paddingRight: '8px'
        }}>
          <LabelField
            marginBottom={'16px'}
            title={'Category Name'}
            type={'text'}
            value={this.state.categoryName}
            onChangeHandler={event => {
              this.setState({
                categoryName: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />
        </div>

        <div style={{
          marginBottom: '16px'
        }}>
          <PhotosDropZoneForm />
        </div>
      </div>
    );
  }
}

CategoryDetailsForm.propTypes = {
  categorySelected: PropTypes.instanceOf(Category),
  onCategoryEntered: PropTypes.func.isRequired,
};