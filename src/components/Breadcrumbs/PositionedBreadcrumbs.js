import * as PropTypes from 'prop-types';
import React from 'react';
import { Breadcrumb } from '../../models/Breadcrumb';
import { Breadcrumbs } from './Breadcrumbs';

export const PositionedBreadcrumbs = props => {
  return (
    <div style={{
      position: 'absolute',
    }}>
      <div style={{
        position: 'relative',
        top: '-48px',
        marginLeft: '16px',
      }}>
        <Breadcrumbs breadcrumbs={props.breadcrumbs} />
      </div>
    </div>
  );
};

PositionedBreadcrumbs.propTypes = {
  breadcrumbs: PropTypes.arrayOf(PropTypes.instanceOf(Breadcrumb)).isRequired,
};