import * as PropTypes from 'prop-types';
import React from 'react';
import { Breadcrumb } from '../../models/Breadcrumb';
import { BreadcrumbItem } from './BreadcrumbItem';

export const Breadcrumbs = props => {
  const contents = [];
  let count = 0;
  props.breadcrumbs.forEach((breadcrumb, index) => {
    const isNotLast = index !== props.breadcrumbs.length - 1;

    contents.push(
      <BreadcrumbItem
        breadcrumb={new Breadcrumb(breadcrumb.label, breadcrumb.url)}
        key={count++}
      />
    );

    if (isNotLast) {
      contents.push(
        <div
          style={{
            color: '#bababa',
            fontSize: '13px',
            fontFamily: 'Montserrat Bold',
            fontWeight: '700',
            height: '16px',
            margin: '0 8px',
          }}
          key={count++}>&gt;
        </div>
      );
    }
  });

  return (
    <div style={{
      display: 'flex',
    }}>
      {contents}
    </div>
  );
};

Breadcrumbs.propTypes = {
  breadcrumbs: PropTypes.arrayOf(PropTypes.instanceOf(Breadcrumb)).isRequired,
};