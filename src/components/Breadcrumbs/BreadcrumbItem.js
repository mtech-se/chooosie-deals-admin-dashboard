import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb } from '../../models/Breadcrumb';

export const BreadcrumbItem = props => {
  return <Link
    to={props.breadcrumb.url}
    style={{
      color: '#bababa',
      fontSize: '13px',
      fontFamily: 'Roboto, sans-serif',
      fontWeight: '700',
      height: '16px',
    }}
  >{props.breadcrumb.label}</Link>;
};

BreadcrumbItem.propTypes = {
  breadcrumb: PropTypes.instanceOf(Breadcrumb).isRequired,
};