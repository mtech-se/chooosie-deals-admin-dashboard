import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { MerchantService } from '../../services/MerchantService';
import { MerchantDetailsForm } from '../MerchantDetailsForm/MerchantDetailsForm';
import { StylisedButton } from '../StylisedButton/StylisedButton';

class BaseMerchantDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      merchantName: '',
      merchantRegistrationNumber: '',
      merchantMailingAddress: '',
      contactPersonName: '',
      contactPersonPosition: '',
      contactPersonMobileNumber: '',
      contactPersonEmailAddress: '',
      disableButton: true
    };

    this.onMerchantEntered = this.onMerchantEntered.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onMerchantEntered(
    merchantName,
    merchantRegistrationNumber,
    merchantMailingAddress,
    contactPersonName,
    contactPersonPosition,
    contactPersonMobileNumber,
    contactPersonEmailAddress,
    isFormComplete
  ) {
    this.setState({
      merchantName,
      merchantRegistrationNumber,
      merchantMailingAddress,
      contactPersonName,
      contactPersonPosition,
      contactPersonMobileNumber,
      contactPersonEmailAddress,
      disableButton: !isFormComplete
    });
  }

  onFormSubmit() {
    this.setState({
      disableButton: true,
    }, async () => {

      const requestBody = {
        name: this.state.merchantName,
        business_registration_number: this.state.merchantRegistrationNumber,
        mailing_address: this.state.merchantMailingAddress,
        contact_persons: [
          {
            name: this.state.contactPersonName,
            position: this.state.contactPersonPosition,
            mobile_number: this.state.contactPersonMobileNumber,
            email: this.state.contactPersonEmailAddress
          }
        ]
      };

      const response = await MerchantService.createMerchant(requestBody);
      if (response.status === 201) this.props.history.push('/merchants');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  render() {
    return (
      <div>
        <div>
          <MerchantDetailsForm onMerchantEntered={this.onMerchantEntered} />

          <StylisedButton
            title={'Create Merchant'}
            onClick={this.onFormSubmit}
            disabled={this.state.disableButton}
            colour={ColorConstants.teal}
          />
        </div>
      </div>
    );
  }
}

BaseMerchantDetails.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
};

export const MerchantDetails = withRouter(BaseMerchantDetails);