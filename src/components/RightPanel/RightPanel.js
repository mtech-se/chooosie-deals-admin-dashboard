import * as PropTypes from 'prop-types';
import React from 'react';
import { TopNavBar } from '../TopNavBar/TopNavBar';

export const RightPanel = props => {
  return (
    <div style={{
      flexGrow: 1,
    }}>
      <TopNavBar />
      {props.component}
    </div>
  );
};

RightPanel.propTypes = {
  component: PropTypes.element.isRequired,
};