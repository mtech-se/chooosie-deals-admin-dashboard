import { shallow } from 'enzyme';
import React from 'react';
import { RightPanel } from './RightPanel';

describe('RightPanel', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<RightPanel component={<div>hello</div>} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});