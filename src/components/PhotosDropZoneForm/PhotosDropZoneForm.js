import 'dropzone/dist/min/dropzone.min.css';
import * as PropTypes from 'prop-types';
import React from 'react';
import { DropzoneComponent } from 'react-dropzone-component';
import 'react-dropzone-component/styles/filepicker.css';
import { connect } from 'react-redux';
import './PhotosDropZoneForm.css';

class BasePhotosDropZoneForm extends React.Component {
  handleFileAdded = file => {
    console.log(file);
  };

  render() {
    const componentConfig = {
      // iconFiletypes: ['.jpg', '.png', '.gif'],
      // showFiletypeIcon: true,
      postUrl: this.props.postUrl,
    };

    const djsConfig = {
      headers: {
        'Authorization': this.props.accessTokenPrefix + this.props.accessToken,
      }
    };

    const eventHandlers = {
      init: () => console.log('init'),
      addedfile: this.handleFileAdded,
    };

    return (
      <DropzoneComponent
        config={componentConfig}
        djsConfig={djsConfig}
        eventHandlers={eventHandlers}
      />
    );
  }
}

BasePhotosDropZoneForm.propTypes = {
  accessToken: PropTypes.string.isRequired,
  accessTokenPrefix: PropTypes.string.isRequired,
  postUrl: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  accessToken: state.accessToken,
  accessTokenPrefix: state.accessTokenPrefix,
});

// const mapDispatchToProps = dispatch => {
//   return {
//     initStore: () => dispatch(()),
//   };
// };

export const PhotosDropZoneForm = connect(mapStateToProps)(BasePhotosDropZoneForm);

PhotosDropZoneForm.propTypes = {
  postUrl: PropTypes.string.isRequired,
};