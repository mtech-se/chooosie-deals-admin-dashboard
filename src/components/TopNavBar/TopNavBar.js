import React from 'react';
import { ColorConstants } from '../../constants/ColorConstants';
import { DimensionConstants } from '../../constants/DimensionConstants';
import { ProfileMenu } from '../ProfileMenu/ProfileMenu';

export const TopNavBar = props => {
  return (
    <div style={{
      height: DimensionConstants.topNavBarHeight,
      backgroundColor: ColorConstants.white,
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
    }}>

      <div style={{
        position: 'absolute',
        right: '0',
      }}>
        <ProfileMenu style={{
          marginRight: '32px',
        }} />
      </div>
    </div>
  );
};
