import { shallow } from 'enzyme';
import React from 'react';
import { TopNavBar } from './TopNavBar';

describe('TopNavBar', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<TopNavBar />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});