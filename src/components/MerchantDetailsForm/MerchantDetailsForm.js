import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { Merchant } from '../../models/Merchant';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class MerchantDetailsForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      merchantName: '',
      merchantRegistrationNumber: '',
      merchantMailingAddress: '',
      contactPersonName: '',
      contactPersonPosition: '',
      contactPersonMobileNumber: '',
      contactPersonEmailAddress: '',
    };

    this.updateParentProps = this.updateParentProps.bind(this);
  }

  isFormComplete() {
    let result = true;
    if (this.state.merchantName === '') result = false;
    if (this.state.merchantRegistrationNumber === '') result = false;
    if (this.state.merchantMailingAddress === '') result = false;
    if (this.state.contactPersonName === '') result = false;
    if (this.state.contactPersonPosition === '') result = false;
    if (this.state.contactPersonMobileNumber === '') result = false;
    if (this.state.contactPersonEmailAddress === '') result = false;
    return result;
  }

  updateParentProps() {
    const isFormComplete = this.isFormComplete();

    this.props.onMerchantEntered(
      this.state.merchantName,
      this.state.merchantRegistrationNumber,
      this.state.merchantMailingAddress,
      this.state.contactPersonName,
      this.state.contactPersonPosition,
      this.state.contactPersonMobileNumber,
      this.state.contactPersonEmailAddress,
      isFormComplete,
    );
  }

  componentDidMount() {
    if (this.props.merchantSelected === undefined || this.props.merchantSelected === null) return;

    this.setState({
      merchantName: this.props.merchantSelected.name,
      merchantRegistrationNumber: this.props.merchantSelected.businessRegistrationNumber,
      merchantMailingAddress: this.props.merchantSelected.mailingAddress,
      contactPersonName: this.props.merchantSelected.contactPersons[0].name,
      contactPersonPosition: this.props.merchantSelected.contactPersons[0].position,
      contactPersonMobileNumber: this.props.merchantSelected.contactPersons[0].mobileNumber,
      contactPersonEmailAddress: this.props.merchantSelected.contactPersons[0].email
    });
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          marginBottom: '8px'
        }}>

        <div style={{
          width: '33%',
          marginRight: '8px'
        }}>

          <PanelTitle
            title={'Business Details'}
            marginBottom={'0px'}
          />

          <LabelField
            title={'Name'}
            type={'text'}
            value={this.state.merchantName}
            onChangeHandler={event => {
              this.setState({
                merchantName: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />

          <LabelField
            title={'Registration Number'}
            type={'text'}
            value={this.state.merchantRegistrationNumber}
            onChangeHandler={event => {
              this.setState({
                merchantRegistrationNumber: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />

          <LabelField
            title={'Mailing Address'}
            type={'text'}
            value={this.state.merchantMailingAddress}
            onChangeHandler={event => {
              this.setState({
                merchantMailingAddress: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />
        </div>

        <div style={{
          width: '33%'
        }}>

          <PanelTitle
            title={'Contact Person Details'}
            marginBottom={'0px'}
          />

          <LabelField
            title={'Name'}
            type={'text'}
            value={this.state.contactPersonName}
            onChangeHandler={event => {
              this.setState({
                contactPersonName: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />

          <LabelField
            title={'Position'}
            type={'text'}
            value={this.state.contactPersonPosition}
            onChangeHandler={event => {
              this.setState({
                contactPersonPosition: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />

          <LabelField
            title={'Mobile Number'}
            type={'text'}
            value={this.state.contactPersonMobileNumber}
            onChangeHandler={event => {
              this.setState({
                contactPersonMobileNumber: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />

          <LabelField
            title={'Email Address'}
            type={'email'}
            value={this.state.contactPersonEmailAddress}
            onChangeHandler={event => {
              this.setState({
                contactPersonEmailAddress: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />
        </div>
      </div>
    );
  }
}

MerchantDetailsForm.propTypes = {
  merchantSelected: PropTypes.instanceOf(Merchant),
  onMerchantEntered: PropTypes.func.isRequired,
};