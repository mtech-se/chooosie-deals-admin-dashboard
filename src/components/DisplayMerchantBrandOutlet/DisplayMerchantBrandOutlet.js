import * as PropTypes from 'prop-types';
import React from 'react';
import { Deal } from '../../models/Deal';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class DisplayMerchantBrandOutlet extends React.Component {
  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '8px'
      }}>
        <div style={{
          flexGrow: '1',
          paddingRight: '8px',
          flexBasis: '33.3%'
        }}>
          <PanelTitle
            title={'Merchant'}
            marginBottom={'0px'}
          />
          <LabelField
            title={'Name'}
            value={this.props.dealSelected.outlet.brand.merchant.name}
            type={'text'}
            isDisabled={true}
          />
        </div>

        <div style={{
          flexGrow: '1',
          paddingRight: '8px',
          flexBasis: '33.3%'
        }}>
          <PanelTitle
            title={'Brand'}
            marginBottom={'0px'}
          />
          <LabelField
            title={'Name'}
            value={this.props.dealSelected.outlet.brand.name}
            type={'text'}
            isDisabled={true}
          />
        </div>

        <div style={{
          flexGrow: '1',
          flexBasis: '33.3%'
        }}>
          <PanelTitle
            title={'Outlet'}
            marginBottom={'0px'}
          />
          <LabelField
            title={'Name'}
            value={this.props.dealSelected.outlet.name}
            type={'text'}
            isDisabled={true}
          />
        </div>
      </div>
    );
  }
}

DisplayMerchantBrandOutlet.propTypes = {
  dealSelected: PropTypes.instanceOf(Deal).isRequired,
};