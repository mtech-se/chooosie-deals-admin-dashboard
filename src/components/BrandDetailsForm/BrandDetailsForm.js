import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { Brand } from '../../models/Brand';
import { PhotoService } from '../../services/PhotoService';
import { ImageDisplay } from '../ImageDisplay';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';

export class BrandDetailsForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      brandName: '',
      brandPhoto: '',
      photoId: undefined,
      photoUrl: undefined,
      file: undefined,
      isPhotoFromServer: true
    };

    this.updateParentProps = this.updateParentProps.bind(this);
    this.deletePhoto = this.deletePhoto.bind(this);
    this.updatePhoto = this.updatePhoto.bind(this);
  }

  updateParentProps() {
    this.props.onBrandEntered(this.state.brandName, this.state.brandPhoto);
  }

  componentDidMount() {
    if (this.props.brandSelected === undefined || this.props.brandSelected === null) return;

    const photoId = this.props.brandSelected.getPhoto(0) === undefined ? undefined : this.props.brandSelected.getPhoto(0).id;
    const photoUrl = this.props.brandSelected.getMainPhotoUrl();

    this.setState({
      brandName: this.props.brandSelected.name,
      photoId,
      photoUrl,
    });
  }

  deletePhoto(photoId) {
    if (this.props.brandSelected && this.state.isPhotoFromServer) {
      this.apiCallToDeletePhoto(photoId);
    }

    this.setState({
      photoId: undefined,
      photoUrl: undefined,
      isPhotoFromServer: false
    });
  }


  async apiCallToDeletePhoto(photoId) {
    const response = await PhotoService.deletePhoto(photoId);

    if (response.status !== 204) {
      console.log('successfully deleted photo: ' + photoId);
      this.props.onPhotoDeleted();
    }
  }

  updatePhoto(event) {
    if (event.target.files && event.target.files[0]) {
      this.setState({
        brandPhoto: event.target.files[0],
        photoUrl: URL.createObjectURL(event.target.files[0])
      }, () => {
        this.updateParentProps();
      });
    }
  }

  render() {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
      }}>
        <div style={{
          maxWidth: '33.3%',
          paddingRight: '8px',
        }}>

          <PanelTitle
            title={'Brand'}
            marginBottom={'0px'}
          />

          <LabelField
            title={'Name'}
            type={'text'}
            value={this.state.brandName}
            onChangeHandler={event => {
              this.setState({
                brandName: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            requiredField={true}
            style={{
              maxWidth: '400px',
            }}
          />
        </div>

        <div style={{
          display: 'flex',
          marginTop: '16px',
        }}>
          <ImageDisplay
            url={this.state.photoUrl}
            photoId={this.state.photoId}
            onDeletePhotoClicked={this.deletePhoto}
            onUpdatePhoto={this.updatePhoto}
          />
        </div>
      </div>
    );
  }
}

BrandDetailsForm.propTypes = {
  brandSelected: PropTypes.instanceOf(Brand),
  onBrandEntered: PropTypes.func.isRequired,
  onPhotoDeleted: PropTypes.func,
};