import React from 'react';
import { Merchant } from '../../models/Merchant';
import { MerchantWithRedemptionCount } from '../../models/MerchantWithRedemptionCount';
import { smokeTest } from '../../utils/TestUtils';
import { RedemptionListItem } from './RedemptionListItem';

describe('RedemptionListItem', () => {
  it('it renders without crashing', () => {
    const merchantWithRedemptionCount = new MerchantWithRedemptionCount(
      new Merchant(
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
      ),
      0
    );

    smokeTest(
      () => (
        <RedemptionListItem
          merchantWithRedemptionCount={merchantWithRedemptionCount}
        />
      )
    );
  });
});