import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import { MerchantWithRedemptionCount } from '../../models/MerchantWithRedemptionCount';
import { StringUtils } from '../../utils/StringUtils';

export const RedemptionListItem = props => {
  return (
    <div>
      <div style={{
        height: '95px',
        padding: '0px 15px',
        border: '1px solid #dddddd',
        backgroundColor: '#ffffff',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column'
      }}>
        <div style={{
          fontFamily: 'Roboto',
          fontSize: '45px',
          color: '#333333',
        }}>{props.merchantWithRedemptionCount.numPurchasesRedeemed}
        </div>

        <div style={{
          fontFamily: 'Roboto',
          fontSize: '13px',
          color: '#333333',
        }}>
          <Link to={'/merchants/' + props.merchantWithRedemptionCount.merchant.id}>
            {StringUtils.ellipsify(props.merchantWithRedemptionCount.merchant.name, 40)}
          </Link>
        </div>
      </div>
    </div>
  );
};

RedemptionListItem.propTypes = {
  merchantWithRedemptionCount: PropTypes.instanceOf(MerchantWithRedemptionCount).isRequired,
};