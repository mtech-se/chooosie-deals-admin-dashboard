import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { BrandService } from '../../services/BrandService';
import { PhotoService } from '../../services/PhotoService';
import { BrandDetailsForm } from '../BrandDetailsForm/BrandDetailsForm';
import { StylisedButton } from '../StylisedButton/StylisedButton';

class BaseBrandDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      brandName: '',
      brandPhoto: undefined,
      disableButton: false
    };

    this.onBrandEntered = this.onBrandEntered.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onBrandEntered(brandName, brandPhoto) {
    this.setState({
      brandName,
      brandPhoto
    });
  }

  onFormSubmit() {
    this.setState({
      disableButton: true,
    }, async () => {
      const merchantId = this.props.merchantSelected.value;
      const merchantName = this.props.merchantSelected.label;

      const requestBody = {

        name: this.state.brandName,
        merchant: {
          name: merchantName,
          id: merchantId
        }
      };

      const response = await BrandService.createBrand(requestBody);
      if (response.status === 201) this.updateBrandPhoto(response.data.id);
      else throw new Error('Something went wrong - the response was something other than 201');
    });
  }

  async updateBrandPhoto(brandId) {
    const formData = new FormData();
    formData.append('photos', this.state.brandPhoto);
    const updateUrl = '/brands/' + brandId;
    const responseUpdatePhoto = await PhotoService.updatePhoto(updateUrl, formData);

    if (responseUpdatePhoto.status === 201) this.props.history.push('/brands');
    else throw new Error('Something went wrong - the response was something other than 200');
  }

  render() {
    return (
      <div>
        <div>
          <BrandDetailsForm
            onBrandEntered={this.onBrandEntered}
          />
          <div style={{
            marginTop: '16px',
          }}>
            <StylisedButton
              title={'Create Brand'}
              onClick={this.onFormSubmit}
              disabled={this.state.disableButton}
              colour={ColorConstants.teal}
            />
          </div>
        </div>
      </div>
    );
  }
}

BaseBrandDetails.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  merchantSelected: PropTypes.object,
};

export const BrandDetails = withRouter(BaseBrandDetails);