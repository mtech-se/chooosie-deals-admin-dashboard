import moment from 'moment/moment';
import * as PropTypes from 'prop-types';
import React from 'react';
import DatePicker from 'react-datepicker/dist/react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export class DateRange extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      startDate: moment('2018-01-01'),
      endDate: moment('2018-12-31'),
      rangeDate: 'today'
    };
    this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
    this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
    this.handleChangeStartEndDate = this.handleChangeStartEndDate.bind(this);
    this.onChangeRangeDate = this.onChangeRangeDate.bind(this);
  }

  handleChangeStartDate(date) {
    this.setState({
      startDate: date
    }, () => {
      this.props.onDateUpdate(this.state.startDate, this.state.endDate);
    });
  }

  handleChangeEndDate(date) {
    this.setState({
      endDate: date
    }, () => {
      this.props.onDateUpdate(this.state.startDate, this.state.endDate);
    });
  }

  handleChangeStartEndDate(startDate, endDate) {
    this.setState({
      startDate: startDate,
      endDate: endDate
    }, () => {
      this.props.onDateUpdate(this.state.startDate, this.state.endDate);
    });
  }

  onChangeRangeDate(event) {
    this.setState({
      rangeDate: event.target.value
    }, () => {
      switch (this.state.rangeDate) {
        case 'today':
          this.handleChangeStartEndDate(moment(), moment());

          break;
        case 'yesterday':
          this.handleChangeStartEndDate(moment().subtract(1, 'days'), moment().subtract(1, 'days'));

          break;
        case 'last7days':
          this.handleChangeStartEndDate(moment().subtract(6, 'days'), moment());

          break;
        case 'last14days':
          this.handleChangeStartEndDate(moment().subtract(13, 'days'), moment());

          break;
        case 'last28days':
          this.handleChangeStartEndDate(moment().subtract(27, 'days'), moment());

          break;
        default:
          console.log('reset');
          this.handleChangeStartEndDate(moment('2018-01-01'), moment('2018-12-31'));
      }
    });
  }

  render() {
    return (
      <div style={{
        display: 'flex',
        alignItems: 'center',
      }}>
        <div style={{
          marginRight: 8
        }}>Starting from
        </div>

        <DatePicker
          dropdownMode={'select'}
          dateFormat='D MMM YYYY'
          disabledKeyboardNavigation
          selected={this.state.startDate}
          selectsStart
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onChange={this.handleChangeStartDate}
        />

        <div style={{
          marginLeft: 8,
          marginRight: 8
        }}>to
        </div>

        <DatePicker
          dropdownMode={'select'}
          dateFormat='D MMM YYYY'
          disabledKeyboardNavigation
          selected={this.state.endDate}
          selectsEnd
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          onChange={this.handleChangeEndDate}
        />

        <div style={{
          marginLeft: 8,
          marginRight: 8
        }}>or
        </div>

        <select id="date-select"
                onChange={this.onChangeRangeDate}
                value={this.state.value}>
          <option value="">Choose Date Range</option>
          <option value="today">Today</option>
          <option value="yesterday">Yesterday</option>
          <option value="last7days">Last 7 days</option>
          <option value="last14days">Last 14 Days</option>
          <option value="last28days">Last 28 Days</option>
        </select>
      </div>
    );
  }
}

DateRange.propTypes = {
  onDateUpdate: PropTypes.func
};