import * as PropTypes from 'prop-types';
import React from 'react';

export const PanelTitle = props => {
  return (
    <div style={{
      fontSize: '21px',
      fontFamily: 'Roboto, sans-serif',
      marginBottom: props.marginBottom !== undefined ? props.marginBottom : '16px'
    }}>{props.title}</div>
  );
};

PanelTitle.propTypes = {
  title: PropTypes.string.isRequired,
  marginBottom: PropTypes.string
};