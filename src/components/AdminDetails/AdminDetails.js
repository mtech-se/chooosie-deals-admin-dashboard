import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { AdminService } from '../../services/AdminService';
import { AdminDetailsForm } from '../AdminDetailsForm/AdminDetailsForm';
import { StylisedButton } from '../StylisedButton/StylisedButton';

class BaseAdminDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      adminFirstName: '',
      adminLastName: '',
      adminEmail: '',
      adminRole: '',
      disableButton: false
    };

    this.onAdminEntered = this.onAdminEntered.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onAdminEntered(
    adminFirstName,
    adminLastName,
    adminEmail,
    adminRole
  ) {
    this.setState({
      adminFirstName,
      adminLastName,
      adminEmail,
      adminRole
    });
  }

  onFormSubmit() {
    this.setState({
      disableButton: true,
    }, async () => {
      const requestBody = {
        first_name: this.state.adminFirstName,
        last_name: this.state.adminLastName,
        email: this.state.adminEmail,
        password: 'secret',
        gender: 'male',
        adminRole: this.state.adminRole,
      };

      const response = await AdminService.createAdmin(requestBody);
      if (response.status === 201) this.props.history.push('/admins');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  render() {
    return (
      <div>
        <div>
          <AdminDetailsForm onAdminEntered={this.onAdminEntered} />

          <StylisedButton
            title={'Create Admin'}
            onClick={this.onFormSubmit}
            disabled={this.state.disableButton}
            colour={ColorConstants.teal}
          />
        </div>
      </div>
    );
  }
}

BaseAdminDetails.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
};

export const AdminDetails = withRouter(BaseAdminDetails);