import { shallow } from 'enzyme';
import React from 'react';
import { DealList } from './DealList';

describe('DealList', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<DealList component={<div>hello</div>} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});