import * as PropTypes from 'prop-types';
import React from 'react';
import { Deal } from '../../models/Deal';
import { DealListItem } from '../DealListItem/DealListItem';
import { ProgressLoader } from '../ProgressLoader/ProgressLoader';

export const DealList = props => {
  const loadingIndicator = (
    <div style={{
      marginTop: '16px',
      display: 'inline-block',
      marginLeft: '16px',
    }}>
      <ProgressLoader />
    </div>
  );

  const finalContent = props.deals === undefined ? undefined : props.deals.map(deal => {
    return (
      <DealListItem
        key={deal.id}
        deal={deal}
      />
    );
  });

  return (
    <div>
      {props.isLoading ? loadingIndicator : finalContent}
    </div>
  );
};

DealList.propTypes = {
  deals: PropTypes.arrayOf(PropTypes.instanceOf(Deal)),
  isLoading: PropTypes.bool,
};

DealList.defaultProps = {
  isLoading: false,
};
