import { IconButton, Menu, MenuItem } from '@material-ui/core';
import * as PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ReactRouterPropTypes from 'react-router-prop-types';
import profileIcon from '../../assets/images/profile-icon.png';
import { ColorConstants } from '../../constants/ColorConstants';
import { Admin } from '../../models/Admin';

class BaseProfileMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorElement: null,
    };
  }

  handleMenuClick = event => {
    this.setState({ anchorElement: event.currentTarget });
  };

  handleMenuItemClick = linkType => {
    switch (linkType) {
      case 'MY_ACCOUNT':
        this.props.history.push('/my-account');
        break;
      case 'LOGOUT':
        this.props.history.push('/logout');
        break;
      default:
        break;
    }

    this.setState({ anchorElement: null });
  };

  render() {
    return (
      <div style={{
        display: 'flex',
        alignItems: 'center',
        ...this.props.style
      }}>
        <IconButton
          aria-owns={this.state.anchorElement ? 'simple-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleMenuClick}
          style={{
            backgroundColor: ColorConstants.primaryPurple,
          }}
        >
          <img
            src={profileIcon}
            alt="Profile Menu"
            style={{
              width: '16px',
              height: '16px',
            }}
          />
        </IconButton>

        <div style={{
          marginLeft: '8px',
        }}>{this.props.admin.firstName}
        </div>

        <Menu
          id="simple-menu"
          anchorEl={this.state.anchorElement}
          open={Boolean(this.state.anchorElement)}
          onClose={this.handleMenuItemClick}
        >
          <MenuItem onClick={() => this.handleMenuItemClick('MY_ACCOUNT')}>My Account</MenuItem>
          <MenuItem onClick={() => this.handleMenuItemClick('LOGOUT')}>Logout</MenuItem>
        </Menu>
      </div>
    );
  }
}

BaseProfileMenu.propTypes = {
  admin: PropTypes.instanceOf(Admin).isRequired,
  style: PropTypes.object,
  history: ReactRouterPropTypes.history.isRequired,
};

const mapStateToProps = state => ({
  admin: state.admin,
});

export const ProfileMenu = connect(mapStateToProps)(withRouter(BaseProfileMenu));