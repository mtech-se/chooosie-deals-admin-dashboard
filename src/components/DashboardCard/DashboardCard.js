import * as PropTypes from 'prop-types';
import React from 'react';
import { CardDetail } from '../CardDetail/CardDetail';
import { CardTitle } from '../CardTitle/CardTitle';

const calculatePercentageString = (currentFigure, previousFigure) => {
  if (previousFigure === 0 && currentFigure === 0) return '0';
  if (previousFigure === 0) return '\u221E';

  return `${Math.round((currentFigure - previousFigure) / previousFigure * 100)}`;
};

const calculateTrend = (currentFigure, previousFigure) => {
  if (previousFigure === 0 && currentFigure === 0) return false;
  if (previousFigure === 0) return false;

  const percentageMovement = (currentFigure - previousFigure) / previousFigure * 100;

  return percentageMovement > 0;
};

export const DashboardCard = props => {
  const percentageString = calculatePercentageString(props.currentFigure, props.previousFigure);
  const trending = calculateTrend(props.currentFigure, props.previousFigure);

  return (
    <div>
      <CardTitle
        title={props.title}
      />

      <CardDetail
        percentage={percentageString}
        currentFigure={props.currentFigure}
        previousFigure={props.previousFigure}
        trending={trending}
        type={props.type}
        isLoading={props.isLoading}
      />
    </div>
  );
};

DashboardCard.propTypes = {
  title: PropTypes.string.isRequired,
  currentFigure: PropTypes.number,
  previousFigure: PropTypes.number,
  type: PropTypes.oneOf(['Currency', 'Non-currency']).isRequired,
  isLoading: PropTypes.bool,
};

DashboardCard.defaultProps = {
  isLoading: false,
};