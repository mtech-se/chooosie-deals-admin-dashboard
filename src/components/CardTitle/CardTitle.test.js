import { shallow } from 'enzyme';
import React from 'react';
import { CardTitle } from './CardTitle';

describe('CardTitle', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<CardTitle title={'title'} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});