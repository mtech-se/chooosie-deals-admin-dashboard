import * as PropTypes from 'prop-types';
import React from 'react';
import { ColorConstants } from '../../constants/ColorConstants';

export const CardTitle = props => {
  return (
    <div style={{
      height: '43px',
      padding: '0 8px',
      border: '1px solid #dddddd',
      borderTopLeftRadius: '4px',
      borderTopRightRadius: '4px',
      backgroundColor: ColorConstants.white,
      display: 'flex',
      alignItems: 'center',
    }}>
      <div style={{
        fontFamily: 'Roboto',
        fontSize: '15px',
        color: '#22211d',
        flexGrow: '5'
      }}>{props.title}
      </div>

    </div>
  );
};

CardTitle.propTypes = {
  title: PropTypes.string.isRequired,
};