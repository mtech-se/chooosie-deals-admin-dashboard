import React from 'react';
import { ColorConstants } from '../../constants/ColorConstants';
import { StylisedButton } from '../StylisedButton/StylisedButton';

export class ProfilePhotoForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      disableButton: false
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit() {
    console.log(this.state);

    this.setState({ disableButton: true });

    // TODO: use the form values in state to make a web service call to BE
  }

  render() {
    return (
      <div>
        <div style={{
          display: 'flex'
        }}>
          <div style={{
            backgroundColor: 'white',
            marginRight: '16px'
          }}>
            <img style={{
              width: '120px',
              height: '120px'
            }}
                 src='https://mdn.mozillademos.org/files/6457/mdn_logo_only_color.png'
                 alt="Logo" />
          </div>

          <div style={{
            backgroundColor: 'white',
          }}>
            <div style={{
              fontSize: '16px',
              fontFamily: 'Roboto, sans-serif',
              marginBottom: '8px'
            }}>Upload your photo..
            </div>

            <div style={{
              fontSize: '15px',
              fontFamily: 'Roboto, sans-serif',
              color: '#757575',
              marginBottom: '16px'
            }}>Photo should be at least 300px × 300px
            </div>

            <StylisedButton
              title={'Upload Photo'}
              onClick={this.onFormSubmit}
              disabled={this.state.disableButton}
              colour={ColorConstants.teal}
            />
          </div>
        </div>
      </div>
    );
  }
}