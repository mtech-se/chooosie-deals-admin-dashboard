import { shallow } from 'enzyme';
import React from 'react';
import { ProfilePhotoForm } from './ProfilePhotoForm';

describe('ProfilePhotoForm', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<ProfilePhotoForm component={<div>hello</div>} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});