import moment from 'moment/moment';
import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { Deal } from '../../models/Deal';
import { Outlet } from '../../models/Outlet';
import { DealService } from '../../services/DealService';
import { DealDetailsForm } from '../DealDetailsForm/DealDetailsForm';
import { StylisedButton } from '../StylisedButton/StylisedButton';

class BaseDealDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listOfAllCategories: [],
      dealTitle: '',
      dealCategory: '',
      dealQuantity: '',
      dealFinePrint: '',
      dealDescription: '',
      dealOriginalPrice: '',
      dealDiscountedPrice: '',
      dealAutoPublishDate: moment(),
      dealRedemptionExpiryDate: moment(),
      dealRedemptionInformation: '',
      disableButton: false,
      dealOfTheMonthFlag: false,
    };

    this.onDealEntered = this.onDealEntered.bind(this);
    this.createDeal = this.createDeal.bind(this);
  }

  /** @param {Deal} deal */
  onDealEntered(deal) {
    this.setState({
      dealTitle: deal.title,
      dealDescription: deal.description,
      dealOriginalPrice: deal.originalPrice,
      dealDiscountedPrice: deal.discountedPrice,
      dealQuantity: deal.maxQuantity,
      dealRedemptionInformation: deal.redemptionInstructions,
      dealFinePrint: deal.termsOfUse,
      dealCategory: deal.categoryDeals,
      dealCuisineDeals: deal.cuisineDeals,
      dealAutoPublishDate: deal.autoPublishDate,
      dealRedemptionExpiryDate: deal.redemptionExpiryDate,
      dealOfTheMonthFlag: deal.dealOfTheMonth,
    });
  }

  createDeal() {
    this.setState({
      disableButton: true,
    }, async () => {
      const outletId = this.props.outletSelected.value;

      const outlet = new Outlet(
        outletId,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
        undefined,
      );

      const deal = new Deal(
        undefined,
        this.state.dealTitle,
        this.state.dealDescription,
        this.state.dealOriginalPrice * 100,
        this.state.dealDiscountedPrice * 100,
        outlet,
        this.state.dealQuantity,
        undefined,
        this.state.dealRedemptionInformation,
        this.state.dealFinePrint,
        undefined,
        this.state.dealOfTheMonthFlag,
        undefined,
        undefined,
        undefined,
        this.state.dealAutoPublishDate,
        this.state.dealRedemptionExpiryDate,
        undefined,
        undefined,
        undefined,
        undefined,
      );

      const requestBody = deal.toJson();

      const response = await DealService.createDeal(requestBody);
      if (response.status === 201) this.props.history.push('/deals');
      else throw new Error('Something went wrong - the response was something other than 200');
    });
  }

  render() {
    return (
      <div>
        <DealDetailsForm onDealEntered={this.onDealEntered} />

        <StylisedButton
          title={'Create Deal'}
          onClick={this.createDeal}
          disabled={this.state.disableButton}
          colour={ColorConstants.teal}
        />
      </div>
    );
  }
}

BaseDealDetails.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  outletSelected: PropTypes.object,
};

export const DealDetails = withRouter(BaseDealDetails);