import * as PropTypes from 'prop-types';
import React from 'react';
import DatePicker from 'react-datepicker/dist/react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Select from 'react-select';
import { CategoryFactory } from '../../factories/CategoryFactory';
import { CuisineFactory } from '../../factories/CuisineFactory';
import { Category } from '../../models/Category';
import { CategoryDeal } from '../../models/CategoryDeal';
import { Cuisine } from '../../models/Cuisine';
import { CuisineDeal } from '../../models/CuisineDeal';
import { Deal } from '../../models/Deal';
import { CategoryService } from '../../services/CategoryService';
import { CuisineService } from '../../services/CuisineService';
import { LabelField } from '../LabelField/LabelField';
import { PanelTitle } from '../PanelTitle/PanelTitle';

/**
 *
 * @param {{value: number, label: string}} selectCategoryValue
 * @returns {CategoryDeal[]}
 */
const makeCategoryDeals = selectCategoryValue => {
  if (selectCategoryValue === undefined) return undefined;

  const category = new Category(
    selectCategoryValue.value,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
  );

  return [
    new CategoryDeal(
      undefined,
      category,
      undefined,
      undefined,
      undefined,
    )
  ];
};

/**
 *
 * @param {{value: number, label: string}} selectCuisineValue
 * @returns {CuisineDeal[]}
 */
const makeCuisineDeals = selectCuisineValue => {
  if (selectCuisineValue === undefined) return undefined;

  const cuisine = new Cuisine(
    selectCuisineValue.value,
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
  );

  return [
    new CuisineDeal(
      undefined,
      cuisine,
      undefined,
      undefined,
      undefined,
    )
  ];

  // const category = new Category(
  //   selectCategoryValue.value,
  //   undefined,
  //   undefined,
  //   undefined,
  //   undefined,
  //   undefined,
  //   undefined,
  // );
  //
  // return [
  //   new CategoryDeal(
  //     undefined,
  //     category,
  //     undefined,
  //     undefined,
  //     undefined,
  //   )
  // ];
};

export class DealDetailsForm extends React.Component {
  constructor(props) {
    super(props);

    let dealTitle = '';
    let dealDescription = '';
    let dealOriginalPrice = '';
    let dealDiscountedPrice = '';
    let dealQuantity = '';
    let dealFinePrint = '';
    let dealRedemptionInformation = '';
    let dealCategory = '';
    let dealCuisine = '';
    let dealOfTheMonthFlag = false;
    let dealAutoPublishDate = undefined;
    let dealRedemptionExpiryDate = undefined;

    let selectCategoryValue = undefined;
    let selectCuisineValue = undefined;

    if (this.props.deal !== undefined) {
      selectCategoryValue = this.props.deal.categoryDeals.length === 0 ? undefined : {
        value: this.props.deal.categoryDeals[0].category.id,
        label: this.props.deal.categoryDeals[0].category.name,
      };

      selectCuisineValue = this.props.deal.cuisineDeals.length === 0 ? undefined : {
        value: this.props.deal.cuisineDeals[0].cuisine.id,
        label: this.props.deal.cuisineDeals[0].cuisine.name,
      };

      dealTitle = this.props.deal.title;
      dealDescription = this.props.deal.description;
      dealOriginalPrice = this.props.deal.originalPrice / 100;
      dealDiscountedPrice = this.props.deal.discountedPrice / 100;
      dealQuantity = this.props.deal.maxQuantity;
      dealFinePrint = this.props.deal.termsOfUse;
      dealRedemptionInformation = this.props.deal.redemptionInstructions;
      dealCategory = this.props.deal.categoryDeals;
      dealCuisine = this.props.deal.cuisineDeals;
      dealAutoPublishDate = this.props.deal.autoPublishDate;
      dealRedemptionExpiryDate = this.props.deal.redemptionExpiryDate;
      dealOfTheMonthFlag = this.props.deal.dealOfTheMonth;
    }

    this.state = {
      dealTitle,
      dealDescription,
      dealOriginalPrice,
      dealDiscountedPrice,
      dealQuantity,
      dealFinePrint,
      dealRedemptionInformation,
      dealCategory,
      dealCuisine,
      dealAutoPublishDate,
      dealRedemptionExpiryDate,
      dealOfTheMonthFlag,

      selectCategoryValue,
      selectCuisineValue,

      listOfAllCuisines: undefined,
      listOfAllCategories: undefined,
      disableButton: false,
    };

    this.handleCategorySelected = this.handleCategorySelected.bind(this);
    this.handleCuisineSelected = this.handleCuisineSelected.bind(this);
    this.handleChangePublishDate = this.handleChangePublishDate.bind(this);
    this.handleChangeExpiryDate = this.handleChangeExpiryDate.bind(this);
    this.handleDealOfTheMonth = this.handleDealOfTheMonth.bind(this);
    this.updateParentProps = this.updateParentProps.bind(this);
  }

  async componentDidMount() {
    const responseCategories = await CategoryService.getAllCategories();
    const listOfAllCategories = CategoryFactory.createFromJsonArray(responseCategories.data.content);

    const responseCuisines = await CuisineService.getAllCuisines();
    const listOfAllCuisines = CuisineFactory.createFromJsonArray(responseCuisines.data.content);

    this.setState({
      listOfAllCategories,
      listOfAllCuisines,
    });
  }

  handleCategorySelected(selectCategoryValue) {
    this.setState({ selectCategoryValue }, () => this.updateParentProps());
  }

  handleCuisineSelected(selectCuisineValue) {
    this.setState({ selectCuisineValue }, () => this.updateParentProps());
  }

  handleChangeExpiryDate(date) {
    this.setState({
      dealRedemptionExpiryDate: date
    }, () => {
      this.updateParentProps();
    });
  }

  handleChangePublishDate(date) {
    this.setState({
      dealAutoPublishDate: date
    }, () => {
      this.updateParentProps();
    });
  }

  handleDealOfTheMonth(event) {
    this.setState({
      dealOfTheMonthFlag: event.target.checked,
    }, () => {
      this.updateParentProps();
    });
  }

  updateParentProps() {
    let categoryDeals = makeCategoryDeals(this.state.selectCategoryValue);
    let cuisineDeals = makeCuisineDeals(this.state.selectCuisineValue);

    const deal = new Deal(
      undefined,
      this.state.dealTitle,
      this.state.dealDescription,
      this.state.dealOriginalPrice,
      this.state.dealDiscountedPrice,
      undefined,
      this.state.dealQuantity,
      undefined,
      this.state.dealRedemptionInformation,
      this.state.dealFinePrint,
      undefined,
      this.state.dealOfTheMonthFlag,
      undefined,
      categoryDeals,
      cuisineDeals,
      this.state.dealAutoPublishDate,
      this.state.dealRedemptionExpiryDate,
      undefined,
      undefined,
      undefined,
      undefined,
    );

    this.props.onDealEntered(deal);
  }

  render() {
    if (this.state.listOfAllCategories === undefined || this.state.listOfAllCuisines === undefined) return null;

    const categoryOptions = this.state.listOfAllCategories.map(categoryOption => {
      return {
        value: categoryOption.id,
        label: categoryOption.name,
      };
    });

    const cuisineOptions = this.state.listOfAllCuisines.map(cuisineOption => {
      return {
        value: cuisineOption.id,
        label: cuisineOption.name,
      };
    });

    return (
      <div>
        <PanelTitle
          title={'Deal'}
          marginBottom={'0px'}
        />

        <div style={{
          display: 'flex',
          flexDirection: 'row',
        }}>
          <div style={{
            flexGrow: '2',
            paddingRight: '8px',
          }}>
            <LabelField
              title={'Title'}
              type={'text'}
              value={this.state.dealTitle}
              onChangeHandler={event => {
                this.setState({
                  dealTitle: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />
          </div>

          <div style={{
            flexGrow: '2',
            paddingRight: '8px',
            width: '10%'
          }}>
            <div style={{
              fontFamily: 'Roboto, sans-serif',
              fontSize: '15px',
              color: '#222114',
            }}>Category *
              <Select
                placeholder={'Select or enter name'}
                options={categoryOptions}
                onChange={this.handleCategorySelected}
                value={this.state.selectCategoryValue}
              />
            </div>
          </div>

          <div style={{
            flexGrow: '2',
            paddingRight: '8px',
            width: '10%'
          }}>
            <div style={{
              fontFamily: 'Roboto, sans-serif',
              fontSize: '15px',
              color: '#222114',
            }}>Cuisine *
              <Select
                placeholder={'Select or enter name'}
                options={cuisineOptions}
                onChange={this.handleCuisineSelected}
                value={this.state.selectCuisineValue}
              />
            </div>
          </div>

          <div>
            <div style={{
              display: 'flex',
            }}>
              <div style={{
                fontFamily: 'Roboto, sans-serif',
                fontSize: '15px',
                color: '#222114',
                width: '100px',
              }}>Publish Date*:
              </div>

              <DatePicker
                dropdownMode={'select'}
                dateFormat='D MMM YYYY'
                disabledKeyboardNavigation
                selected={this.state.dealAutoPublishDate}
                onChange={this.handleChangePublishDate}
              />
            </div>

            <div style={{
              display: 'flex',
              marginTop: '8px',
              marginBottom: '8px'
            }}>
              <div style={{
                fontFamily: 'Roboto, sans-serif',
                fontSize: '15px',
                color: '#222114',
                width: '100px',
              }}>Expiry Date*:
              </div>

              <DatePicker
                dropdownMode={'select'}
                dateFormat='D MMM YYYY'
                disabledKeyboardNavigation
                selected={this.state.dealRedemptionExpiryDate}
                onChange={this.handleChangeExpiryDate}
              />
            </div>
          </div>
        </div>

        <div style={{
          display: 'flex',
          alignItems: 'center',
        }}>
          <div style={{
            paddingRight: '8px',
            flexGrow: 1,
          }}>
            <LabelField
              title={'Original Price'}
              type={'number'}
              value={this.state.dealOriginalPrice}
              onChangeHandler={event => {
                this.setState({
                  dealOriginalPrice: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />
          </div>

          <div style={{
            flexGrow: 1,
            paddingRight: '8px',
          }}>
            <LabelField
              title={'Discounted Price'}
              type={'number'}
              value={this.state.dealDiscountedPrice}
              onChangeHandler={event => {
                this.setState({
                  dealDiscountedPrice: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />
          </div>

          <div style={{
            flexGrow: 1,
            paddingRight: '8px',
          }}>
            <LabelField
              title={'Quantity'}
              type={'number'}
              value={this.state.dealQuantity}
              onChangeHandler={event => {
                this.setState({
                  dealQuantity: event.target.value
                }, () => {
                  this.updateParentProps();
                });
              }}
              required={true}
            />
          </div>

          <div style={{
            flexGrow: 1,
          }}>
            <input
              type="checkbox"
              id="dealOfTheMonth"
              name="dealOfTheMonth"
              defaultChecked={this.state.dealOfTheMonthFlag}
              onChange={this.handleDealOfTheMonth}
            />
            <label htmlFor="dealOfTheMonth">Deal of the Month</label>
          </div>
        </div>

        <div>
          <LabelField
            title={'Description'}
            type={'text'}
            value={this.state.dealDescription}
            onChangeHandler={event => {
              this.setState({
                dealDescription: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
            multiline={true}
          />
          <LabelField
            title={'Fine Print'}
            type={'text'}
            value={this.state.dealFinePrint}
            onChangeHandler={event => {
              this.setState({
                dealFinePrint: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
            multiline={true}
          />
          <LabelField
            marginBottom={'16px'}
            title={'Redemption Information'}
            type={'text'}
            value={this.state.dealRedemptionInformation}
            onChangeHandler={event => {
              this.setState({
                dealRedemptionInformation: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
            multiline={true}
          />
        </div>
      </div>
    );
  }
}

DealDetailsForm.propTypes = {
  deal: PropTypes.instanceOf(Deal),
  onDealEntered: PropTypes.func,
};