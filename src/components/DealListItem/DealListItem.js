import * as PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import { Deal } from '../../models/Deal';
import { StringUtils } from '../../utils/StringUtils';

export const DealListItem = props => {
  return (
    <div style={{
      display: 'flex',
      width: '100%',
    }}>

      <div style={{
        height: '95px',
        width: '100%',
        padding: '0px 15px',
        border: '1px solid #dddddd',
        backgroundColor: '#ffffff',
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column'
      }}>
        <div style={{
          fontFamily: 'Roboto',
          fontSize: '12px',
          color: '#27a297',
          marginBottom: '8px',
          textTransform: 'uppercase'
        }}><Link to={`/deals/${props.deal.id}`}>{StringUtils.ellipsify(props.deal.title, 40)}</Link>
        </div>

        <div style={{
          fontFamily: 'Roboto',
          fontSize: '12px',
          color: '#474747',
          marginBottom: '8px'
        }}>{StringUtils.ellipsify(props.deal.description, 80)}
        </div>

        <div style={{
          fontFamily: 'Roboto',
          fontSize: '12px',
          color: '#474747',
          marginBottom: '8px'
        }}>Num Bought: {props.deal.numBought}
        </div>
      </div>
    </div>
  );
};

DealListItem.propTypes = {
  deal: PropTypes.instanceOf(Deal),
};