import { shallow } from 'enzyme';
import React from 'react';
import { Deal } from '../../models/Deal';
import { DealListItem } from './DealListItem';

describe('DealListItem', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();

    const deal = new Deal(
      1,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
    );

    shallow(
      <DealListItem
        numBought={0}
        title={'title'}
        text={'text'}
        deal={deal}
      />
    );
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});