import React from 'react';
import { withRouter } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import { ColorConstants } from '../../constants/ColorConstants';
import { AppBrand } from '../AppBrand/AppBrand';
import { LeftNavBarLink } from '../LeftNavBarLink/LeftNavBarLink';

const BaseLeftNavBar = props => {
  const links = [
    { label: 'Overview', url: '/overview' },
    { label: 'Merchants', url: '/merchants' },
    { label: 'Brands', url: '/brands' },
    { label: 'Outlets', url: '/outlets' },
    { label: 'Deals', url: '/deals' },
    { label: 'Categories', url: '/categories' },
    { label: 'Cuisines', url: '/cuisines' },
    { label: 'Customers', url: '/customers' },
    // { label: 'Sales Summary', url: '/sales-summary' },
    // { label: 'Purchases', url: '/purchases' },
    { label: 'Manage Admins', url: '/admins' }
  ];

  const leftNavBarLinks = links.map((link, index) => {
    return <LeftNavBarLink
      label={link.label}
      url={link.url}
      isActive={props.location.pathname === link.url}
      key={index}
    />;
  });

  return (
    <div style={{
      backgroundColor: ColorConstants.white,
      width: '256px',
      minWidth: '256px',
    }}>
      <AppBrand />
      {leftNavBarLinks}
    </div>
  );
};

BaseLeftNavBar.propTypes = {
  location: ReactRouterPropTypes.location.isRequired,
};

export const LeftNavBar = withRouter(BaseLeftNavBar);