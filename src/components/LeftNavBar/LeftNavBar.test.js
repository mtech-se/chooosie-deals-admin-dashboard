import { shallow } from 'enzyme';
import React from 'react';
import { LeftNavBar } from './LeftNavBar';

describe('LeftNavBarLink', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<LeftNavBar />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});