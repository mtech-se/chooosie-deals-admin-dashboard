import { shallow } from 'enzyme';
import React from 'react';
import { ProgressLoader } from './ProgressLoader';

describe('ProgressLoader', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<ProgressLoader />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});