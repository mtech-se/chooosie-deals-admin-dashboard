import { CircularProgress } from '@material-ui/core';
import * as PropTypes from 'prop-types';
import React from 'react';
import { ColorConstants } from '../../constants/ColorConstants';

export const ProgressLoader = props => {
  return (
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
      <CircularProgress
        size={50}
        style={{
          color: ColorConstants.primaryPurple,
          ...props.style
        }}
      />
    </div>
  );
};

ProgressLoader.propTypes = {
  style: PropTypes.object,
};