import * as PropTypes from 'prop-types';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import Select from 'react-select';
import { Admin } from '../../models/Admin';
import { LabelField } from '../LabelField/LabelField';

export class AdminDetailsForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      adminFirstName: '',
      adminLastName: '',
      adminEmail: '',
      adminRole: '',
    };

    this.updateParentProps = this.updateParentProps.bind(this);
    this.handleAdminRoleSelected = this.handleAdminRoleSelected.bind(this);
  }

  componentDidMount() {
    if (this.props.adminSelected === undefined || this.props.adminSelected === null) return;

    this.setState({
      adminFirstName: this.props.adminSelected.firstName,
      adminLastName: this.props.adminSelected.lastName,
      adminEmail: this.props.adminSelected.email,
      adminRole: this.props.adminSelected.role
    }, () => this.updateParentProps());
  }

  updateParentProps() {
    this.props.onAdminEntered(
      this.state.adminFirstName,
      this.state.adminLastName,
      this.state.adminEmail,
      this.state.adminRole
    );
  }

  handleAdminRoleSelected(role) {
    this.setState({
      adminRole: role
    });
  }

  render() {
    const adminRoleOptions = [
      { value: 'admin', label: 'Admin' },
      { value: 'superAdmin', label: 'Super Admin' }
    ];

    return (
      <div style={{
        display: 'flex',
        flexDirection: 'row'
      }}>
        <div style={{
          width: '33%',
          marginRight: '8px'
        }}>
          <LabelField
            title={'First Name'}
            type={'text'}
            value={this.state.adminFirstName}
            onChangeHandler={event => {
              this.setState({
                adminFirstName: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />

          <LabelField
            title={'Last Name'}
            type={'text'}
            value={this.state.adminLastName}
            onChangeHandler={event => {
              this.setState({
                adminLastName: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
          />

          <LabelField
            marginBottom={'8px'}
            title={'Email'}
            type={'email'}
            value={this.state.adminEmail}
            onChangeHandler={event => {
              this.setState({
                adminEmail: event.target.value
              }, () => {
                this.updateParentProps();
              });
            }}
            required={true}
            isDisabled={this.props.adminSelected !== undefined}
          />

          <div style={{
            fontFamily: 'Roboto, sans-serif',
            fontSize: '15px',
            color: '#222114',
            marginBottom: '16px'
          }}>Role *
            <Select
              defaultValue={adminRoleOptions[0]}
              options={adminRoleOptions}
              onChange={this.handleAdminRoleSelected}
              isSearchable={false}
            />
          </div>
        </div>
      </div>
    );
  }
}

AdminDetailsForm.propTypes = {
  adminSelected: PropTypes.instanceOf(Admin),
  onAdminEntered: PropTypes.func.isRequired,
};