import React from 'react';
import { ColorConstants } from '../../constants/ColorConstants';
import { LabelField } from '../LabelField/LabelField';
import { StylisedButton } from '../StylisedButton/StylisedButton';

export class ChangePasswordForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      oldPassword: '',
      newPassword: '',
      repeatNewPassword: '',
      disableButton: false
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit() {
    console.log(this.state);

    this.setState({ disableButton: true });

    // TODO: use the form values in state to make a web service call to BE
  }

  render() {
    return (
      <div>
        <form>
          <LabelField
            marginBottom={'8px'}
            title={'Current Password'}
            type={'password'}
            value={this.state.oldPassword}
            onChangeHandler={event => {
              this.setState({
                oldPassword: event.target.value
              });
            }}
          />
          <LabelField
            marginBottom={'8px'}
            title={'New Password'}
            type={'password'}
            value={this.state.newPassword}
            onChangeHandler={event => {
              this.setState({
                newPassword: event.target.value
              });
            }}
          />

          <div style={{
            display: 'flex',
            flexDirection: 'row'
          }}>
            <div>
              <ul>
                <li>One lowercase character</li>
                <li>One uppercase character</li>
                <li>One number</li>
              </ul>
            </div>

            <div>
              <ul>
                <li>One special character</li>
                <li>8 characters minimum</li>
              </ul>
            </div>
          </div>

          <LabelField
            marginBottom={'16px'}
            title={'Confirm New Password'}
            type={'password'}
            value={this.state.repeatNewPassword}
            onChangeHandler={event => {
              this.setState({
                repeatNewPassword: event.target.value
              });
            }}
          />
        </form>

        <StylisedButton
          title={'Update'}
          onClick={this.onFormSubmit}
          disabled={this.state.disableButton}
          colour={ColorConstants.teal}
        />
      </div>
    );
  }
}