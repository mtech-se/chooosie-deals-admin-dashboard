import { shallow } from 'enzyme';
import React from 'react';
import { ChangePasswordForm } from './ChangePasswordForm';

describe('ChangePasswordForm', () => {
  it('it renders without crashing', async () => {
    global.console.error = jest.fn();
    shallow(<ChangePasswordForm component={<div>hello</div>} />);
    expect(global.console.error).toHaveBeenCalledTimes(0);
  });
});