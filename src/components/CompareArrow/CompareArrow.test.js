import React from 'react';
import { smokeTest } from '../../utils/TestUtils';
import { CompareArrow } from './CompareArrow';

describe('CompareArrow', () => {
  it('it renders without crashing', () => {
    smokeTest(() => <CompareArrow percent={'50'}
                                  trend={true} />);
  });
});