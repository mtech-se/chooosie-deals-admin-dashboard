import * as PropTypes from 'prop-types';
import React from 'react';
import arrowDownPicture from '../../assets/images/Arrow_down.png';
import arrowUpPicture from '../../assets/images/Arrow_up.png';

export const CompareArrow = props => {
  if (props.trend === true) {
    return (
      <div style={{
        fontSize: '13px',
        display: 'flex',
        flexDirection: 'row'
      }}>
        <div style={{ color: '#6edc6c', fontWeight: 'bold' }}>{props.percent}%</div>
        <div>
          <img src={arrowUpPicture}
               width="15"
               height="13"
               alt="Up" />
          <span>Compared to previous period&#39;s {props.previousFigure}</span>
        </div>
      </div>
    );
  } else {
    return (
      <div style={{
        fontSize: '13px',
        display: 'flex',
        flexDirection: 'row'
      }}>
        <div style={{ color: '#c53e3e', fontWeight: 'bold' }}>{props.percent}%</div>
        <div>
          <img src={arrowDownPicture}
               width="15"
               height="13"
               alt="13" />
          <span>Compared to previous period&#39;s {props.previousFigure}</span>
        </div>
      </div>
    );
  }
};

CompareArrow.propTypes = {
  percent: PropTypes.string.isRequired,
  trend: PropTypes.bool.isRequired,
  previousFigure: PropTypes.number,
};