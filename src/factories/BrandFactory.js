import { Brand } from '../models/Brand';
import { DateFactory } from './DateFactory';
import { EntityPhotoFactory } from './EntityPhotoFactory';
import { MerchantFactory } from './MerchantFactory';

export class BrandFactory {
  /**
   *
   * @param {string} json
   * @returns {Brand}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Brand(
      json.id,
      json.name,
      MerchantFactory.createFromJson(json.merchant),
      EntityPhotoFactory.createFromJsonArray(json.brand_photos),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Brand[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => BrandFactory.createFromJson(json));
  }
}