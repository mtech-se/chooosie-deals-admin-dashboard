import { OverviewStats } from '../models/OverviewStats';

export class OverviewStatsFactory {
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new OverviewStats(
      json.total_published_deals,
      json.total_new_customers,
      json.total_purchased_deals,
      json.total_sales / 100,
      json.total_payouts / 100,
      json.total_commissions / 100,
    );
  }
}