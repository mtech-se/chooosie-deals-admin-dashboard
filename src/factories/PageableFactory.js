import { Pageable } from '../models/Pageable';
import { SortFactory } from './SortFactory';

export class PageableFactory {
  static createFromJson(json) {
    if (json === undefined) return undefined;

    return new Pageable(
      SortFactory.createFromJson(json.sort),
      json.offset,
      json.pageNumber,
      json.pageSize,
      json.unpaged,
      json.paged
    );
  }
}