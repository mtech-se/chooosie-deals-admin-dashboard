import { CategoryDeal } from '../models/CategoryDeal';
import { CategoryFactory } from './CategoryFactory';
import { DateFactory } from './DateFactory';

export class CategoryDealFactory {
  /**
   *
   * @param {string} json
   * @returns {CategoryDeal}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new CategoryDeal(
      json.id,
      CategoryFactory.createFromJson(json.category),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {CategoryDeal[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => CategoryDealFactory.createFromJson(json));
  }
}