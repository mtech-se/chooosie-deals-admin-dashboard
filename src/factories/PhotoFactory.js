import { Photo } from '../models/Photo';

export class PhotoFactory {
  /**
   *
   * @param {string} json
   * @returns {Photo}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Photo(
      json.id,
      json.path,
      json.created_at,
      json.updated_at,
      json.deleted_at,
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Photo[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => PhotoFactory.createFromJson(json));
  }
}