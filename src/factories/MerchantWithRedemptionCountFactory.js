import { MerchantWithRedemptionCount } from '../models/MerchantWithRedemptionCount';
import { MerchantFactory } from './MerchantFactory';

export class MerchantWithRedemptionCountFactory {
  /**
   *
   * @param {string} json
   * @returns {MerchantWithRedemptionCount}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new MerchantWithRedemptionCount(
      MerchantFactory.createFromJson(json.merchant),
      json.num_purchases_redeemed
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {MerchantWithRedemptionCount[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => MerchantWithRedemptionCountFactory.createFromJson(json));
  }
}

