import { Customer } from '../models/Customer';
import { DateFactory } from './DateFactory';

export class CustomerFactory {
  /**
   *
   * @param {string} json
   * @returns {Customer}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Customer(
      json.id,
      json.first_name,
      json.last_name,
      json.email,
      json.gender,
      json.billing_first_name,
      json.billing_last_name,
      json.billing_cellphone,
      json.billing_telephone,
      json.billing_add_line_one,
      json.billing_add_line_two,
      json.billing_add_postal_code,
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => CustomerFactory.createFromJson(json));
  }
}