import { Cuisine } from '../models/Cuisine';
import { DateFactory } from './DateFactory';

export class CuisineFactory {
  /**
   *
   * @param {string} json
   * @returns {Cuisine}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Cuisine(
      json.id,
      json.name,
      json.description,
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Cuisine[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => CuisineFactory.createFromJson(json));
  }
}