import { ContactPerson } from '../models/ContactPerson';
import { DateFactory } from './DateFactory';

export class ContactPersonFactory {
  /**
   *
   * @param json
   * @returns {ContactPerson}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new ContactPerson(
      json.id,
      json.name,
      json.position,
      json.mobile_number,
      json.email,
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {ContactPerson[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => ContactPersonFactory.createFromJson(json));
  }
}

