import { CuisineDeal } from '../models/CuisineDeal';
import { CuisineFactory } from './CuisineFactory';
import { DateFactory } from './DateFactory';

export class CuisineDealFactory {
  /**
   *
   * @param {string} json
   * @returns {CuisineDeal}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new CuisineDeal(
      json.id,
      CuisineFactory.createFromJson(json.cuisine),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {CuisineDeal[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => CuisineDealFactory.createFromJson(json));
  }
}