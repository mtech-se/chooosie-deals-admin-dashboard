import { Deal } from '../models/Deal';
import { CategoryDealFactory } from './CategoryDealFactory';
import { CuisineDealFactory } from './CuisineDealFactory';
import { DateFactory } from './DateFactory';
import { EntityPhotoFactory } from './EntityPhotoFactory';
import { OutletFactory } from './OutletFactory';

export class DealFactory {
  /**
   *
   * @param {string} json
   * @returns {Deal}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Deal(
      json.id,
      json.title,
      json.description,
      json.original_price,
      json.discounted_price,
      OutletFactory.createFromJson(json.outlet),
      json.max_quantity,
      json.num_bought,
      json.redemption_instructions,
      json.terms_of_use,
      EntityPhotoFactory.createFromJsonArray(json.deal_photos),
      json.deal_of_the_month,
      json.hit_count,
      CategoryDealFactory.createFromJsonArray(json.category_deals),
      CuisineDealFactory.createFromJsonArray(json.cuisine_deals),
      DateFactory.createMomentFromDateString(json.auto_publish_date),
      DateFactory.createMomentFromDateString(json.redemption_expiry_date),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.published_at),
      DateFactory.createMomentFromDateString(json.archived_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Deal[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => DealFactory.createFromJson(json));
  }
}