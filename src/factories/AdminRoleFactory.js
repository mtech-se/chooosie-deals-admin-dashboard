import { AdminRole } from '../models/AdminRole';
import { DateFactory } from './DateFactory';
import { RoleFactory } from './RoleFactory';

export class AdminRoleFactory {
  /**
   *
   * @param {string} json
   * @returns {AdminRole}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new AdminRole(
      json.id,
      RoleFactory.createFromJson(json.role),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {AdminRole[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => AdminRoleFactory.createFromJson(json));
  }
}