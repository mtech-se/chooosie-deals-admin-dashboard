import { Category } from '../models/Category';
import { DateFactory } from './DateFactory';
import { EntityPhotoFactory } from './EntityPhotoFactory';

export class CategoryFactory {
  /**
   *
   * @param {string} json
   * @returns {Category}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Category(
      json.id,
      json.name,
      json.description,
      EntityPhotoFactory.createFromJsonArray(json.category_photos),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Category[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => CategoryFactory.createFromJson(json));
  }
}