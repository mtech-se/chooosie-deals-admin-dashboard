import { EntityPhoto } from '../models/EntityPhoto';
import { PhotoFactory } from './PhotoFactory';

export class EntityPhotoFactory {
  /**
   *
   * @param {string} json
   * @returns {EntityPhoto}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new EntityPhoto(
      json.id,
      PhotoFactory.createFromJson(json.photo),
      json.created_at,
      json.updated_at,
      json.deleted_at,
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {EntityPhoto[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => EntityPhotoFactory.createFromJson(json));
  }
}