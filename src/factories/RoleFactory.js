import { Role } from '../models/Role';
import { DateFactory } from './DateFactory';

export class RoleFactory {
  /**
   *
   * @param {string} json
   * @returns {Role}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Role(
      json.id,
      json.name,
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Role[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => RoleFactory.createFromJson(json));
  }
}