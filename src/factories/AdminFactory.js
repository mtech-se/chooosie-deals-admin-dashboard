import { Admin } from '../models/Admin';
import { AdminRoleFactory } from './AdminRoleFactory';
import { DateFactory } from './DateFactory';

export class AdminFactory {
  /**
   *
   * @param {string} json
   * @returns {Admin}
   */
  static createFromJson(json) {
    if (json === undefined || json === null) return undefined;

    return new Admin(
      json.id,
      json.first_name,
      json.last_name,
      json.gender,
      json.email,
      AdminRoleFactory.createFromJsonArray(json.admin_roles),
      DateFactory.createMomentFromDateString(json.created_at),
      DateFactory.createMomentFromDateString(json.updated_at),
      DateFactory.createMomentFromDateString(json.deleted_at),
    );
  }

  /**
   *
   * @param {string[]} jsonArray
   * @returns {Admin[]}
   */
  static createFromJsonArray(jsonArray) {
    if (jsonArray === undefined || jsonArray === null) return undefined;

    return jsonArray.map(json => AdminFactory.createFromJson(json));
  }
}