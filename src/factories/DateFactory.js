import moment from 'moment';

export class DateFactory {
  static createMomentFromDateString(dateString) {
    if (dateString === undefined || dateString === null) return undefined;
    return moment(dateString, 'YYYY-MM-DD HH:mm');
  }
}