import { User } from '../models/User';

export class UserFactory {
  /**
   *
   * @param json
   * @returns {User}
   */
  static createFromJson(json) {
    return new User(
      json.email,
      json.first_name,
      json.last_name,
      json.billing_first_name,
      json.billing_cellphone,
      json.billing_add_line_one,
      json.billing_add_postal_code,
      json.gender,
      json.billing_telephone
    );
  }
}