https://www.npmjs.com/package/eslint-plugin-react

https://www.npmjs.com/package/react-router-prop-types

import React from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';

```javascript
class MyComponent extends React.Component {
  static propTypes = {
    // You can chain any of the above with `isRequired` to make sure a warning
    // is shown if the prop isn't provided.
    history: ReactRouterPropTypes.history.isRequired,
    location: ReactRouterPropTypes.location.isRequired,
    match: ReactRouterPropTypes.match.isRequired,
    route: ReactRouterPropTypes.route.isRequired, // for react-router-config
  }
}
``` 

